<?php

namespace App\Repository;

use App\Entity\CorreoSoporte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CorreoSoporte|null find($id, $lockMode = null, $lockVersion = null)
 * @method CorreoSoporte|null findOneBy(array $criteria, array $orderBy = null)
 * @method CorreoSoporte[]    findAll()
 * @method CorreoSoporte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorreoSoporteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CorreoSoporte::class);
    }

    public function findCorreoByGestor(int $codGestor, string $pantalla)
    {
        $entityManager = $this->getEntityManager();

        if($pantalla == 'mailbox'){
            return $entityManager->createQueryBuilder()
                ->select('c.id, c.titulo, c.adjuntos, c.leido, c.fechaCreacion, c.tipoCorreo, c.estado, g.nombre')
                ->from('App\Entity\CorreoSoporte', 'c')
                ->innerJoin('c.codGestor','g')
                ->where('c.codGestorDestinatario = :codGestor AND c.estado in (\'No Leido\', \'Leido\')')
                ->setParameter('codGestor', $codGestor)
                ->orderBy('c.fechaCreacion', 'DESC')
                ->getQuery()
                ->getResult()
                ;
        }else if($pantalla == 'mailbox-send'){
            return $entityManager->createQueryBuilder()
                ->select('c.id, c.titulo, c.adjuntos, c.leido, c.fechaCreacion, c.tipoCorreo, c.estado, g.nombre')
                ->from('App\Entity\CorreoSoporte', 'c')
                ->innerJoin('c.codGestor','g')
                ->where('c.codGestor = :codGestor')
                ->setParameter('codGestor', $codGestor)
                ->orderBy('c.fechaCreacion', 'DESC')
                ->getQuery()
                ->getResult()
                ;
        }else if($pantalla == 'mailbox-deleted'){
            return $entityManager->createQueryBuilder()
                ->select('c.id, c.titulo, c.adjuntos, c.leido, c.fechaCreacion, c.tipoCorreo, c.estado, g.nombre')
                ->from('App\Entity\CorreoSoporte', 'c')
                ->innerJoin('c.codGestor','g')
                ->where('c.codGestorDestinatario = :codGestor AND c.estado in (\'Borrado\')')
                ->setParameter('codGestor', $codGestor)
                ->orderBy('c.fechaCreacion', 'DESC')
                ->getQuery()
                ->getResult()
                ;
        }
    }

    public function findNuevosByGestor(int $codGestor)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQueryBuilder()
        ->select('COUNT(c.id) as numNuevos')
        ->from('App\Entity\CorreoSoporte', 'c')
        ->where('c.codGestorDestinatario = :codGestor')
        ->andWhere('c.estado = :estado')
        ->setParameter('codGestor', $codGestor)
        ->setParameter('estado', "No Leido")
        ->orderBy('c.fechaCreacion', 'DESC')
        ->getQuery()
        ->getResult()
        ;
    }

    public function findCorreoByCod(int $codEmail)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQueryBuilder()
        ->select('c.id, c.titulo, c.cuerpo, c.adjuntos, c.fechaCreacion, g.nombre')
        ->from('App\Entity\CorreoSoporte', 'c')
        ->innerJoin('c.codGestor','g')
        ->where('c.id = :id')
        ->setParameter('id', $codEmail)
        ->orderBy('c.fechaCreacion', 'DESC')
        ->getQuery()
        ->getOneOrNullResult()
        ;
    }

    public function findCorreoByEmpresa(int $codEmpresa)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQueryBuilder()
        ->select('c.id, c.titulo, c.adjuntos, c.leido, c.fechaCreacion, c.tipoCorreo, c.estado, e.nombre')
        ->from('App\Entity\CorreoSoporte', 'c')
        ->innerJoin('c.codEmpresa','e')
        ->where('c.codEmpresa = :codEmpresa')
        ->setParameter('codEmpresa', $codEmpresa)
        ->orderBy('c.fechaCreacion', 'DESC')
        ->getQuery()
        ->getResult()
        ;
    }

    public function deleteEmailById($id)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
                'DELETE FROM App\Entity\CorreoSoporte c
                WHERE c.id = :id'
            )
            ->setParameter('id', $id)
            ->getOneOrNullResult();
    }

    // /**
    //  * @return CorreoSoporte[] Returns an array of CorreoSoporte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CorreoSoporte
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
