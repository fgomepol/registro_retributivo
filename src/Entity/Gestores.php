<?php

namespace App\Entity;

use App\Repository\GestoresRepository;
use App\Validator\Constraints as AcmeAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=GestoresRepository::class)
 */
class Gestores implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=9, unique=true) 
     * @AcmeAssert\ValidateIdentityNumber
     */
    private $cif;

    /**
     * @ORM\Column(type="string", length=350, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=35)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es un email válido."
     * )
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED", nullable=true)
     */
    private $fechaAlta;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $verificationCode;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $licenciasDisponibles = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activo = false;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $primeraClave = true;

    /**
     * @ORM\OneToMany(targetEntity=InformeRegistroSalarial::class, mappedBy="codGestor")
     */
    private $informeRegistroSalariales;

    /**
     * @ORM\OneToMany(targetEntity=Empresa::class, mappedBy="codigoGestor")
     */
    private $empresas;

    /**
     * @ORM\OneToMany(targetEntity=Compras::class, mappedBy="codGestor")
     */
    private $compras;

    /**
     * @ORM\ManyToOne(targetEntity=CCAA::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codComunidad;

    /**
     * @ORM\ManyToOne(targetEntity=Provincias::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codProvincia;

    /**
     * @ORM\OneToMany(targetEntity=ConvenioColectivo::class, mappedBy="codGestor")
     */
    private $conveniosColectivos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localidad;

    /**
     * @ORM\OneToOne(targetEntity=DatosFacturacion::class, mappedBy="codGestor", cascade={"persist", "remove"})
     */
    private $datosFacturacion;

    /**
     * @ORM\OneToMany(targetEntity=Facturas::class, mappedBy="codGestor")
     */
    private $facturas;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $licenciasDisponiblesAsistida = 0;

    /**
     * @ORM\OneToMany(targetEntity=CorreoSoporte::class, mappedBy="codGestor")
     */
    private $correoSoportes;

    /**
     * @ORM\OneToMany(targetEntity=CorreoSoporte::class, mappedBy="codGestorDestinatario")
     */
    private $correoSoporteDestinatario;

    public function __construct()
    {
        $this->informeRegistroSalariales = new ArrayCollection();
        $this->empresas = new ArrayCollection();
        $this->compras = new ArrayCollection();
        $this->conveniosColectivos = new ArrayCollection();
        $this->facturas = new ArrayCollection();
        $this->correoSoportes = new ArrayCollection();
        $this->correoSoporteDestinatario = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function setCif(string $cif): self
    {
        $this->cif = $cif;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_GESTOR';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(?int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFechaAlta(): ?int
    {
        return $this->fechaAlta;
    }

    public function setFechaAlta(int $fechaAlta): self
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getVerificationCode(): ?string
    {
        return $this->verificationCode;
    }

    public function setVerificationCode(?string $verificationCode): self
    {
        $this->verificationCode = $verificationCode;

        return $this;
    }

    public function getLicenciasDisponibles(): ?int
    {
        return $this->licenciasDisponibles;
    }

    public function setLicenciasDisponibles(?int $licenciasDisponibles): self
    {
        $this->licenciasDisponibles = $licenciasDisponibles;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(?bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->cif;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPrimeraClave(): ?bool
    {
        return $this->primeraClave;
    }

    public function setPrimeraClave(?bool $primeraClave): self
    {
        $this->primeraClave = $primeraClave;

        return $this;
    }

    /**
     * @return Collection|InformeRegistroSalarial[]
     */
    public function getInformeRegistroSalariales(): Collection
    {
        return $this->informeRegistroSalariales;
    }

    public function addInformeRegistroSalariale(InformeRegistroSalarial $informeRegistroSalariale): self
    {
        if (!$this->informeRegistroSalariales->contains($informeRegistroSalariale)) {
            $this->informeRegistroSalariales[] = $informeRegistroSalariale;
            $informeRegistroSalariale->setCodGestor($this);
        }

        return $this;
    }

    public function removeInformeRegistroSalariale(InformeRegistroSalarial $informeRegistroSalariale): self
    {
        if ($this->informeRegistroSalariales->removeElement($informeRegistroSalariale)) {
            // set the owning side to null (unless already changed)
            if ($informeRegistroSalariale->getCodGestor() === $this) {
                $informeRegistroSalariale->setCodGestor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Empresa[]
     */
    public function getEmpresas(): Collection
    {
        return $this->empresas;
    }

    public function addEmpresa(Empresa $empresa): self
    {
        if (!$this->empresas->contains($empresa)) {
            $this->empresas[] = $empresa;
            $empresa->setCodGestor($this);
        }

        return $this;
    }

    public function removeEmpresa(Empresa $empresa): self
    {
        if ($this->empresas->removeElement($empresa)) {
            // set the owning side to null (unless already changed)
            if ($empresa->getCodGestor() === $this) {
                $empresa->setCodGestor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Compras[]
     */
    public function getCompras(): Collection
    {
        return $this->compras;
    }

    public function addCompra(Compras $compra): self
    {
        if (!$this->compras->contains($compra)) {
            $this->compras[] = $compra;
            $compra->setCodGestor($this);
        }

        return $this;
    }

    public function removeCompra(Compras $compra): self
    {
        if ($this->compras->removeElement($compra)) {
            // set the owning side to null (unless already changed)
            if ($compra->getCodGestor() === $this) {
                $compra->setCodGestor(null);
            }
        }

        return $this;
    }

    public function getCodComunidad(): ?CCAA
    {
        return $this->codComunidad;
    }

    public function setCodComunidad(?CCAA $codComunidad): self
    {
        $this->codComunidad = $codComunidad;

        return $this;
    }

    public function getCodProvincia(): ?Provincias
    {
        return $this->codProvincia;
    }

    public function setCodProvincia(?Provincias $codProvincia): self
    {
        $this->codProvincia = $codProvincia;

        return $this;
    }

    /**
     * @return Collection|ConvenioColectivo[]
     */
    public function getConveniosColectivos(): Collection
    {
        return $this->conveniosColectivos;
    }

    public function addConveniosColectivo(ConvenioColectivo $conveniosColectivo): self
    {
        if (!$this->conveniosColectivos->contains($conveniosColectivo)) {
            $this->conveniosColectivos[] = $conveniosColectivo;
            $conveniosColectivo->setCodGestor($this);
        }

        return $this;
    }

    public function removeConveniosColectivo(ConvenioColectivo $conveniosColectivo): self
    {
        if ($this->conveniosColectivos->removeElement($conveniosColectivo)) {
            // set the owning side to null (unless already changed)
            if ($conveniosColectivo->getCodGestor() === $this) {
                $conveniosColectivo->setCodGestor(null);
            }
        }

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getDatosFacturacion(): ?DatosFacturacion
    {
        return $this->datosFacturacion;
    }

    public function setDatosFacturacion(?DatosFacturacion $datosFacturacion): self
    {
        // unset the owning side of the relation if necessary
        if ($datosFacturacion === null && $this->datosFacturacion !== null) {
            $this->datosFacturacion->setCodGestor(null);
        }

        // set the owning side of the relation if necessary
        if ($datosFacturacion !== null && $datosFacturacion->getCodGestor() !== $this) {
            $datosFacturacion->setCodGestor($this);
        }

        $this->datosFacturacion = $datosFacturacion;

        return $this;
    }

    /**
     * @return Collection|Facturas[]
     */
    public function getFacturas(): Collection
    {
        return $this->facturas;
    }

    public function addFactura(Facturas $factura): self
    {
        if (!$this->facturas->contains($factura)) {
            $this->facturas[] = $factura;
            $factura->setCodGestor($this);
        }

        return $this;
    }

    public function removeFactura(Facturas $factura): self
    {
        if ($this->facturas->removeElement($factura)) {
            // set the owning side to null (unless already changed)
            if ($factura->getCodGestor() === $this) {
                $factura->setCodGestor(null);
            }
        }

        return $this;
    }

    public function getLicenciasDisponiblesAsistida(): ?int
    {
        return $this->licenciasDisponiblesAsistida;
    }

    public function setLicenciasDisponiblesAsistida(?int $licenciasDisponiblesAsistida): self
    {
        $this->licenciasDisponiblesAsistida = $licenciasDisponiblesAsistida;

        return $this;
    }

    /**
     * @return Collection|CorreoSoporte[]
     */
    public function getCorreoSoportes(): Collection
    {
        return $this->correoSoportes;
    }

    public function addCorreoSoportes(CorreoSoporte $correoSoportes): self
    {
        if (!$this->correoSoportes->contains($correoSoportes)) {
            $this->correoSoportes[] = $correoSoportes;
            $correoSoportes->setCodGestor($this);
        }

        return $this;
    }

    public function removeCorreoSoportes(CorreoSoporte $correoSoportes): self
    {
        if ($this->correoSoportes->removeElement($correoSoportes)) {
            // set the owning side to null (unless already changed)
            if ($correoSoportes->getCodGestor() === $this) {
                $correoSoportes->setCodGestor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CorreoSoporte[]
     */
    public function getCorreoSoporteDestinatario(): Collection
    {
        return $this->correoSoporteDestinatario;
    }

    public function addCorreoSoporteDestinatario(CorreoSoporte $correoSoporteDestinatario): self
    {
        if (!$this->correoSoporteDestinatario->contains($correoSoporteDestinatario)) {
            $this->correoSoporteDestinatario[] = $correoSoporteDestinatario;
            $correoSoporteDestinatario->setCodGestorDestinatario($this);
        }

        return $this;
    }

    public function removeCorreoSoporteDestinatario(CorreoSoporte $correoSoporteDestinatario): self
    {
        if ($this->correoSoporteDestinatario->removeElement($correoSoporteDestinatario)) {
            // set the owning side to null (unless already changed)
            if ($correoSoporteDestinatario->getCodGestorDestinatario() === $this) {
                $correoSoporteDestinatario->setCodGestorDestinatario(null);
            }
        }

        return $this;
    }
}
