$(document).ready(function() {
    $("#listaEmpresas").addClass("active");
    $("#registroMenu").addClass("menu-is-opening menu-open");
    $("#registroRetributivo").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

$("form[name='form']").validate({
    rules: {
        'form[trabajador]': "required",
        'form[fechaInicio]': "required",
        'form[fechaFin]': "required"
    },
    messages: {
        'form[trabajador]': "Por favor, seleccione el trabajador para obtener los registros",
        'form[fechaInicio]': "Por favor, introduzca la fecha inicio para obtener los registros",
        'form[fechaFin]': "Por favor, introduzca la fecha fin para obtener los registros"
    },
    submitHandler: function(form) {
        form.submit();
    }
});