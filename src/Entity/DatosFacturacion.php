<?php

namespace App\Entity;

use App\Repository\DatosFacturacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DatosFacturacionRepository::class)
 */
class DatosFacturacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Gestores::class, inversedBy="datosFacturacion", cascade={"persist", "remove"})
     */
    private $codGestor;

    /**
     * @ORM\OneToOne(targetEntity=Empresa::class, inversedBy="datosFacturacion", cascade={"persist", "remove"})
     */
    private $codEmpresa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $cif;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $direccion;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=Provincias::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codProvincia;

    /**
     * @ORM\ManyToOne(targetEntity=CCAA::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codComunidad;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localidad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodGestor(): ?Gestores
    {
        return $this->codGestor;
    }

    public function setCodGestor(?Gestores $codGestor): self
    {
        $this->codGestor = $codGestor;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function setCif(string $cif): self
    {
        $this->cif = $cif;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getCodProvincia(): ?Provincias
    {
        return $this->codProvincia; 
    }

    public function setCodProvincia(?Provincias $codProvincia): self
    {
        $this->codProvincia = $codProvincia;

        return $this;
    }

    public function getCodComunidad(): ?CCAA
    {
        return $this->codComunidad;
    }

    public function setCodComunidad(?CCAA $codComunidad): self
    {
        $this->codComunidad = $codComunidad;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }
}
