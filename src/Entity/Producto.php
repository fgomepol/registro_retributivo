<?php

namespace App\Entity;

use App\Repository\ProductoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductoRepository::class)
 */
class Producto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="float")
     */
    private $PVD;

    /**
     * @ORM\Column(type="float")
     */
    private $PVP;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numLicencias = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $soloGestor = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $renovacion = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipoLicencia;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPVD(): ?float
    {
        return $this->PVD;
    }

    public function setPVD(float $PVD): self
    {
        $this->PVD = $PVD;

        return $this;
    }

    public function getPVP(): ?float
    {
        return $this->PVP;
    }

    public function setPVP(float $PVP): self
    {
        $this->PVP = $PVP;

        return $this;
    }

    public function getNumLicencias(): ?int
    {
        return $this->numLicencias;
    }

    public function setNumLicencias(int $numLicencias): self
    {
        $this->numLicencias = $numLicencias;

        return $this;
    }

    public function getSoloGestor(): ?bool
    {
        return $this->soloGestor;
    }

    public function setSoloGestor(bool $soloGestor): self
    {
        $this->soloGestor = $soloGestor;

        return $this;
    }

    public function getRenovacion(): ?bool
    {
        return $this->renovacion;
    }

    public function setRenovacion(bool $renovacion): self
    {
        $this->renovacion = $renovacion;

        return $this;
    }

    public function getTipoLicencia(): ?string
    {
        return $this->tipoLicencia;
    }

    public function setTipoLicencia(string $tipoLicencia): self
    {
        $this->tipoLicencia = $tipoLicencia;

        return $this;
    }
}
