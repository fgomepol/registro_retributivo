<?php

namespace App\Entity;

use App\Repository\CorreoSoporteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CorreoSoporteRepository::class)
 */
class CorreoSoporte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Gestores::class, inversedBy="codEmpresa")
     */
    private $codGestor;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class, inversedBy="correoSoportes")
     */
    private $codEmpresa;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $titulo;

    /**
     * @ORM\Column(type="text")
     */
    private $cuerpo;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $adjuntos = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $leido = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaCreacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipoCorreo = "Inicial";

    /**
     * @ORM\OneToOne(targetEntity=CorreoSoporte::class, inversedBy="correoRelacionado", cascade={"persist", "remove"})
     */
    private $codCorreoRelacionado;

    /**
     * @ORM\OneToOne(targetEntity=CorreoSoporte::class, mappedBy="codCorreoRelacionado", cascade={"persist", "remove"})
     */
    private $correoRelacionado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $estado;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codGestorDestinatario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodGestor(): ?Gestores
    {
        return $this->codGestor;
    }

    public function setCodGestor(?Gestores $codGestor): self
    {
        $this->codGestor = $codGestor;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getCuerpo(): ?string
    {
        return $this->cuerpo;
    }

    public function setCuerpo(string $cuerpo): self
    {
        $this->cuerpo = $cuerpo;

        return $this;
    }

    public function getAdjuntos(): ?array
    {
        return $this->adjuntos;
    }

    public function setAdjuntos(?array $adjuntos): self
    {
        $this->adjuntos = $adjuntos;

        return $this;
    }

    public function getLeido(): ?bool
    {
        return $this->leido;
    }

    public function setLeido(bool $leido): self
    {
        $this->leido = $leido;

        return $this;
    }

    public function getFechaCreacion(): ?\DateTimeInterface
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(\DateTimeInterface $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getTipoCorreo(): ?string
    {
        return $this->tipoCorreo;
    }

    public function setTipoCorreo(string $tipoCorreo): self
    {
        $this->tipoCorreo = $tipoCorreo;

        return $this;
    }

    public function getCodCorreoRelacionado(): ?self
    {
        return $this->codCorreoRelacionado;
    }

    public function setCodCorreoRelacionado(?self $codCorreoRelacionado): self
    {
        $this->codCorreoRelacionado = $codCorreoRelacionado;

        return $this;
    }

    public function getCorreoRelacionado(): ?self
    {
        return $this->correoRelacionado;
    }

    public function setCorreoRelacionado(?self $correoRelacionado): self
    {
        // unset the owning side of the relation if necessary
        if ($correoRelacionado === null && $this->correoRelacionado !== null) {
            $this->correoRelacionado->setCodCorreoRelacionado(null);
        }

        // set the owning side of the relation if necessary
        if ($correoRelacionado !== null && $correoRelacionado->getCodCorreoRelacionado() !== $this) {
            $correoRelacionado->setCodCorreoRelacionado($this);
        }

        $this->correoRelacionado = $correoRelacionado;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getCodGestorDestinatario(): ?int
    {
        return $this->codGestorDestinatario;
    }

    public function setCodGestorDestinatario(?int $codGestorDestinatario): self
    {
        $this->codGestorDestinatario = $codGestorDestinatario;

        return $this;
    }
}
