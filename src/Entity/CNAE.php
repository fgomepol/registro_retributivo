<?php

namespace App\Entity;

use App\Repository\CNAERepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CNAERepository::class)
 */
class CNAE
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $codCNAE;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $tituloCNAE;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCNAE(): ?string
    {
        return $this->codCNAE;
    }

    public function setCodCNAE(string $codCNAE): self
    {
        $this->codCNAE = $codCNAE;

        return $this;
    }

    public function getTituloCNAE(): ?string
    {
        return $this->tituloCNAE;
    }

    public function setTituloCNAE(string $tituloCNAE): self
    {
        $this->tituloCNAE = $tituloCNAE;

        return $this;
    }
}
