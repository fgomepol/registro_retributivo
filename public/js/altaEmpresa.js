$(document).ready(function() {
    $("#nuevaEmpresa").addClass("active");
    $("#registroMenu").addClass("menu-is-opening menu-open");
    $("#registroRetributivo").show();
});

$("form[name='empresa']").validate({
    rules: {
        'empresa[nombre]': "required",
        'empresa[telefono]': "required",
        'empresa[direccion]': "required",
        'empresa[tipoLicencia]': "required",
        'empresa[cp]': {
            cpValid: true
        },
        'empresa[email]': {
            emailValid: true
        },
        'empresa[cif]': {
            docValid: true
        }
    },
    messages: {
        'empresa[nombre]': "Por favor, introduzca su nombre",
        'empresa[telefono]': "Por favor, introduzca su telefono",
        'empresa[direccion]': "Por favor, introduzca su direccion",
        'empresa[cp]': "Por favor, introduzca un código postal válido",
        'empresa[cif]': "Introduzca un CIF/NIF/NIE válido.",
        'empresa[email]': "Por favor, introduce una dirección de correo electrónico válida",
        'empresa[codComunidad]': "Por favor, introduzca su comunidad autónoma",
        'empresa[codProvincia]': "Por favor, introduzca su provincia",
        'empresa[tipoLicencia]': "Por favor, seleccione el tipo de licencia"
    },
    submitHandler: function(form) {
        form.submit();
    }
});