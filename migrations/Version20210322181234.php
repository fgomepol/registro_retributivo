<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210322181234 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ccaa (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE centro_trabajo (id INT AUTO_INCREMENT NOT NULL, cod_empresa INT NOT NULL, ccaa SMALLINT NOT NULL, provincia SMALLINT NOT NULL, direccion VARCHAR(255) DEFAULT NULL, cp SMALLINT UNSIGNED, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE compras (id INT AUTO_INCREMENT NOT NULL, cod_gestor INT DEFAULT NULL, cod_empresa INT DEFAULT NULL, cod_producto INT NOT NULL, cantidad SMALLINT NOT NULL, importe DOUBLE PRECISION NOT NULL, fecha_compra INTEGER UNSIGNED, modo_pago VARCHAR(255) NOT NULL, pagado TINYINT(1) NOT NULL, fecha_pago INTEGER UNSIGNED, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE convenio_colectivo (id INT AUTO_INCREMENT NOT NULL, cod_gestor INT DEFAULT NULL, cod_empresa INT DEFAULT NULL, nombre_convenio VARCHAR(500) NOT NULL, salario_convenio DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE datos_informe_registro_salarial (id INT AUTO_INCREMENT NOT NULL, id_informe INT NOT NULL, grupo SMALLINT NOT NULL, puesto VARCHAR(255) NOT NULL, sexo VARCHAR(1) NOT NULL, concepto VARCHAR(255) NOT NULL, horas SMALLINT DEFAULT NULL, importe DOUBLE PRECISION NOT NULL, brecha DOUBLE PRECISION NOT NULL, observaciones LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detalle_registro_salarial (id INT AUTO_INCREMENT NOT NULL, cod_empresa INT NOT NULL, cod_centro INT DEFAULT NULL, cod_trabajador INT NOT NULL, grupo SMALLINT NOT NULL, puesto VARCHAR(255) NOT NULL, sexo VARCHAR(1) NOT NULL, concepto VARCHAR(255) NOT NULL, descripcion VARCHAR(500) NOT NULL, horas SMALLINT DEFAULT NULL, importe DOUBLE PRECISION NOT NULL, mes SMALLINT NOT NULL, anio SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empresa (id INT AUTO_INCREMENT NOT NULL, cod_gestor INT DEFAULT NULL, nombre VARCHAR(350) NOT NULL, cif VARCHAR(9) NOT NULL, direccion VARCHAR(350) DEFAULT NULL, ccaa SMALLINT NOT NULL, provincia SMALLINT NOT NULL, cp SMALLINT UNSIGNED, email VARCHAR(255) NOT NULL, telefono VARCHAR(35) NOT NULL, actividad_principal VARCHAR(255) DEFAULT NULL, fecha_alta INTEGER UNSIGNED, autogestion TINYINT(1) NOT NULL, fecha_renovacion INTEGER UNSIGNED, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gestores (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, cif VARCHAR(9) NOT NULL, direccion VARCHAR(350) DEFAULT NULL, telefono VARCHAR(35) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(50) NOT NULL, fecha_alta INTEGER UNSIGNED, uuid VARCHAR(255) DEFAULT NULL, verification_code VARCHAR(6) DEFAULT NULL, licencias_disponibles SMALLINT UNSIGNED, activo TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_2D0C8E6E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE grupo_profesional (id INT AUTO_INCREMENT NOT NULL, grupo VARCHAR(15) NOT NULL, nombre_grupo VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE informe_registro_salarial (id INT AUTO_INCREMENT NOT NULL, cod_empresa INT NOT NULL, cod_centro INT DEFAULT NULL, mes SMALLINT NOT NULL, anio SMALLINT NOT NULL, fecha_creacion INTEGER UNSIGNED, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE producto (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, pvd DOUBLE PRECISION NOT NULL, pvp DOUBLE PRECISION NOT NULL, num_licencias SMALLINT NOT NULL, solo_gestor TINYINT(1) NOT NULL, renovacion TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provincias (id INT AUTO_INCREMENT NOT NULL, cod_comunidad INT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trabajador (id INT AUTO_INCREMENT NOT NULL, cod_empresa INT NOT NULL, cod_centro INT DEFAULT NULL, cod_convenio INT NOT NULL, nombre VARCHAR(255) NOT NULL, apellidos VARCHAR(350) NOT NULL, dni VARCHAR(9) NOT NULL, direccion VARCHAR(2500) DEFAULT NULL, telefono VARCHAR(35) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, fecha_inicio_empresa INTEGER UNSIGNED, sexo VARCHAR(1) NOT NULL, grupo SMALLINT NOT NULL, puesto VARCHAR(255) NOT NULL, num_pagas SMALLINT NOT NULL, fecha_alta INTEGER UNSIGNED, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ccaa');
        $this->addSql('DROP TABLE centro_trabajo');
        $this->addSql('DROP TABLE compras');
        $this->addSql('DROP TABLE convenio_colectivo');
        $this->addSql('DROP TABLE datos_informe_registro_salarial');
        $this->addSql('DROP TABLE detalle_registro_salarial');
        $this->addSql('DROP TABLE empresa');
        $this->addSql('DROP TABLE gestores');
        $this->addSql('DROP TABLE grupo_profesional');
        $this->addSql('DROP TABLE informe_registro_salarial');
        $this->addSql('DROP TABLE producto');
        $this->addSql('DROP TABLE provincias');
        $this->addSql('DROP TABLE trabajador');
    }
}
