<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210503144229 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE datos_facturacion CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas ADD cod_compra_id INT NOT NULL, CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas ADD CONSTRAINT FK_622B9C0F5A772B6D FOREIGN KEY (cod_compra_id) REFERENCES compras (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_622B9C0F5A772B6D ON facturas (cod_compra_id)');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE datos_facturacion CHANGE cp cp SMALLINT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas DROP FOREIGN KEY FK_622B9C0F5A772B6D');
        $this->addSql('DROP INDEX UNIQ_622B9C0F5A772B6D ON facturas');
        $this->addSql('ALTER TABLE facturas DROP cod_compra_id, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
