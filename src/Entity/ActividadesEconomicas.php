<?php

namespace App\Entity;

use App\Repository\ActividadesEconomicasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActividadesEconomicasRepository::class)
 */
class ActividadesEconomicas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $idActividad;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codActividad;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $nombre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdActividad(): ?string
    {
        return $this->idActividad;
    }

    public function setIdActividad(string $idActividad): self
    {
        $this->idActividad = $idActividad;

        return $this;
    }

    public function getCodActividad(): ?string
    {
        return $this->codActividad;
    }

    public function setCodActividad(string $codActividad): self
    {
        $this->codActividad = $codActividad;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }
}
