<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210410070300 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compras_producto (compras_id INT NOT NULL, producto_id INT NOT NULL, INDEX IDX_4070487BFF341D56 (compras_id), INDEX IDX_4070487B7645698E (producto_id), PRIMARY KEY(compras_id, producto_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE compras_producto ADD CONSTRAINT FK_4070487BFF341D56 FOREIGN KEY (compras_id) REFERENCES compras (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE compras_producto ADD CONSTRAINT FK_4070487B7645698E FOREIGN KEY (producto_id) REFERENCES producto (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras ADD cod_gestor_id INT DEFAULT NULL, ADD cod_empresa_id INT DEFAULT NULL, DROP cod_gestor, DROP cod_empresa, DROP cod_producto, CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE compras ADD CONSTRAINT FK_3692E1B7E0F7686E FOREIGN KEY (cod_gestor_id) REFERENCES gestores (id)');
        $this->addSql('ALTER TABLE compras ADD CONSTRAINT FK_3692E1B77902D32C FOREIGN KEY (cod_empresa_id) REFERENCES empresa (id)');
        $this->addSql('CREATE INDEX IDX_3692E1B7E0F7686E ON compras (cod_gestor_id)');
        $this->addSql('CREATE INDEX IDX_3692E1B77902D32C ON compras (cod_empresa_id)');
        $this->addSql('ALTER TABLE datos_informe_registro_salarial CHANGE id_informe id_informe_id INT NOT NULL');
        $this->addSql('ALTER TABLE datos_informe_registro_salarial ADD CONSTRAINT FK_EB890E9326990C38 FOREIGN KEY (id_informe_id) REFERENCES informe_registro_salarial (id)');
        $this->addSql('CREATE INDEX IDX_EB890E9326990C38 ON datos_informe_registro_salarial (id_informe_id)');
        $this->addSql('ALTER TABLE detalle_registro_salarial ADD cod_empresa_id INT NOT NULL, ADD cod_trabajador_id INT NOT NULL, ADD grupo_id INT NOT NULL, DROP cod_empresa, DROP cod_trabajador, DROP grupo');
        $this->addSql('ALTER TABLE detalle_registro_salarial ADD CONSTRAINT FK_8C50B51E7902D32C FOREIGN KEY (cod_empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE detalle_registro_salarial ADD CONSTRAINT FK_8C50B51EE6516B8D FOREIGN KEY (cod_trabajador_id) REFERENCES trabajador (id)');
        $this->addSql('ALTER TABLE detalle_registro_salarial ADD CONSTRAINT FK_8C50B51E9C833003 FOREIGN KEY (grupo_id) REFERENCES grupo_profesional (id)');
        $this->addSql('CREATE INDEX IDX_8C50B51E7902D32C ON detalle_registro_salarial (cod_empresa_id)');
        $this->addSql('CREATE INDEX IDX_8C50B51EE6516B8D ON detalle_registro_salarial (cod_trabajador_id)');
        $this->addSql('CREATE INDEX IDX_8C50B51E9C833003 ON detalle_registro_salarial (grupo_id)');
        $this->addSql('ALTER TABLE empresa ADD cod_comunidad_id INT NOT NULL, ADD cod_provincia_id INT NOT NULL, ADD actividad_principal_id INT NOT NULL, DROP ccaa, DROP provincia, DROP actividad_principal, CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED, CHANGE cod_gestor cod_gestor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A5092FE170C FOREIGN KEY (cod_comunidad_id) REFERENCES ccaa (id)');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A5064ABF1E8 FOREIGN KEY (cod_provincia_id) REFERENCES provincias (id)');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A50E0F7686E FOREIGN KEY (cod_gestor_id) REFERENCES gestores (id)');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A5095E3C98C FOREIGN KEY (actividad_principal_id) REFERENCES actividades_economicas (id)');
        $this->addSql('CREATE INDEX IDX_B8D75A5092FE170C ON empresa (cod_comunidad_id)');
        $this->addSql('CREATE INDEX IDX_B8D75A5064ABF1E8 ON empresa (cod_provincia_id)');
        $this->addSql('CREATE INDEX IDX_B8D75A50E0F7686E ON empresa (cod_gestor_id)');
        $this->addSql('CREATE INDEX IDX_B8D75A5095E3C98C ON empresa (actividad_principal_id)');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE ccaa ccaa SMALLINT UNSIGNED, CHANGE provincia provincia SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador ADD cod_empresa_id INT NOT NULL, ADD cod_convenio_id INT NOT NULL, ADD grupo_id INT NOT NULL, DROP cod_empresa, DROP cod_centro, DROP cod_convenio, DROP grupo, CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador ADD CONSTRAINT FK_42157CDF7902D32C FOREIGN KEY (cod_empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE trabajador ADD CONSTRAINT FK_42157CDF4C2FECD1 FOREIGN KEY (cod_convenio_id) REFERENCES convenio_colectivo (id)');
        $this->addSql('ALTER TABLE trabajador ADD CONSTRAINT FK_42157CDF9C833003 FOREIGN KEY (grupo_id) REFERENCES grupo_profesional (id)');
        $this->addSql('CREATE INDEX IDX_42157CDF7902D32C ON trabajador (cod_empresa_id)');
        $this->addSql('CREATE INDEX IDX_42157CDF4C2FECD1 ON trabajador (cod_convenio_id)');
        $this->addSql('CREATE INDEX IDX_42157CDF9C833003 ON trabajador (grupo_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE compras_producto');
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras DROP FOREIGN KEY FK_3692E1B7E0F7686E');
        $this->addSql('ALTER TABLE compras DROP FOREIGN KEY FK_3692E1B77902D32C');
        $this->addSql('DROP INDEX IDX_3692E1B7E0F7686E ON compras');
        $this->addSql('DROP INDEX IDX_3692E1B77902D32C ON compras');
        $this->addSql('ALTER TABLE compras ADD cod_gestor INT DEFAULT NULL, ADD cod_empresa INT DEFAULT NULL, ADD cod_producto INT NOT NULL, DROP cod_gestor_id, DROP cod_empresa_id, CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE datos_informe_registro_salarial DROP FOREIGN KEY FK_EB890E9326990C38');
        $this->addSql('DROP INDEX IDX_EB890E9326990C38 ON datos_informe_registro_salarial');
        $this->addSql('ALTER TABLE datos_informe_registro_salarial CHANGE id_informe_id id_informe INT NOT NULL');
        $this->addSql('ALTER TABLE detalle_registro_salarial DROP FOREIGN KEY FK_8C50B51E7902D32C');
        $this->addSql('ALTER TABLE detalle_registro_salarial DROP FOREIGN KEY FK_8C50B51EE6516B8D');
        $this->addSql('ALTER TABLE detalle_registro_salarial DROP FOREIGN KEY FK_8C50B51E9C833003');
        $this->addSql('DROP INDEX IDX_8C50B51E7902D32C ON detalle_registro_salarial');
        $this->addSql('DROP INDEX IDX_8C50B51EE6516B8D ON detalle_registro_salarial');
        $this->addSql('DROP INDEX IDX_8C50B51E9C833003 ON detalle_registro_salarial');
        $this->addSql('ALTER TABLE detalle_registro_salarial ADD cod_empresa INT NOT NULL, ADD cod_trabajador INT NOT NULL, ADD grupo SMALLINT NOT NULL, DROP cod_empresa_id, DROP cod_trabajador_id, DROP grupo_id');
        $this->addSql('ALTER TABLE empresa DROP FOREIGN KEY FK_B8D75A5092FE170C');
        $this->addSql('ALTER TABLE empresa DROP FOREIGN KEY FK_B8D75A5064ABF1E8');
        $this->addSql('ALTER TABLE empresa DROP FOREIGN KEY FK_B8D75A50E0F7686E');
        $this->addSql('ALTER TABLE empresa DROP FOREIGN KEY FK_B8D75A5095E3C98C');
        $this->addSql('DROP INDEX IDX_B8D75A5092FE170C ON empresa');
        $this->addSql('DROP INDEX IDX_B8D75A5064ABF1E8 ON empresa');
        $this->addSql('DROP INDEX IDX_B8D75A50E0F7686E ON empresa');
        $this->addSql('DROP INDEX IDX_B8D75A5095E3C98C ON empresa');
        $this->addSql('ALTER TABLE empresa ADD ccaa LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:object)\', ADD provincia LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:object)\', ADD actividad_principal LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:object)\', DROP cod_comunidad_id, DROP cod_provincia_id, DROP actividad_principal_id, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL, CHANGE cod_gestor_id cod_gestor INT DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE ccaa ccaa SMALLINT UNSIGNED DEFAULT NULL, CHANGE provincia provincia SMALLINT UNSIGNED DEFAULT NULL, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE trabajador DROP FOREIGN KEY FK_42157CDF7902D32C');
        $this->addSql('ALTER TABLE trabajador DROP FOREIGN KEY FK_42157CDF4C2FECD1');
        $this->addSql('ALTER TABLE trabajador DROP FOREIGN KEY FK_42157CDF9C833003');
        $this->addSql('DROP INDEX IDX_42157CDF7902D32C ON trabajador');
        $this->addSql('DROP INDEX IDX_42157CDF4C2FECD1 ON trabajador');
        $this->addSql('DROP INDEX IDX_42157CDF9C833003 ON trabajador');
        $this->addSql('ALTER TABLE trabajador ADD cod_empresa INT NOT NULL, ADD cod_centro INT DEFAULT NULL, ADD cod_convenio INT NOT NULL, ADD grupo SMALLINT NOT NULL, DROP cod_empresa_id, DROP cod_convenio_id, DROP grupo_id, CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
