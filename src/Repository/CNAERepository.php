<?php

namespace App\Repository;

use App\Entity\CNAE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CNAE|null find($id, $lockMode = null, $lockVersion = null)
 * @method CNAE|null findOneBy(array $criteria, array $orderBy = null)
 * @method CNAE[]    findAll()
 * @method CNAE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CNAERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CNAE::class);
    }

    public function findByBusqueda(string $tipo, string $valor)
    {
        $resultado = null;

        if($tipo == "codigo"){
            $resultado = $this->createQueryBuilder('c')
            ->where('c.codCNAE like :valor')
            ->setParameter('valor', $valor."%")
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
        }else if($tipo == "actividad"){
            $resultado = $this->createQueryBuilder('c')
            ->where('c.tituloCNAE like :valor')
            ->setParameter('valor', "%".$valor."%")
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
        }

        return $resultado;
    }

    // /**
    //  * @return CNAE[] Returns an array of CNAE objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CNAE
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
