<?php

namespace App\Entity;

use App\Repository\IAERepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IAERepository::class)
 */
class IAE
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $seccion;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $division;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $agrupacion;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $grupo;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $epigrafe;

    /**
     * @ORM\Column(type="string", length=350, nullable=true)
     */
    private $denominacion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeccion(): ?string
    {
        return $this->seccion;
    }

    public function setSeccion(?string $seccion): self
    {
        $this->seccion = $seccion;

        return $this;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    public function setDivision(?string $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getAgrupacion(): ?string
    {
        return $this->agrupacion;
    }

    public function setAgrupacion(?string $agrupacion): self
    {
        $this->agrupacion = $agrupacion;

        return $this;
    }

    public function getGrupo(): ?string
    {
        return $this->grupo;
    }

    public function setGrupo(?string $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getEpigrafe(): ?string
    {
        return $this->epigrafe;
    }

    public function setEpigrafe(?string $epigrafe): self
    {
        $this->epigrafe = $epigrafe;

        return $this;
    }

    public function getDenominacion(): ?string
    {
        return $this->denominacion;
    }

    public function setDenominacion(?string $denominacion): self
    {
        $this->denominacion = $denominacion;

        return $this;
    }
}
