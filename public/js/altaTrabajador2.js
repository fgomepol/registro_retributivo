$(document).ready(function() {
    $("#nuevoTrabajador").addClass("active");
    $("#registroMenu").addClass("menu-is-opening menu-open");
    $("#registroRetributivo").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

$(document).change(function() {
    if($("#form_codConvenio").val() == '0'){
        $("#nuevoConvenio1").show();
        $("#nuevoConvenio2").show();
    }else{
        $("#nuevoConvenio1").hide();
        $("#nuevoConvenio2").hide(); 
    }

    if( $("#form_jornadaReducida").prop('checked') ) {
        $("#datosReduccion").show();
        $("#datosReduccion2").show();

        $('#form_motivoReduccion').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Seleccione el motivo de la reducción.'
            }
        });

        $('#form_porcentajeReducida').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Debe introducir el % de la jornada total con la reducción.'
            }
        });

    }else{
        $("#datosReduccion").hide();
        $("#datosReduccion2").hide();

        $('#form_motivoReduccion').val("");
        $('#form_porcentajeReducida').val("");

        $('#form_motivoReduccion').rules("remove");
        $('#form_porcentajeReducida').rules("remove");
    }
});

$("form[name='form']").validate({
    rules: {
        'form[nombre]': "required",
        'form[apellidos]': "required",
        'form[puesto]': "required",
        'form[codConvenio]': "required",
        'form[tipoConvenio]': "required",
        'form[grupo]': "required",
        'form[fechaInicioEmpresa]': "required",
        'form[porcentajeJornada]': "required"
    },
    messages: {
        'form[nombre]': "Por favor, introduzca su nombre",
        'form[apellidos]': "Por favor, introduzca sus apellidos",
        'form[puesto]': "Por favor, introduzca su puesto",
        'form[codConvenio]': "Por favor, elija su convenio",
        'form[tipoConvenio]': "Por favor, indique el nuevo convenio.",
        'form[grupo]': "Por favor, elija un grupo profesional",
        'form[fechaInicioEmpresa]': "Por favor, introduzca su fecha de inicio en la empresa"
    },
    submitHandler: function(form) {
        form.submit();
    }
});