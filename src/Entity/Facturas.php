<?php

namespace App\Entity;

use App\Repository\FacturasRepository;
use App\Validator\Constraints as AcmeAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasRepository::class)
 */
class Facturas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $codFactura;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=9)
     * @AcmeAssert\ValidateIdentityNumber
     */
    private $cif;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $localidad;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $cp;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaFactura;

    /**
     * @ORM\Column(type="integer")
     */
    private $anio;

    /**
     * @ORM\ManyToOne(targetEntity=CCAA::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codComunidad;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombreFichero;

    /**
     * @ORM\OneToMany(targetEntity=FacturaDetalle::class, mappedBy="codFactura", orphanRemoval=true)
     */
    private $facturaDetalles;

    /**
     * @ORM\ManyToOne(targetEntity=Gestores::class, inversedBy="facturas")
     */
    private $codGestor;

    /**
     * @ORM\OneToOne(targetEntity=Compras::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $codCompra;

    /**
     * @ORM\ManyToOne(targetEntity=Provincias::class)
     */
    private $codProvincia;

    /**
     * @ORM\ManyToOne(targetEntity=DatosFacturacion::class)
     */
    private $codDatosFacturacion;

    public function __construct()
    {
        $this->facturaDetalles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodFactura(): ?int
    {
        return $this->codFactura;
    }

    public function setCodFactura(int $codFactura): self
    {
        $this->codFactura = $codFactura;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function setCif(string $cif): self
    {
        $this->cif = $cif;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getFechaFactura(): ?int
    {
        return $this->fechaFactura;
    }

    public function setFechaFactura(int $fechaFactura): self
    {
        $this->fechaFactura = $fechaFactura;

        return $this;
    }

    public function getAnio(): ?int
    {
        return $this->anio;
    }

    public function setAnio(int $anio): self
    {
        $this->anio = $anio;

        return $this;
    }

    public function getCodComunidad(): ?CCAA
    {
        return $this->codComunidad;
    }

    public function setCodComunidad(?CCAA $codComunidad): self
    {
        $this->codComunidad = $codComunidad;

        return $this;
    }

    public function getNombreFichero(): ?string
    {
        return $this->nombreFichero;
    }

    public function setNombreFichero(string $nombreFichero): self
    {
        $this->nombreFichero = $nombreFichero;

        return $this;
    }

    /**
     * @return Collection|FacturaDetalle[]
     */
    public function getFacturaDetalles(): Collection
    {
        return $this->facturaDetalles;
    }

    public function addFacturaDetalle(FacturaDetalle $facturaDetalle): self
    {
        if (!$this->facturaDetalles->contains($facturaDetalle)) {
            $this->facturaDetalles[] = $facturaDetalle;
            $facturaDetalle->setCodFactura($this);
        }

        return $this;
    }

    public function removeFacturaDetalle(FacturaDetalle $facturaDetalle): self
    {
        if ($this->facturaDetalles->removeElement($facturaDetalle)) {
            // set the owning side to null (unless already changed)
            if ($facturaDetalle->getCodFactura() === $this) {
                $facturaDetalle->setCodFactura(null);
            }
        }

        return $this;
    }

    public function getCodGestor(): ?Gestores
    {
        return $this->codGestor;
    }

    public function setCodGestor(?Gestores $codGestor): self
    {
        $this->codGestor = $codGestor;

        return $this;
    }

    /**
     * Calcula total factura.
     *
     * @return float
     */
    public function getTotal(): float
    {
        $total = 0;

        foreach ($this->getFacturaDetalles() as $item) {
            $total += $item->getImporte();
        }

        return $total;
    }

    public function getCodCompra(): ?Compras
    {
        return $this->codCompra;
    }

    public function setCodCompra(Compras $codCompra): self
    {
        $this->codCompra = $codCompra;

        return $this;
    }

    public function getCodProvincia(): ?Provincias
    {
        return $this->codProvincia;
    }

    public function setCodProvincia(?Provincias $codProvincia): self
    {
        $this->codProvincia = $codProvincia;

        return $this;
    }

    public function getCodDatosFacturacion(): ?DatosFacturacion
    {
        return $this->codDatosFacturacion;
    }

    public function setCodDatosFacturacion(?DatosFacturacion $codDatosFacturacion): self
    {
        $this->codDatosFacturacion = $codDatosFacturacion;

        return $this;
    }
}
