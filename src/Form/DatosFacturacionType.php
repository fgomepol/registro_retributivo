<?php

namespace App\Form;

use App\Entity\CCAA;
use App\Entity\DatosFacturacion;
use App\Entity\Provincias;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatosFacturacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre de la empresa']])
            ->add('cif', TextType::class, ['attr' => ['placeholder' => 'CIF/NIF/NIE de la empresa']])
            ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección de la empresa']])
            ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal']])
            ->add('localidad', TextType::class, ['attr' => ['placeholder' => 'Localidad']])
            ->add('codProvincia', EntityType::class, [
                'class' => Provincias::class,
                'choice_label' => 'nombre',
            ])
            ->add('codComunidad', EntityType::class, [
                'class' => CCAA::class,
                'choice_label' => 'nombre',
            ])
            ->add('save', SubmitType::class, ['label' => 'Guardar Datos'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DatosFacturacion::class,
        ]);
    }
}
