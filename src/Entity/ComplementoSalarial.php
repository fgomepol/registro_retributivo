<?php

namespace App\Entity;

use App\Repository\ComplementoSalarialRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementoSalarialRepository::class)
 */
class ComplementoSalarial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tipoRetribucion;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $normalizable;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $anualizable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipoRetribucion(): ?string
    {
        return $this->tipoRetribucion;
    }

    public function setTipoRetribucion(string $tipoRetribucion): self
    {
        $this->tipoRetribucion = $tipoRetribucion;

        return $this;
    }

    public function getNormalizable(): ?string
    {
        return $this->normalizable;
    }

    public function setNormalizable(string $normalizable): self
    {
        $this->normalizable = $normalizable;

        return $this;
    }

    public function getAnualizable(): ?string
    {
        return $this->anualizable;
    }

    public function setAnualizable(string $anualizable): self
    {
        $this->anualizable = $anualizable;

        return $this;
    }
}
