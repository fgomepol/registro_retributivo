<?php

namespace App\Repository;

use App\Entity\Compras;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compras|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compras|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compras[]    findAll()
 * @method Compras[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComprasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compras::class);
    }

    public function deleteCompraById($id)
    {
        $entityManager = $this->getEntityManager();

        $entityManager->createQuery(
            'DELETE FROM App\Entity\DetalleCompras d
            WHERE d.codCompra = :id'
        )
        ->setParameter('id', $id)
        ->getResult();

        return $entityManager->createQuery(
                'DELETE FROM App\Entity\Compras c
                WHERE c.id = :id'
            )
            ->setParameter('id', $id)
            ->getOneOrNullResult();
    }

    public function findComprasNoPagadas()
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
            ->select('c.id','g.id as codGestor','g.nombre','g.email','g.telefono','c.fechaCompra','c.importe')
            ->from('App\Entity\Compras', 'c')
            ->innerJoin('c.codGestor','g')
            ->where('c.pagado = 0')
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findUltimaCompra(int $codGestor): ?Compras
    {
        return $this->createQueryBuilder('c')
            ->where('c.pagado = 0')
            ->andWhere('c.codGestor = :val')
            ->setParameter('val', $codGestor)
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findUltimaCompraPersonalizada(int $codGestor): ?Compras
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.codGestor = :val')
            ->setParameter('val', $codGestor)
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    // /**
    //  * @return Compras[] Returns an array of Compras objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Compras
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
