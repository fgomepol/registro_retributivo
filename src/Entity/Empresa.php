<?php

namespace App\Entity;

use App\Repository\EmpresaRepository;
use App\Validator\Constraints as AcmeAssert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=EmpresaRepository::class)
 */
class Empresa implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=9, unique=true) 
     * @AcmeAssert\ValidateIdentityNumber
     */
    private $cif;

    /**
     * @ORM\Column(type="string", length=350, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\ManyToOne(targetEntity=CCAA::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codComunidad;

    /**
     * @ORM\ManyToOne(targetEntity=Provincias::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codProvincia;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es un email válido."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=35)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $carta_digital;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaAlta;

    /**
     * @ORM\Column(type="boolean")
     */
    private $autogestion = false;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaRenovacion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $activo = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $claveEnviada=false;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $primeraClave = true;

    /**
     * @ORM\OneToMany(targetEntity=InformeRegistroSalarial::class, mappedBy="codEmpresa")
     */
    private $informeRegistroSalarial;

    /**
     * @ORM\ManyToOne(targetEntity=Gestores::class, inversedBy="empresas")
     */
    private $codGestor;

    /**
     * @ORM\OneToMany(targetEntity=Compras::class, mappedBy="codEmpresa")
     */
    private $compras;

    /**
     * @ORM\OneToMany(targetEntity=DetalleRegistroSalarial::class, mappedBy="codEmpresa", orphanRemoval=true)
     */
    private $detallesRegistroSalarial;

    /**
     * @ORM\OneToMany(targetEntity=Trabajador::class, mappedBy="codEmpresa", orphanRemoval=true)
     */
    private $trabajadores;

    /**
     * @ORM\OneToMany(targetEntity=ConvenioColectivo::class, mappedBy="codEmpresa")
     */
    private $conveniosColectivos;

    /**
     * @ORM\OneToOne(targetEntity=DatosFacturacion::class, mappedBy="codEmpresa", cascade={"persist", "remove"})
     */
    private $datosFacturacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tipoLicencia;

    /**
     * @ORM\OneToMany(targetEntity=CorreoSoporte::class, mappedBy="codEmpresa")
     */
    private $correoSoportes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $registroHorario = true;

    public function __construct()
    {
        $this->informeRegistroSalarial = new ArrayCollection();
        $this->compras = new ArrayCollection();
        $this->detallesRegistroSalarial = new ArrayCollection();
        $this->trabajadores = new ArrayCollection();
        $this->conveniosColectivos = new ArrayCollection();
        $this->correoSoportes = new ArrayCollection();
        $this->registroHorario = true;
        $this->activo = true;
        $this->primeraClave = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCif(): ?string
    {
        return $this->cif;
    }

    public function setCif(string $cif): self
    {
        $this->cif = $cif;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(?int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getFechaAlta(): ?int
    {
        return $this->fechaAlta;
    }

    public function setFechaAlta(int $fechaAlta): self
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    public function getAutogestion(): ?bool
    {
        return $this->autogestion;
    }

    public function setAutogestion(bool $autogestion): self
    {
        $this->autogestion = $autogestion;

        return $this;
    }

    public function getFechaRenovacion(): ?int
    {
        return $this->fechaRenovacion;
    }

    public function setFechaRenovacion(int $fechaRenovacion): self
    {
        $this->fechaRenovacion = $fechaRenovacion;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getClaveEnviada(): ?bool
    {
        return $this->claveEnviada;
    }

    public function setClaveEnviada(?bool $claveEnviada): self
    {
        $this->claveEnviada = $claveEnviada;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(?bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    public function getCartaDigital(): ?string
    {
        return $this->carta_digital;
    }

    public function setCartaDigital(string $carta_digital): self
    {
        $this->carta_digital = $carta_digital;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->cif;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_EMPRESA';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPrimeraClave(): ?bool
    {
        return $this->primeraClave;
    }

    public function setPrimeraClave(?bool $primeraClave): self
    {
        $this->primeraClave = $primeraClave;

        return $this;
    }

    /**
     * @return Collection|InformeRegistroSalarial[]
     */
    public function getInformeRegistroSalarial(): Collection
    {
        return $this->informeRegistroSalarial;
    }

    public function addInformeRegistroSalarial(InformeRegistroSalarial $informeRegistroSalarial): self
    {
        if (!$this->informeRegistroSalarial->contains($informeRegistroSalarial)) {
            $this->informeRegistroSalarial[] = $informeRegistroSalarial;
            $informeRegistroSalarial->setCodEmpresa($this);
        }

        return $this;
    }

    public function removeInformeRegistroSalarial(InformeRegistroSalarial $informeRegistroSalarial): self
    {
        if ($this->informeRegistroSalarial->removeElement($informeRegistroSalarial)) {
            // set the owning side to null (unless already changed)
            if ($informeRegistroSalarial->getCodEmpresa() === $this) {
                $informeRegistroSalarial->setCodEmpresa(null);
            }
        }

        return $this;
    }

    public function getCodComunidad(): ?CCAA
    {
        return $this->codComunidad;
    }

    public function setCodComunidad(?CCAA $codComunidad): self
    {
        $this->codComunidad = $codComunidad;

        return $this;
    }

    public function getCodProvincia(): ?Provincias
    {
        return $this->codProvincia;
    }

    public function setCodProvincia(?Provincias $codProvincia): self
    {
        $this->codProvincia = $codProvincia;

        return $this;
    }

    public function getCodGestor(): ?Gestores
    {
        return $this->codGestor;
    }

    public function setCodGestor(?Gestores $codGestor): self
    {
        $this->codGestor = $codGestor;

        return $this;
    }

    /**
     * @return Collection|Compras[]
     */
    public function getCompras(): Collection
    {
        return $this->compras;
    }

    public function addCompra(Compras $compra): self
    {
        if (!$this->compras->contains($compra)) {
            $this->compras[] = $compra;
            $compra->setCodEmpresa($this);
        }

        return $this;
    }

    public function removeCompra(Compras $compra): self
    {
        if ($this->compras->removeElement($compra)) {
            // set the owning side to null (unless already changed)
            if ($compra->getCodEmpresa() === $this) {
                $compra->setCodEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DetalleRegistroSalarial[]
     */
    public function getDetallesRegistroSalarial(): Collection
    {
        return $this->detallesRegistroSalarial;
    }

    public function addDetallesRegistroSalarial(DetalleRegistroSalarial $detallesRegistroSalarial): self
    {
        if (!$this->detallesRegistroSalarial->contains($detallesRegistroSalarial)) {
            $this->detallesRegistroSalarial[] = $detallesRegistroSalarial;
            $detallesRegistroSalarial->setCodEmpresa($this);
        }

        return $this;
    }

    public function removeDetallesRegistroSalarial(DetalleRegistroSalarial $detallesRegistroSalarial): self
    {
        if ($this->detallesRegistroSalarial->removeElement($detallesRegistroSalarial)) {
            // set the owning side to null (unless already changed)
            if ($detallesRegistroSalarial->getCodEmpresa() === $this) {
                $detallesRegistroSalarial->setCodEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Trabajador[]
     */
    public function getTrabajadores(): Collection
    {
        return $this->trabajadores;
    }

    public function addTrabajadore(Trabajador $trabajadore): self
    {
        if (!$this->trabajadores->contains($trabajadore)) {
            $this->trabajadores[] = $trabajadore;
            $trabajadore->setCodEmpresa($this);
        }

        return $this;
    }

    public function removeTrabajadore(Trabajador $trabajadore): self
    {
        if ($this->trabajadores->removeElement($trabajadore)) {
            // set the owning side to null (unless already changed)
            if ($trabajadore->getCodEmpresa() === $this) {
                $trabajadore->setCodEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConvenioColectivo[]
     */
    public function getConveniosColectivos(): Collection
    {
        return $this->conveniosColectivos;
    }

    public function addConveniosColectivo(ConvenioColectivo $conveniosColectivo): self
    {
        if (!$this->conveniosColectivos->contains($conveniosColectivo)) {
            $this->conveniosColectivos[] = $conveniosColectivo;
            $conveniosColectivo->setCodEmpresa($this);
        }

        return $this;
    }

    public function removeConveniosColectivo(ConvenioColectivo $conveniosColectivo): self
    {
        if ($this->conveniosColectivos->removeElement($conveniosColectivo)) {
            // set the owning side to null (unless already changed)
            if ($conveniosColectivo->getCodEmpresa() === $this) {
                $conveniosColectivo->setCodEmpresa(null);
            }
        }

        return $this;
    }

    public function getDatosFacturacion(): ?DatosFacturacion
    {
        return $this->datosFacturacion;
    }

    public function setDatosFacturacion(?DatosFacturacion $datosFacturacion): self
    {
        // unset the owning side of the relation if necessary
        if ($datosFacturacion === null && $this->datosFacturacion !== null) {
            $this->datosFacturacion->setCodEmpresa(null);
        }

        // set the owning side of the relation if necessary
        if ($datosFacturacion !== null && $datosFacturacion->getCodEmpresa() !== $this) {
            $datosFacturacion->setCodEmpresa($this);
        }

        $this->datosFacturacion = $datosFacturacion;

        return $this;
    }

    public function getTipoLicencia(): ?string
    {
        return $this->tipoLicencia;
    }

    public function setTipoLicencia(?string $tipoLicencia): self
    {
        $this->tipoLicencia = $tipoLicencia;

        return $this;
    }

    /**
     * @return Collection|CorreoSoporte[]
     */
    public function getCorreoSoportes(): Collection
    {
        return $this->correoSoportes;
    }

    public function addCorreoSoporte(CorreoSoporte $correoSoporte): self
    {
        if (!$this->correoSoportes->contains($correoSoporte)) {
            $this->correoSoportes[] = $correoSoporte;
            $correoSoporte->setCodEmpresa($this);
        }

        return $this;
    }

    public function removeCorreoSoporte(CorreoSoporte $correoSoporte): self
    {
        if ($this->correoSoportes->removeElement($correoSoporte)) {
            // set the owning side to null (unless already changed)
            if ($correoSoporte->getCodEmpresa() === $this) {
                $correoSoporte->setCodEmpresa(null);
            }
        }

        return $this;
    }

    public function getRegistroHorario(): ?bool
    {
        return $this->registroHorario;
    }

    public function setRegistroHorario(bool $registroHorario): self
    {
        $this->registroHorario = $registroHorario;

        return $this;
    }
}
