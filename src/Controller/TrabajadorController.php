<?php

namespace App\Controller;

use App\Entity\ConvenioColectivo;
use App\Entity\DetalleRegistroSalarial;
use App\Entity\GrupoProfesional;
use App\Entity\Trabajador;
use App\Entity\Empresa;
use App\Entity\Gestores;
use App\Entity\MotivoReduccion;
use App\Entity\RegistroHorario;
use App\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Validator\Constraints\Regex;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Finder\Finder;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class TrabajadorController extends AbstractController
{

    /**
     * @Route("/trabajador/{codEmpresa}/{codTrabajador}/{tipo}/entradaSalida", name="entrada_salida")
     */
    public function guardarEntradaSalida(int $codEmpresa, int $codTrabajador, string $tipo, Request $request)
    {
        $trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->findOneBy([
            'id' => $codTrabajador
        ]);

        $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
            'id' => $codEmpresa
        ]);

        $entityManager = $this->getDoctrine()->getManager();

        if($tipo == 'e'){
            $registro = new RegistroHorario();

            $registro->setCodTrabajador($trabajador);
            $registro->setCodEmpresa($empresa);

            $registro->setHoraEntrada(time());
            $tipoDato ='entrada';
        }else if($tipo == 's'){
            $registro = $this->getDoctrine()->getRepository(RegistroHorario::class)->findOneBy([
                'codTrabajador' => $trabajador,
                'codEmpresa' => $empresa,
                'horaSalida' => null
            ]);

            $registro->setHoraSalida(time()); 
            $tipoDato = 'salida';

            $registro->setTiempoTotal($registro->getHoraSalida() - $registro->getHoraEntrada());
        }

        $entityManager->persist($registro);

        $entityManager->flush();

        $this->get('session')->set('templateURL', '/registroHorario/inicio.html.twig');
        $this->get('session')->set('page', 'paginaRegistroHorario');

        $request->query->remove('codEmpresa');
        $request->query->remove('codTrabajador');
        $request->query->remove('tipo');
            
        return $this->redirectToRoute('ya_logado_trabajador', array(
            'guardado' => true,
            'tipoDato' => $tipoDato
        ));        
    }

    /**
     * @Route("/trabajador", name="ya_logado_trabajador")
     */
    public function login(Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $trabajador = $this->getUser();
        if($trabajador->getPrimeraClave()){
            
            $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => ['attr' => ['placeholder' => 'Contraseña']],
                'second_options' => ['attr' => ['placeholder' => 'Repetir Contraseña']],
                'invalid_message' => 'Los campos de contraseña deben ser iguales',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?=.{6})(?=.*[A-Z])(?=.*[a-z])/',
                        'message' => 'La contraseña debe tener al menos 6 carácteres y al menos una letra mayúscula y otra minúscula.',
                    ]),
                ],
            ))
            ->add('save', SubmitType::class, array('label' => 'Guardar'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $formData = $form->getData();

                $trabajador->setPassword(password_hash($formData['password'], PASSWORD_BCRYPT));
                $trabajador->setPrimeraClave(0);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($trabajador);
                $entityManager->flush();
            }
            
            return $this->render('trabajador/primerLogin.html.twig', array(
                'form' => $form->createView(),
            ));
        }else{
            $this->get('session')->set('templateURL', '/registroHorario/inicio.html.twig');
            $this->get('session')->set('page', 'paginaRegistroHorario');

            $listaHorarios = $this->getDoctrine()->getRepository(RegistroHorario::class)->findBy([
                'codTrabajador' => $trabajador,
                'codEmpresa' => $trabajador->getCodEmpresa()
            ],
            [
                'id' => 'DESC'
            ],
            15);

            $entrada = true;
            if(!empty($listaHorarios)){
                if(empty($listaHorarios[0]->getHoraSalida())){
                    $entrada = false;
                }
            }
            return $this->render('dashboard/inicio.html.twig', array(
                'trabajador' => $trabajador,
                'empresa' => $trabajador->getCodEmpresa(),
                'lista' => $listaHorarios,
                'entrada' => $entrada
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/{codTrabajador}/informeExcelHorario",  name="informe_excel_horario")
     */
    public function informeExcelHorario(int $codEmpresa, int $codTrabajador, Utils $utiles)
    {        
        $registroHorario = $this->getDoctrine()->getRepository(RegistroHorario::class)->findBy([
            'codEmpresa' => $codEmpresa,
            'codTrabajador' => $codTrabajador
        ],[
            'id' => 'DESC'
        ]);

        if (!empty($registroHorario)) {

            $trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->findOneBy([
                'id' => $codTrabajador
            ]);

            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'id' => $codEmpresa
            ]);

            //Datos de la bbdd

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load('../public/uploads/Plantilla_horarios.xlsx');

            // Sheet CONC.RETR
            $sheet = $spreadsheet->setActiveSheetIndex(0);

            $fila = 2;

            foreach($registroHorario as $registro){

                $sheet->setCellValue('A'.$fila, date("d/m/Y", $registro->getHoraEntrada()));
                $sheet->setCellValue('B'.$fila, ($trabajador->getNombre())." ".($trabajador->getApellidos()));
                $sheet->setCellValue('C'.$fila, $trabajador->getDni());
                $sheet->setCellValue('D'.$fila, $empresa->getNombre());
                $sheet->setCellValue('E'.$fila, date("H:i:s", $registro->getHoraEntrada()));
                
                $horaFin = "---";

                $horas = 0;
                $minutos = 0;

                if(!empty($registro->getHoraSalida())){
                    $horaFin = date("H:i:s", $registro->getHoraSalida());

                    if($registro->getTiempoTotal() > 0){
                        $horas = intdiv ( $registro->getTiempoTotal() , 3600);
                    }

                    $minutos = intdiv (($registro->getTiempoTotal() % 3600) , 60);
                }

                $tiempoTotal = $horas.":".$minutos." H";

                $sheet->setCellValue('F'.$fila, $horaFin);
                $sheet->setCellValue('G'.$fila, $tiempoTotal);

                $fila++;
                                   
            }            
            
            //Con esto sacamos por pantalla el informe 
            $writer = new Xlsx($spreadsheet);
            $writer->setPreCalculateFormulas(false);

            $response =  new StreamedResponse(
                function () use ($writer) {
                    $writer->save('php://output');
                }
            );

            $response->headers->set('Content-Type', 'application/vnd.ms-excel');
            $response->headers->set('Content-Disposition', 'attachment;filename=registro horario '.($trabajador->getNombre()).' '.($trabajador->getApellidos()).'.xlsx');
            $response->headers->set('Cache-Control','max-age=0');

            return $response;
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Aún no se ha registrado control horario del trabajador."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/{codTrabajador}/{fechaInicio}/{fechaFin}/informeExcelHorarioFecha",  name="informe_excel_horario_empresa")
     */
    public function informeExcelHorarioEmpresa(int $codEmpresa, int $codTrabajador, int $fechaInicio, int $fechaFin)
    {        
        
        $registroHorario = $this->getDoctrine()->getRepository(RegistroHorario::class)->findByDate($codTrabajador, $codEmpresa, $fechaInicio, $fechaFin);
        
        if (!empty($registroHorario)) {

            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'id' => $codEmpresa
            ]);

            //Datos de la bbdd

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load('../public/uploads/Plantilla_horarios.xlsx');

            // Sheet CONC.RETR
            $sheet = $spreadsheet->setActiveSheetIndex(0);

            $fila = 2;

            foreach($registroHorario as $registro){

                $sheet->setCellValue('A'.$fila, date("d/m/Y", $registro->getHoraEntrada()));
                $sheet->setCellValue('B'.$fila, ($registro->getCodTrabajador()->getNombre())." ".($registro->getCodTrabajador()->getApellidos()));
                $sheet->setCellValue('C'.$fila, $registro->getCodTrabajador()->getDni());
                $sheet->setCellValue('D'.$fila, $empresa->getNombre());
                $sheet->setCellValue('E'.$fila, date("H:i:s", $registro->getHoraEntrada()));
                
                $horaFin = "---";

                $horas = 0;
                $minutos = 0;

                if(!empty($registro->getHoraSalida())){
                    $horaFin = date("d/m/Y H:i:s", $registro->getHoraSalida());

                    if($registro->getTiempoTotal() > 0){
                        $horas = intdiv ( $registro->getTiempoTotal() , 3600);
                    }

                    $minutos = intdiv (($registro->getTiempoTotal() % 3600) , 60);
                }

                $tiempoTotal = $horas.":".$minutos." H";

                $sheet->setCellValue('F'.$fila, $horaFin);
                $sheet->setCellValue('G'.$fila, $tiempoTotal);

                $fila++;
                                   
            }            
            
            //Con esto sacamos por pantalla el informe 
            $writer = new Xlsx($spreadsheet);
            $writer->setPreCalculateFormulas(false);

            $response =  new StreamedResponse(
                function () use ($writer) {
                    $writer->save('php://output');
                }
            );

            $response->headers->set('Content-Type', 'application/vnd.ms-excel');
            
            //if($codTrabajador==0){
                $response->headers->set('Content-Disposition', 'attachment;filename=registro horario del '.date('d-m-Y', $fechaInicio).' a '.date('d-m-Y', $fechaFin).'.xlsx');
            /*}else{
                $response->headers->set('Content-Disposition', 'attachment;filename=registro horario '.($empresa->getNombre()).' del trabajador '.$trabajador->getNombre()." ".$trabajador->getApellidos().' de '.date('d-m-Y', $fechaInicio).' a '.date('d-m-Y', $fechaFin).'.xlsx');
            }*/
            $response->headers->set('Cache-Control','max-age=0');

            return $response;
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Aún no se ha registrado control horario del trabajador."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/nuevoTrabajador", name="crear_trabajador")
     */
    public function nuevoTrabajador(Request $request, int $codEmpresa, Utils $utiles, MailerInterface $mailer): Response
    {
        $this->get('session')->set('templateURL', 'trabajador/alta.html.twig');
        $this->get('session')->set('page', 'altaTrabajador');

        $loggedUser = $this->getUser();

        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }

        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {

            $this->get('session')->set('registroHorario', $empresa->getRegistroHorario());

            $trabajador = new Trabajador();
            $convenio = new ConvenioColectivo();

            $random_passwd = $utiles->generateStrongPassword();
            $trabajador->setPassword($random_passwd);

            $array_otro = array('Otro' => '0');
            $convenio_array = $this->getDoctrine()->getRepository(ConvenioColectivo::class)->findByCodEmpresa($codEmpresa);

            $array_final = array();
            foreach($convenio_array as $convenio_final){
                $array_final[$convenio_final['nombreConvenio']] = $convenio_final['id'];
            }
            $array_final = array_merge($array_final,$array_otro);

            $form = $this->createFormBuilder();

            $form = self::generaFormTrabajador($empresa->getRegistroHorario(), $array_final, $form); 
            
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                
                $entityManager = $this->getDoctrine()->getManager();
                $formData = $form->getData();

                if(isset($formData['tipoConvenio'])){

                    $convenio = self::crearConvenioMapper($formData, $convenio, $empresa);
                    $entityManager->persist($convenio);

                    $entityManager->flush();

                    $bbdd = $this->getDoctrine()->getRepository(ConvenioColectivo::class)->findOneBy([
                        'codEmpresa'     => $codEmpresa,
                        'nombreConvenio' => $formData['tipoConvenio']
                    ]); 

                    $trabajador->setCodConvenio($bbdd);
                }

                $trabajador = self::crearTrabajadorMapper($formData, $trabajador, $entityManager, $empresa);

                if($empresa->getRegistroHorario()){
                    $utiles->sendPassTrabajadorEmail($mailer, $trabajador->getCodEmpresa()->getNombre(), $trabajador->getNombre(), $trabajador->getApellidos(), $trabajador->getEmail(), $trabajador->getPassword());
                }

                $trabajador->setPassword(password_hash($trabajador->getPassword(), PASSWORD_BCRYPT));

                $entityManager->persist($trabajador);
                $entityManager->flush();

                /*$fechaInicioContrato =  new \DateTime('@' . $trabajador->getFechaInicioEmpresa());

                $esBaja = false;

                if(!is_null($formData['fechaFinEmpresa']) && $trabajador->getFechaFinEmpresa() < time()){
                    $fechaFinContrato = new \DateTime('@' . $trabajador->getFechaFinEmpresa());
                    $esBaja = true;
                }else{
                    $fechaFinContrato = new \DateTime();
                }
                
                if($trabajador->getFechaInicioEmpresa() < time() - (30*34*3600) || $trabajador->getFechaFinEmpresa() < time()){
                    self::rellenaDetallesSalario($trabajador, $entityManager, $fechaInicioContrato, $fechaFinContrato, $esBaja, $utiles);
                }*/

                $form = $this->createFormBuilder();
                $form = self::generaFormTrabajador($empresa->getRegistroHorario(), $array_final, $form);

                return $this->render('dashboard/inicio.html.twig', array(
                    'form' => $form->createView(),
                    'empresa' => $empresa,
                    'creada' => true
                ));
                
            }

            return $this->render('dashboard/inicio.html.twig', array(
                'form' => $form->createView(),
                'empresa' => $empresa,
                'creada' => ''
            ));
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/trabajador/{codEmpresa}/{codTrabajador}/perfil", name="perfil_trabajador")
     */
    public function perfil(Request $request, int $codEmpresa, int $codTrabajador): Response
    {
        $this->get('session')->set('templateURL', 'trabajador/perfil.html.twig');
        $this->get('session')->set('page', 'perfilTrabajador');

        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {

            $trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->findOneBy([
                'id' => $codTrabajador,
                'codEmpresa' => $empresa,
                ]);

            if(!empty($trabajador)){

                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($trabajador->getCodEmpresa());

                $dtInicio = new \DateTime('@' . $trabajador->getFechaInicioEmpresa());

                $dtBaja = null;

                if(!is_null($trabajador->getFechaFinEmpresa())){
                    $dtBaja = new \DateTime('@' . $trabajador->getFechaFinEmpresa());
                }
                
                $array_otro = array('Otro' => '0');
                $convenio_array = $this->getDoctrine()->getRepository(ConvenioColectivo::class)->findByCodEmpresa($codEmpresa);

                $array_final = array();
                foreach($convenio_array as $convenio_final){
                    $array_final[$convenio_final['nombreConvenio']] = $convenio_final['id'];
                }
                $array_final = array_merge($array_final,$array_otro);

                $codigoConvenio = "";


                if(!empty($trabajador->getCodConvenio())){
                    $codigoConvenio = $trabajador->getCodConvenio()->getId();
                }

                $form = $this->createFormBuilder()
                    ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre', 'empty_data' => $trabajador->getNombre()]]) 
                    ->add('apellidos', TextType::class, ['attr' => ['placeholder' => 'Nombre', 'empty_data' => $trabajador->getApellidos()]])
                    ->add('dni', TextType::class, ['attr' => ['placeholder' => 'DNI/NIE', 'empty_data' => $trabajador->getDni()]])
                    ->add('puesto', TextType::class, ['attr' => ['placeholder' => 'Puesto del trabajador', 'empty_data' => $trabajador->getPuesto()]])
                    ->add('codConvenio', ChoiceType::class, array(
                            'choices' => $array_final,
                            'placeholder' => '-- Elija un Convenio Colectivo --',
                            'data'  => $codigoConvenio,
                        )
                    )
                    ->add('tipoConvenio', TextType::class)
                    //->add('salarioConvenio', IntegerType::class)
                    ->add('email', TextType::class, ['attr' => ['placeholder' => 'Dirección de correo', 'empty_data' => $trabajador->getEmail()]])
                    ->add('fechaInicioEmpresa', DateType::class, [
                        'widget'=> 'single_text',
                        // this is actually the default format for single_text
                        'attr'  => ['class' => 'js-datepicker'],
                        'data'  => $dtInicio,
                    ])
                    ->add('fechaFinEmpresa', DateType::class, [
                        'widget'=> 'single_text',
                        'attr'  => ['class' => 'js-datepicker'],
                        'data'  => $dtBaja,
                        'required' => false
                    ])
                    //->add('salarioAnual', IntegerType::class, ['attr' => ['empty_data' => $trabajador->getSalarioAnual()]])
                    ->add('numPagas', ChoiceType::class, [
                        'choices'  => [
                            '12' => '12',
                            '14' => '14',
                        ],
                        'data' => $trabajador->getNumPagas(),
                    ])
                    ->add('sexo', ChoiceType::class, [
                        'choices'  => [
                            'Hombre' => 'H',
                            'Mujer' => 'M',
                        ],
                        'data' => $trabajador->getSexo(),
                    ])
                    ->add('grupo', EntityType::class, [
                        'class' => GrupoProfesional::class,
                        'choice_label' => function ($grupo) {
                            return $grupo->getGrupo() ." - ". $grupo->getNombreGrupo();
                        },
                        'data' => $trabajador->getGrupo(),
                    ])
                    ->add('jornadaReducida', CheckboxType::class, ['required' => false])
                    ->add('motivoReduccion', EntityType::class, array(
                        'class' => MotivoReduccion::class,
                        'choice_label' => function($grupo){
                                return $grupo->getDescripcion();
                        },
                        'data' => $trabajador->getCodMotivoReduccion(),
                        'placeholder' => '-- Elija el Motivo de la Reducción --'
                        )
                    )
                    ->add('contrato', ChoiceType::class, [
                        'choices'  => [
                            '100 - Indefinido' => '100',
                            '109 - Indefinido' => '109',
                            '130 - Indefinido' => '130',
                            '131 - Indefinido' => '131',
                            '139 - Indefinido' => '139',
                            '141 - Indefinido' => '141',
                            '150 - Indefinido' => '150',
                            '151 - Indefinido' => '151',
                            '189 - Indefinido' => '189',
                            '200 - Indefinido' => '200',
                            '209 - Indefinido' => '209',
                            '230 - Indefinido' => '230',
                            '231 - Indefinido' => '231',
                            '239 - Indefinido' => '239',
                            '241 - Indefinido' => '241',
                            '250 - Indefinido' => '250',
                            '251 - Indefinido' => '251',
                            '289 - Indefinido' => '289',
                            '300 - Indefinido' => '300',
                            '309 - Indefinido' => '309',
                            '330 - Indefinido' => '330',
                            '331 - Indefinido' => '331',
                            '350 - Indefinido' => '350',
                            '351 - Indefinido' => '351',
                            '389 - Indefinido' => '389',
                            '401 - Duración determinada' => '401',
                            '402 - Duración determinada' => '402',
                            '403 - Duración determinada' => '403',
                            '408 - Temporal' => '408',
                            '410 - Duración temporal' => '410',
                            '418 - Duración determinada' => '418',
                            '420 - Temporal' => '420',
                            '421 - Temporal' => '421',
                            '430 - Temporal' => '430',
                            '431 - Temporal' => '431',
                            '441 - Temporal' => '441',
                            '451 - Temporal' => '451',
                            '501 - Duración determinada' => '501',
                            '502 - Duración determinada' => '502',
                            '503 - Duración determinada' => '503',
                            '508 - Temporal' => '508',
                            '510 - Duración determinada' => '510',
                            '518 - Duración determinada' => '518',
                            '520 - Temporal' => '520',
                            '530 - Temporal' => '530',
                            '531 - Temporal' => '531',
                            '540 - Temporal' => '540',
                            '541 - Temporal' => '541',
                            '551 - Temporal' => '551',
                        ],
                        'data' => $trabajador->getCodigoContrato(),
                        'required' => false                        
                    ])
                    ->add('porcentajeReducida', IntegerType::class, ['attr' => ['empty_data' => $trabajador->getPorcentajeReducida()]])
                    ->add('porcentajeJornada', IntegerType::class, ['attr' => ['empty_data' => $trabajador->getPorcentajeJornada()]])
                    ->add('save', SubmitType::class, ['label' => 'Modificar datos'])
                    ->getForm();

                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {

                    $convenio = new ConvenioColectivo();
                
                    $entityManager = $this->getDoctrine()->getManager();
                    $formData = $form->getData();

                    if(isset($formData['tipoConvenio'])){

                        $convenio = self::crearConvenioMapper($formData, $convenio, $empresa);
                        $entityManager->persist($convenio);
    
                        $entityManager->flush();
    
                        $bbdd = $this->getDoctrine()->getRepository(ConvenioColectivo::class)->findOneBy([
                            'codEmpresa'     => $codEmpresa,
                            'nombreConvenio' => $formData['tipoConvenio']
                        ]); 
    
                        $trabajador->setCodConvenio($bbdd);
                    }

                    $trabajador = self::modificaTrabajadorMapper($formData, $trabajador, $entityManager);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->flush();

                    return $this->redirectToRoute('perfil_trabajador', array(
                        'modificado' => true,
                        'codTrabajador' => $trabajador->getId(),
                        'codEmpresa'    => $empresa->getId(),
                    ));
                }

                return $this->render('dashboard/inicio.html.twig', array(
                    'form' => $form->createView(),
                    'modificado' => '',
                    'trabajador' => $trabajador,
                    'empresa'    => $empresa,
                ));

            }else{
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');
            
                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "Los datos del trabajador introducido no son correctos."
                ));
            }
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    private static function modificaTrabajadorMapper($formData, $trabajador, $entityManager){

        $fechaInicioEmpresa = $formData['fechaInicioEmpresa']->getTimestamp();

        if(!is_null($formData['fechaFinEmpresa'])){
            $fechaFinEmpresa =  $formData['fechaFinEmpresa']->getTimestamp();
        }

        $trabajador->setNombre($formData['nombre']);
        $trabajador->setApellidos($formData['apellidos']);
        
        if(!is_null($formData['dni'])){
            $trabajador->setDni($formData['dni']);
        }

        $trabajador->setPuesto($formData['puesto']);
        $trabajador->setFechaInicioEmpresa($fechaInicioEmpresa);

        if(isset($fechaFinEmpresa) && $fechaFinEmpresa > $fechaInicioEmpresa){
            $trabajador->setFechaFinEmpresa($fechaFinEmpresa);
        }
        
        if($formData['codConvenio'] != 0){
            $codConvenio = $entityManager->getRepository(ConvenioColectivo::class)->find($formData['codConvenio']);
            $trabajador->setCodConvenio($codConvenio);
        }

        //$trabajador->setSalarioAnual($formData['salarioAnual']);
        $trabajador->setNumPagas($formData['numPagas']);
        $trabajador->setSexo($formData['sexo']);
        $trabajador->setGrupo($formData['grupo']);
        $trabajador->setPorcentajeReducida($formData['porcentajeReducida']);
        $trabajador->setPorcentajeJornada($formData['porcentajeJornada']);
        $trabajador->setJornadaReducida($formData['jornadaReducida']);
        $trabajador->setCodMotivoReduccion($formData['motivoReduccion']);
        if(!is_null($formData['email'])){
            $trabajador->setEmail($formData['email']);
        }
        $trabajador->setCodigoContrato($formData['contrato']);
        
        return $trabajador;

    }

    private static function crearTrabajadorMapper($formData, $trabajador, $entityManager, $codEmpresa){

        $fechaInicioEmpresa = $formData['fechaInicioEmpresa']->getTimestamp();

        if(!is_null($formData['fechaFinEmpresa'])){
            $fechaFinEmpresa =  $formData['fechaFinEmpresa']->getTimestamp();
        }

        $trabajador->setNombre($formData['nombre']);
        $trabajador->setApellidos($formData['apellidos']);
        
        if(!is_null($formData['dni'])){
            $trabajador->setDni($formData['dni']);
        }

        if($formData['codConvenio'] != 0){
            $codConvenio = $entityManager->getRepository(ConvenioColectivo::class)->find($formData['codConvenio']);
            $trabajador->setCodConvenio($codConvenio);
        }

        $trabajador->setPuesto($formData['puesto']);
        $trabajador->setFechaInicioEmpresa($fechaInicioEmpresa);

        if(isset($fechaFinEmpresa) && $fechaFinEmpresa > $fechaInicioEmpresa){
            $trabajador->setFechaFinEmpresa($fechaFinEmpresa);
            //TODO calcular parte proporcional del mes de la baja
        }
        
        //$trabajador->setSalarioAnual($formData['salarioAnual']);
        $trabajador->setNumPagas($formData['numpagas']);
        $trabajador->setSexo($formData['sexo']);
        $trabajador->setGrupo($formData['grupo']);
        $trabajador->setPorcentajeReducida($formData['porcentajeReducida']);
        $trabajador->setPorcentajeJornada($formData['porcentajeJornada']);
        $trabajador->setJornadaReducida($formData['jornadaReducida']);
        $trabajador->setCodMotivoReduccion($formData['motivoReduccion']);
        
        if(!is_null($formData['email'])){
            $trabajador->setEmail($formData['email']);
        }
        
        $trabajador->setRoles($trabajador->getRoles());
        $trabajador->setCodEmpresa($codEmpresa);
        $trabajador->setFechaAlta(time());
        $trabajador->setCodigoContrato($formData['contrato']);

        return $trabajador;

    }

    private static function crearConvenioMapper($formData, $convenio, $codEmpresa){

        $convenio->setNombreConvenio($formData['tipoConvenio']);
        $convenio->setCodEmpresa($codEmpresa);

        return $convenio;
    }

    private static function rellenaDetallesSalario($trabajador, $entityManager, $fechaInicio, $fechaFin, $esBaja, $utiles){
        
        $array31Dias = array('1','3','5','7','8','10','12');
        $array30Dias = array('4','6','9','11');

        $diaInicio = $fechaInicio->format('d');
        $mesInicio = $fechaInicio->format('m'); //1
        $anioInicio = $fechaInicio->format('Y'); //2019

        if($anioInicio < 2020){
            $diaInicio = 1;
            $mesInicio = 1;
            $anioInicio = 2020;    
        }

        $diaFin = $esBaja ? $fechaFin->format('d') : 0;

        $mesFin = $fechaFin->format('m');
        $anioFin = $fechaFin->format('Y');
        
        if(!$esBaja && $fechaFin->format('d') < 20){
            if($fechaFin->format('m') == 1){
                $mesFin = 12;
                $anioFin = $fechaFin->format('Y') - 1;
            }else{
                $mesFin = $fechaFin->format('m') - 1;
                $anioFin = $fechaFin->format('Y');
            }
        }

        $salarioMensualCompleto = $utiles->redondeado($trabajador->getSalarioAnual()/$trabajador->getNumPagas(), 2);

        $iteracion = 0;
        
        for($y = $anioInicio; $y <= $anioFin; $y++){
            
            $mesMaximo = ($y == $anioFin && 12 > $mesFin) ? $mesFin : 12;

            for($m = $mesInicio; $m <= $mesMaximo; $m++){
                
                $detalleSalarial = new DetalleRegistroSalarial();

                $detalleSalarial->setPuesto($trabajador->getPuesto());
                $detalleSalarial->setSexo($trabajador->getSexo());
                $detalleSalarial->setConcepto('salarioBase');
                $detalleSalarial->setDescripcion('Salario base');

                if($iteracion == 0 && $diaInicio > 1 || $m == $mesFin && $esBaja){

                    //Calculamos los dias del mes
                    $dias = in_array($m, $array31Dias) ? 31 : (in_array($m, $array30Dias) ? 30 : ($utiles->esBisiesto($y) ? 29 : 28));

                    if($m == $mesFin && $esBaja){
                        //Si es el ultimo mes y es baja el trabajador los dias que pagamos son hasta su baja

                        if($mesInicio == $mesFin && $diaInicio > 1){
                            $diasMes = ($diaFin - $diaInicio) + 1;
                        }else{
                            $diasMes = $diaFin;
                        }
                        
                    }else{
                        //Calculamos los dias que cobra del mes
                        $diasMes = ($dias - $diaInicio) + 1;
                    }                   
                    
                    //Calculamos el importe de esos dias
                    $salarioParcial = ($salarioMensualCompleto/$dias) * $diasMes;
                    $detalleSalarial->setImporte($utiles->redondeado($salarioParcial, 2));
                }else{
                    $detalleSalarial->setImporte($salarioMensualCompleto);
                }

                $detalleSalarial->setMes($m);
                $detalleSalarial->setAnio($y);
                $detalleSalarial->setCodEmpresa($trabajador->getCodEmpresa());
                $detalleSalarial->setCodTrabajador($trabajador);        
                $detalleSalarial->setGrupo($trabajador->getGrupo());
                $detalleSalarial->setRepetible(true);

                $entityManager->persist($detalleSalarial);

                if($trabajador->getNumPagas() == 14 && ($m == 7 || $m == 12)){
                    $detallePagaExtra = new DetalleRegistroSalarial();

                    $detallePagaExtra->setPuesto($trabajador->getPuesto());
                    $detallePagaExtra->setSexo($trabajador->getSexo());
                    $detallePagaExtra->setConcepto('pagaExtra');
                    $detallePagaExtra->setDescripcion('Paga Extra');
                    $detallePagaExtra->setImporte($salarioMensualCompleto);
                    $detallePagaExtra->setMes($m);
                    $detallePagaExtra->setAnio($y);
                    $detallePagaExtra->setCodEmpresa($trabajador->getCodEmpresa());
                    $detallePagaExtra->setCodTrabajador($trabajador);        
                    $detallePagaExtra->setGrupo($trabajador->getGrupo());

                    $entityManager->persist($detallePagaExtra);
                }

                $entityManager->flush();
                $iteracion++;
            }
        }
    }

    private static function generaFormTrabajador($registroHorario, $array_final, $form){

        $form->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre Trabajador']])
            ->add('apellidos', TextType::class, ['attr' => ['placeholder' => 'Apellidos Trabajador']])
            ->add('tipoConvenio', TextType::class, ['attr' => ['placeholder' => 'Nombre Convenio']])
            //->add('salarioConvenio', IntegerType::class, ['attr' => ['placeholder' => 'Salario Anual del Convenio']])
            //->add('salarioAnual', IntegerType::class, ['attr' => ['placeholder' => 'Salario Base Anual del Trabajador']])
            ->add('codConvenio', ChoiceType::class, array(
                    'choices' => $array_final,
                    'placeholder' => '-- Elija un Convenio Colectivo --'
                )
            )
            ->add('sexo', ChoiceType::class, array(
                'choices' => array(
                    'Hombre' => 'H',
                    'Mujer' => 'M')
                )
            )
            ->add('grupo', EntityType::class, array(
                'class' => GrupoProfesional::class,
                'choice_label' => function($grupo){
                        return $grupo->getGrupo().' - '.$grupo->getNombreGrupo();
                    },
                    'placeholder' => '-- Elija un Grupo Profesional --'
                )
            )
            ->add('jornadaReducida', CheckboxType::class, ['required' => false])
            ->add('motivoReduccion', EntityType::class, array(
                'class' => MotivoReduccion::class,
                'choice_label' => function($grupo){
                        return $grupo->getDescripcion();
                    },
                    'placeholder' => '-- Elija el Motivo de la Reducción --'
                )
            )
            ->add('contrato', ChoiceType::class, [
                'choices'  => [
                    '100 - Indefinido' => '100',
                    '109 - Indefinido' => '109',
                    '130 - Indefinido' => '130',
                    '131 - Indefinido' => '131',
                    '139 - Indefinido' => '139',
                    '141 - Indefinido' => '141',
                    '150 - Indefinido' => '150',
                    '151 - Indefinido' => '151',
                    '189 - Indefinido' => '189',
                    '200 - Indefinido' => '200',
                    '209 - Indefinido' => '209',
                    '230 - Indefinido' => '230',
                    '231 - Indefinido' => '231',
                    '239 - Indefinido' => '239',
                    '241 - Indefinido' => '241',
                    '250 - Indefinido' => '250',
                    '251 - Indefinido' => '251',
                    '289 - Indefinido' => '289',
                    '300 - Indefinido' => '300',
                    '309 - Indefinido' => '309',
                    '330 - Indefinido' => '330',
                    '331 - Indefinido' => '331',
                    '350 - Indefinido' => '350',
                    '351 - Indefinido' => '351',
                    '389 - Indefinido' => '389',
                    '401 - Duración determinada' => '401',
                    '402 - Duración determinada' => '402',
                    '403 - Duración determinada' => '403',
                    '408 - Temporal' => '408',
                    '410 - Duración temporal' => '410',
                    '418 - Duración determinada' => '418',
                    '420 - Temporal' => '420',
                    '421 - Temporal' => '421',
                    '430 - Temporal' => '430',
                    '431 - Temporal' => '431',
                    '441 - Temporal' => '441',
                    '451 - Temporal' => '451',
                    '501 - Duración determinada' => '501',
                    '502 - Duración determinada' => '502',
                    '503 - Duración determinada' => '503',
                    '508 - Temporal' => '508',
                    '510 - Duración determinada' => '510',
                    '518 - Duración determinada' => '518',
                    '520 - Temporal' => '520',
                    '530 - Temporal' => '530',
                    '531 - Temporal' => '531',
                    '540 - Temporal' => '540',
                    '541 - Temporal' => '541',
                    '551 - Temporal' => '551',
                ],
                'placeholder' => '-- Elija el tipo de contrato --',
                'required' => false
            ])
            ->add('porcentajeReducida', IntegerType::class, ['attr' => ['placeholder' => '% Total Jornada']])
            ->add('porcentajeJornada', IntegerType::class, ['attr' => ['empty_data' => 100]])
            ->add('puesto', TextType::class, ['attr' => ['placeholder' => 'Puesto del Trabajdor en la Empresa']])
            ->add('numpagas', ChoiceType::class, array(
                'choices' => array(
                    '12' => '12',
                    '14' => '14')
                )
            )
            ->add('fechaInicioEmpresa', DateType::class, array(
                'attr' => array(
                    'class' => 'js-datepicker'
                ),
                'widget' => 'single_text'
                )
            )
            ->add('fechaFinEmpresa', DateType::class, array(
                'attr' => array(
                    'class' => 'js-datepicker'
                ),
                'widget' => 'single_text',
                'required' => false
                )
            );

        if($registroHorario){
            $form
            ->add('dni', TextType::class, ['attr' => ['placeholder' => 'NIF/NIE Trabajador']])
            ->add('email', TextType::class, ['attr' => ['placeholder' => 'Email del Trabajador']]);
        }else{
            $form
            ->add('dni', TextType::class, ['attr' => ['placeholder' => 'NIF/NIE Trabajador'], 'required' => false])
            ->add('email', TextType::class, ['attr' => ['placeholder' => 'Email del Trabajador'], 'required' => false]);
        }

        return $form->add('save', SubmitType::class, ['label' => 'Guardar trabajador'])->getForm();
    }
}
