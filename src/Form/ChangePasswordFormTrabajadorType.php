<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class ChangePasswordFormTrabajadorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => ['attr' => ['placeholder' => 'Contraseña']],
                'second_options' => ['attr' => ['placeholder' => 'Repetir Contraseña']],
                'invalid_message' => 'Los campos de contraseña deben ser iguales',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?=.{6})(?=.*[A-Z])(?=.*[a-z])/',
                        'message' => 'La contraseña debe tener al menos 6 carácteres y al menos una letra mayúscula y otra minúscula.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
