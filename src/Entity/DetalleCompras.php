<?php

namespace App\Entity;

use App\Repository\DetalleComprasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DetalleComprasRepository::class)
 */
class DetalleCompras
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Compras::class, inversedBy="detalleCompras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codCompra;

    /**
     * @ORM\Column(type="smallint")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity=Producto::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $idProducto;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCompra(): ?Compras
    {
        return $this->codCompra;
    }

    public function setCodCompra(?Compras $codCompra): self
    {
        $this->codCompra = $codCompra;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getIdProducto(): ?Producto
    {
        return $this->idProducto;
    }

    public function setIdProducto(?Producto $idProducto): self
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }
}
