<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */
class ValidateIdentityNumber extends Constraint
{
    public $message = 'El DNI/CIF/NIF "{{ string }}" introducido no es válido.';
}