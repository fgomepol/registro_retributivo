<?php

namespace App\Entity;

use App\Repository\ProvinciasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProvinciasRepository::class)
 */
class Provincias
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity=CCAA::class, inversedBy="provincias")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codComunidad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCodComunidad(): ?CCAA
    {
        return $this->codComunidad;
    }

    public function setCodComunidad(?CCAA $codComunidad): self
    {
        $this->codComunidad = $codComunidad;

        return $this;
    }
}
