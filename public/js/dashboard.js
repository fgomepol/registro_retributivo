$(document).ready(function () {
    
    $("#result").hide();
    $("#result").removeClass("alert-success");
    $("#result").removeClass("alert-danger");

    //Calcular IBAN de cuenta
        
    $("#calculaIBAN").validate({
        rules: {
            'calculaIBAN[pais]': "required",
            'calculaIBAN[banco]': "required",
            'calculaIBAN[oficina]': "required",
            'calculaIBAN[dc]': "required",
            'calculaIBAN[cuenta]': "required"
        },
        messages: {
            'calculaIBAN[pais]': "Debe seleccionar el país",
            'calculaIBAN[banco]': "Introduzca el código de banco",
            'calculaIBAN[oficina]': "Introduzca la oficina bancaria",
            'calculaIBAN[dc]': "Introduzca su dígito de control",
            'calculaIBAN[cuenta]': "Introduzca el código de cuenta"
        },
        submitHandler: function(form) {

            $("#result").removeClass("alert-success");
            $("#result").removeClass("alert-danger");
            
            var cuenta = $(form).find('input[name="calculaIBAN[banco]"]').val() + $(form).find('input[name="calculaIBAN[oficina]"]').val() + $(form).find('input[name="calculaIBAN[dc]"]').val() + $(form).find('input[name="calculaIBAN[cuenta]"]').val();
            var cuenta_formateada = $(form).find('input[name="calculaIBAN[banco]"]').val() +' '+ $(form).find('input[name="calculaIBAN[oficina]"]').val() +' '+ $(form).find('input[name="calculaIBAN[dc]"]').val() +' '+ $(form).find('input[name="calculaIBAN[cuenta]"]').val();
            var pais = $('select[name="calculaIBAN[pais]"]').children("option:selected").val();

            var iban = calcularIBAN(cuenta, pais);
            
            if(iban == ""){
                $("#result").show();
                $("#result").addClass("alert-danger");
                $("#result").text('El número de cuenta introducido no es correcto.');
            }else{
                $("#result").show();
                $("#result").addClass("alert-success");
                $("#result").text('El IBAN de cuenta introducida es: ' + iban +' '+ cuenta_formateada );
            }
        }
    });

    $('#botonCalcular').click(function() {
        if($("#calculaIBAN").valid()){
            $("#calculaIBAN").submit();
        }
    });

    //Validar cuenta IBAN

    $("form[name='validaIBAN']").validate({
        rules: {
            'validaIBAN[iban]': "required",
            'validaIBAN[banco]': "required",
            'validaIBAN[oficina]': "required",
            'validaIBAN[dc]': "required",
            'validaIBAN[cuenta]': "required"
        },
        messages: {
            'validaIBAN[iban]': "Introduzca su código de IBAN",
            'validaIBAN[banco]': "Introduzca el código de banco",
            'validaIBAN[oficina]': "Introduzca la oficina bancaria",
            'validaIBAN[dc]': "Introduzca su dígito de control",
            'validaIBAN[cuenta]': "Introduzca el código de cuenta"
        },
        submitHandler: function(form) {

            $("#result").removeClass("alert-success");
            $("#result").removeClass("alert-danger");

            var iban = $(form).find('input[name="validaIBAN[iban]"]').val() + $(form).find('input[name="validaIBAN[banco]"]').val() + $(form).find('input[name="validaIBAN[oficina]"]').val() + $(form).find('input[name="validaIBAN[dc]"]').val() + $(form).find('input[name="validaIBAN[cuenta]"]').val();
            if(fn_ValidateIBAN(iban)){
                $("#result").show();
                $("#result").addClass("alert-success");
                $("#result").text('El número de cuenta con IBAN introducido ' + iban + ' es correcto.');
            }else{
                $("#result").show();
                $("#result").addClass("alert-danger");
                $("#result").text('El número de cuenta con IBAN introducido ' + iban + ' no es correcto.');
            }
        }
    });

    //Calculadora Facturas

    $('#baseImponible').on('change', function () {
        var base = $(this).val();
        $(this).val(parseFloat(base).toFixed(2) + " €");
        var iva  = $('select[name="iva"]').children("option:selected").val();
        var irpf = $('select[name="irpf"]').children("option:selected").val();
        
        var retencion = 0;
        var precioIva = base;
        var importeTotal = 0;

        $('#impuestoiva').val(0.00 + " €");
        $('#retencionirpf').val(0.00 + " €");

        if(iva > 0){
            $('#impuestoiva').val(parseFloat(base * (iva/100)).toFixed(2) + " €");
            var precioIva = parseFloat(base) + parseFloat(base * (iva/100));
            $('#precioiva').val(parseFloat(precioIva).toFixed(2) + " €");
        }

        if(irpf > 0){
            $('#retencionirpf').val(parseFloat(base * ( irpf/100)).toFixed(2) + " €");
            var retencion = base * (irpf/100);
        }

        importeTotal = precioIva - retencion;

        $('#importeTotal').val(parseFloat(importeTotal).toFixed(2) + " €");
    });

    $('#importeTotal').on('change', function () {
        var importeTotal = $(this).val();
        $(this).val(parseFloat(importeTotal).toFixed(2) + " €");

        var iva  = $('select[name="iva"]').children("option:selected").val();
        var irpf = $('select[name="irpf"]').children("option:selected").val();
        
        var diferencial = 1 + (iva/100) - (irpf/100);
        var base = importeTotal / diferencial;
        
        if(iva > 0){
            $('#impuestoiva').val(parseFloat(base * ( iva/100)).toFixed(2) + " €");
            var precioIva = base + (base * ( iva/100));
            $('#precioiva').val(parseFloat(precioIva).toFixed(2) + " €");
        }

        if(irpf > 0){
            $('#retencionirpf').val(parseFloat(base * ( irpf/100)).toFixed(2) + " €");
        }

        $('#baseImponible').val(parseFloat(base).toFixed(2) + " €");
    });

    //Buscador CNAE

    $('#botonBuscarCNAE').click(function() {
        if($("#buscaCNAE").valid()){
            $("#buscaCNAE").submit();
        }
    });

    $("form[name='buscaCNAE']").validate({
        rules: {
            'buscaCNAE[tipoBusqueda]': "required",
            'buscaCNAE[criterio]': "required"
        },
        messages: {
            'buscaCNAE[tipoBusqueda]': "Seleccione el tipo de búsqueda",
            'buscaCNAE[criterio]': "Introduzca el criterio para buscar el CNAE"
        },
        submitHandler: function(form) {

            var tipoBusqueda  = $('select[name="buscaCNAE[tipoBusqueda]"]').children("option:selected").val();
            var criterio  = $('#criterioCNAE').val();

            console.log(tipoBusqueda + " " + criterio);

            $.ajax({
                type: "GET",
                url: '/gestores/buscaCNAE/' + tipoBusqueda + '/' + criterio,
                success: function (data) {
                    $("#listaEncontrada").show();
                    $("#listaEncontrada").html(data);
                },
                error: function () {
                    // alert('it broke');
                },
                complete: function () {
                    // alert('it completed');
                }
            });
        }
    });

    //Buscador IAE

    $('#botonBuscarIAE').click(function() {
        if($("#buscarIAE").valid()){
            $("#buscarIAE").submit();
        }
    });

    $("form[name='buscarIAE']").validate({
        rules: {
            'buscarIAE[tipoBusqueda]': "required",
            'buscarIAE[criterio]': "required"
        },
        messages: {
            'buscarIAE[tipoBusqueda]': "Seleccione el tipo de búsqueda",
            'buscarIAE[criterio]': "Introduzca el criterio para buscar el IAE"
        },
        submitHandler: function(form) {

            var tipoBusqueda  = $('select[name="buscarIAE[tipoBusqueda]"]').children("option:selected").val();
            var criterio  = $('#criterioIAE').val();

            console.log(tipoBusqueda + " " + criterio);

            $.ajax({
                type: "GET",
                url: '/gestores/buscaIAE/' + tipoBusqueda + '/' + criterio,
                success: function (data) {
                    $("#listaEncontradaIAE").show();
                    $("#listaEncontradaIAE").html(data);
                },
                error: function () {
                    // alert('it broke');
                },
                complete: function () {
                    // alert('it completed');
                }
            });
        }
    });
});