<?php

namespace App\Entity;

use App\Repository\ResetPasswordRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordRequestTrait;

/**
 * @ORM\Entity(repositoryClass=ResetPasswordRequestRepository::class)
 */
class ResetPasswordRequest implements ResetPasswordRequestInterface
{
    use ResetPasswordRequestTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Gestores::class)
     */
    private $userGestor;

    /**
     * @ORM\ManyToOne(targetEntity=Trabajador::class)
     */
    private $userTrabajador;

    public function __construct(object $user, \DateTimeInterface $expiresAt, string $selector, string $hashedToken)
    {
        if($user instanceof Empresa){
            $this->user = $user;
        }else if($user instanceof Gestores){
            $this->userGestor = $user;
        }else{
            $this->userTrabajador = $user;
        }

        $this->initialize($expiresAt, $selector, $hashedToken);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): object
    {
        if(!is_null($this->userTrabajador)){
            return $this->userTrabajador;
        }else if(!is_null($this->userGestor)){
            return $this->userGestor;
        }else{
            return $this->user;
        }
    }
}
