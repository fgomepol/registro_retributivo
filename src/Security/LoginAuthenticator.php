<?php

namespace App\Security;

use App\Entity\Gestores;
use App\Entity\Empresa;
use App\Entity\Trabajador;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {        
        $credentials = [
            'cif' => $request->request->get('cif'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['cif']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(Gestores::class)->loadUserByUsername($credentials['cif']);

        if (!$user || !password_verify($credentials['password'], $user->getPassword())) {
            // fail authentication with a custom error
            //throw new CustomUserMessageAuthenticationException('No existe ningun gestor con los datos facilitados.');

            $user = $this->entityManager->getRepository(Empresa::class)->loadUserByUsername($credentials['cif']);

            if (!$user || !password_verify($credentials['password'], $user->getPassword())) {
                // fail authentication with a custom error
                $user = $this->entityManager->getRepository(Trabajador::class)->loadUserByUsername($credentials['cif']);

                if (!$user || !password_verify($credentials['password'], $user->getPassword())) {
                    // fail authentication with a custom error
                    throw new CustomUserMessageAuthenticationException('No existe ningun usuario con los datos facilitados.');
                }else if (!$user->getActivo()) {
                    // fail authentication user inactive
                    throw new CustomUserMessageAuthenticationException('El trabajador tiene el acceso bloqueado.');
                }
            }else if (!$user->getActivo()) {
                // fail authentication user inactive
                throw new CustomUserMessageAuthenticationException('La empresa tiene el acceso bloqueado.');
            }
        }else if (!$user->getActivo()) {
            // fail authentication user inactive
            throw new CustomUserMessageAuthenticationException('El gestor tiene el acceso bloqueado.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        if(in_array('ROLE_TRABAJADOR', $token->getUser()->getRoles())){
            return new RedirectResponse($this->urlGenerator->generate('ya_logado_trabajador'));
        }else if(in_array('ROLE_GESTOR', $token->getUser()->getRoles())){
            return new RedirectResponse($this->urlGenerator->generate('ya_logado'));
        }else if(in_array('ROLE_EMPRESA', $token->getUser()->getRoles())){
            return new RedirectResponse($this->urlGenerator->generate('ya_logado_empresa'));
        }        
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }
}
