$(document).ready(function() {
    $("#compraProductos").addClass("active");
    $("#compraLicencias").addClass("menu-is-opening menu-open");
    $("#menuCompras").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

$("form[name='correo_soporte']").validate({
    rules: {
        'correo_soporte[titulo]': "required",
        'correo_soporte[cuerpo]': "required",
        'correo_soporte[gestorSeleccionado]': "required"
    },
    messages: {
        'correo_soporte[titulo]': "Debe indicar el asunto de la incidencia",
        'correo_soporte[cuerpo]': "Debe rellenar el cuerpo del mensaje",
        'correo_soporte[gestorSeleccionado]': "Debe seleccionar el destinatario"
    },
    submitHandler: function(form) {
        form.submit();
    }
});