<?php

namespace App\Utils;

use App\Entity\Compras;
use App\Entity\Contacto;
use App\Entity\DatosFacturacion;
use App\Entity\DetalleCompras;
use App\Entity\Empresa;
use App\Entity\FacturaDetalle;
use App\Entity\Facturas;
use App\Entity\Gestores;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Email;

class Utils
{

/**
     * Generates a strong password of N length containing at least one lower case letter,
     * one uppercase letter, one digit, and one special character.
     * The remaining characters in the password are chosen at random from those four sets.
     * The available characters in each set are user friendly - there are no ambiguous characters such as i, l, 1, o, 0, etc.
     *
     * Note: the $add_dashes option will increase the length of the password by floor(sqrt(N)) characters.
     *
     * @return string random strong password
     */

    public function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }

        $password = str_shuffle($password);

        if (!$add_dashes) {
            return $password;
        }

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function sendPassEmail(MailerInterface $mailer, string $nombre, string $emailAddress, string $password)
    {
        $sendemail = (new TemplatedEmail())
            ->from('noreply@informeretributivo.com')
            ->to(new Address($emailAddress))
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Bienvenido a informeretributivo.com')            
            // path of the Twig template to render
            ->htmlTemplate('email/signup.html.twig')

             // pass variables (name => value) to the template
            ->context([
                'nombre'    => $nombre,
                'password'  => $password
            ]);

        try {
            $mailer->send($sendemail);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendPassTrabajadorEmail(MailerInterface $mailer, string $razonSocial, string $nombre, string $apellidos, string $emailAddress, string $password)
    {
        $sendemail = (new TemplatedEmail())
            ->from('noreply@informeretributivo.com')
            ->to(new Address($emailAddress))
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Claves de acceso al Registro Horario de la empresa '.$razonSocial)            
            // path of the Twig template to render
            ->htmlTemplate('email/signupTrabajador.html.twig')

             // pass variables (name => value) to the template
            ->context([
                'razonSocial'    => $razonSocial,
                'nombre'    => $nombre,
                'apellidos'    => $apellidos,
                'password'  => $password
            ]);

        try {
            $mailer->send($sendemail);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendGestoresEmail(MailerInterface $mailer, Gestores $gestor)
    {
        $sendemail = (new TemplatedEmail())
            ->from('info@informeretributivo.com')
            ->to('info@informeretributivo.com')
            ->addCc('f.gomez@informeretributivo.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Alta nuevo gestor en informeretributivo.com')            
            // path of the Twig template to render
            ->htmlTemplate('email/altaGestor.html.twig')

             // pass variables (name => value) to the template
            ->context([
                'razonSocial'    => $gestor->getNombre(),
                'cif'  => $gestor->getCif(),
                'correo'  => $gestor->getEmail(),
                'telefono'  => $gestor->getTelefono(),
                'localidad'  => $gestor->getLocalidad()
            ]);

        try {
            $mailer->send($sendemail);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendEmpresaAsistidaEmail(MailerInterface $mailer, Empresa $empresa)
    {
        $sendemail = (new TemplatedEmail())
            ->from('info@informeretributivo.com')
            ->to('registrodatos@informeretributivo.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Alta nueva empresa asistida '.$empresa->getNombre())
            // path of the Twig template to render
            ->htmlTemplate('email/altaEmpresaAsistida.html.twig')

             // pass variables (name => value) to the template
            ->context([
                'razonSocial'    => $empresa->getNombre(),
                'cif'  => $empresa->getCif(),
                'correo'  => $empresa->getEmail(),
                'telefono'  => $empresa->getTelefono()
            ]);

        try {
            $mailer->send($sendemail);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendContactEmail(MailerInterface $mailer, $formData)
    {
        $sendemail = (new TemplatedEmail())
            ->from('info@informeretributivo.com')
            ->to('info@informeretributivo.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Contacto desde la web informeretributivo.com')
            ->htmlTemplate('email/contacto.html.twig')
            ->context([
                'nombre'   => $formData['nombre'],
                'direccionEmail'    => $formData['email'],
                'mensaje'  => $formData['mensaje']
            ]);

        try {
            $mailer->send($sendemail);
            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }

    public function sendInvoice(MailerInterface $mailer, int $factura, string $emailAddress)
    {
        $sendemail = (new TemplatedEmail())
            ->from('noreply@informeretributivo.com')
            ->to(new Address($emailAddress))
            ->priority(Email::PRIORITY_HIGH)
            ->subject('Factura de compra informeretributivo.com')
            ->htmlTemplate('email/factura.html.twig')
            ->context([
                'idFactura'   => $factura
            ]);

        try {
            $mailer->send($sendemail);
            //unlink($factura_path);
            return true;
        } catch (TransportExceptionInterface $e) {
            //unlink($factura_path);
            return false;
        }
    }

    public function calcula_mediana($array) {
        
        sort($array);
        
        $count = count($array);
        $middleval = floor(($count-1)/2);
        
        if($count % 2) {
            $median = $array[$middleval];
        } else {
            $low = $array[$middleval];
            $high = $array[$middleval+1];
            $median = (($low+$high)/2);
        }
        
        return $median;
    }

    public function esBisiesto($anio) {
        return !($anio % 4) && ($anio % 100 || !($anio % 400));
    }

    function redondeado ($numero, $decimales) {
        $factor = pow(10, $decimales);
        return (round($numero*$factor)/$factor); 
    }

    function eliminar_tildes($cadena){

        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        //$cadena = utf8_encode($cadena);
    
        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );
    
        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );
    
        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );
    
        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );
    
        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );
    
        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );
    
        return $cadena;
    }

    function load_data() {
        $xmlDoc = new \DOMDocument();
        $xmlDoc->load('../public/uploads/data.xml');
        $data = array();
        foreach ($xmlDoc->documentElement->childNodes AS $item)
        {
          if ( $item->nodeType == XML_ELEMENT_NODE ) {
          $data []= array( "Account" => $item->getAttribute("account"),
            "Genre" => $item->getAttribute("genre"),
            "Images" => $item->getAttribute("images"),
            "Average Ranking" => $item->getAttribute("avgrank"),
            "Total Size" => $item->getAttribute("size") );
          }
        }
        return $data;
    }

    public function generaFacturaPDF(Facturas $datos){

        $pdf = new \FPDF();

        define('EURO',chr(128));
        define('IVA',0.21);
        define('IRPF',0.15);
        define('DIFF',1.21);

        $modoPago = $datos->getCodCompra()->getModoPago();

        $num_factura = "R-".str_pad($datos->getCodFactura(), 5, '00000', STR_PAD_LEFT);
        
        $fechaFactura = new \DateTime('@' . $datos->getFechaFactura());

        $dia  = $fechaFactura->format('d');
        $mes  = self::calculaMes($fechaFactura->format('m'));
        $anio = $fechaFactura->format('Y');

        $fechaFactura =$dia." de ".$mes." de ".$anio;
        
        $pdf->AddPage();
        $pdf->Image('../public/uploads/factura_2.png', 0, 0, $pdf->GetPageWidth(), $pdf->GetPageHeight());
        $pdf->SetFont('Times','',10);
        
        $pdf->SetXY(132, 83.5);
        $pdf->MultiCell(90,5,$fechaFactura, 0);

        $pdf->SetXY(137, 88);
        $pdf->MultiCell(90,5,$num_factura, 0);

        $pdf->SetFont('Times','',12);
        $pdf->SetXY(27, 105);
        $pdf->MultiCell(150,5,$datos->getNombre() ." - ". $datos->getCif(), 0);

        $pdf->SetXY(27, 110);
        $pdf->MultiCell(150,5,utf8_decode($datos->getDireccion() .", ". $datos->getCp() .", ". $datos->getCodProvincia()->getNombre() ." - ".$datos->getLocalidad()), 0);

        $pdf->SetFont('Times','',10);

        $x = 30;
        $y = 147;
        
        $total = 0;
        
        foreach($datos->getFacturaDetalles() as $concepto){
            $pdf->SetXY($x, $y);
            $pdf->MultiCell(90,5,$concepto->getCantidad()."x ".$concepto->getConcepto(), 0);
            
            $pdf->SetXY($x + 127, $y);
            $pdf->MultiCell(20,5,number_format($concepto->getImporte(),2,'.',',')." ".EURO, 0, 'R');

            $y+=5;
            $total+=$concepto->getImporte();
        }

        $pdf->SetXY(145, 178);
        $pdf->MultiCell(20,5,number_format($total,2,'.',','), 0, 'R');

        $baseImponible = $total/DIFF;
        $IVA = ($total/DIFF) * IVA;
        $IRPF = ($total/DIFF) * IRPF;
        $total_factura = $total - (($total/DIFF)*IRPF);

        $pdf->SetXY(82.5, 205);
        $pdf->MultiCell(20,5,number_format($baseImponible,2,'.',','), 0, 'R');

        $pdf->SetXY(80, 209.5);
        $pdf->MultiCell(20,5,number_format($IVA,2,'.',','), 0, 'R');

        $pdf->SetXY(89, 218);
        $pdf->MultiCell(20,5,number_format($IRPF,2,'.',','), 0, 'R');

        $pdf->SetFont('Times','B',10);
        $pdf->SetXY(82.5, 213.5);
        $pdf->MultiCell(20,5,number_format($total,2,'.',','), 0, 'R');
        
        $pdf->SetXY(84, 222.5);
        $pdf->MultiCell(20,5,number_format($total_factura,2,'.',','), 0, 'R');

        
        $pdf->SetFont('Times','',10);
        $pdf->SetXY(43, 234.5);
        $pdf->MultiCell(25,5,$modoPago, 0, 'R');
        
        return new Response($pdf->Output($datos->getNombreFichero(),'I'), 200, array(
                'Content-Type' => 'application/pdf'));
        
    }

    public function generaFacturaPaypal(Gestores $loggedUser, $cart, $entityManager, $utils, $datosCompra){

        $nombreFichero = 'factura_'.time().'_'.$loggedUser->getId().'.pdf';
        $anio = date('Y');
        $codFactura = $entityManager->getRepository(Facturas::class)->findLastFactura($anio);
        
        $datosFacturacion = $entityManager->getRepository(DatosFacturacion::class)->findOneBy([
            'codGestor' => $loggedUser
        ]);
        
        if(empty($codFactura[0]['codFactura'])){
            $codFactura = 1;
        }else{
            $codFactura = $codFactura[0]['codFactura'] + 1;
        }
        
        $factura = new Facturas();

        $factura->setCodFactura($codFactura);

        //Datos del Cliente
        $factura->setNombre($datosFacturacion->getNombre());
        $factura->setCif($datosFacturacion->getCif());
        $factura->setDireccion($datosFacturacion->getDireccion());
        $factura->setCodComunidad($datosFacturacion->getCodComunidad());
        $factura->setCodProvincia($datosFacturacion->getCodProvincia());
        $factura->setLocalidad($datosFacturacion->getLocalidad());
        $factura->setCp($datosFacturacion->getCp());
        $factura->setFechaFactura(time());
        $factura->setAnio($anio);
        $factura->setNombreFichero($nombreFichero);
        $factura->setCodGestor($loggedUser);
        $factura->setCodCompra($datosCompra);

        $entityManager->persist($factura);
        $entityManager->flush();

        $facturaDB = $entityManager->getRepository(Facturas::class)->findOneBy([
            'codGestor'  => $loggedUser,
            'codFactura'    => $codFactura
        ]);

        //Conceptos
        foreach($cart->getItems() as $item){
            $facturaDetalle = new FacturaDetalle();

            $facturaDetalle->setCantidad($item->getCantidad());
            $facturaDetalle->setImporte($item->getProducto()->getPVD() * $item->getCantidad());
            $facturaDetalle->setConcepto($item->getProducto()->getNombre());
            $facturaDetalle->setCodFactura($facturaDB);
            
            $entityManager->persist($facturaDetalle);
            $entityManager->flush();
        }

        return $facturaDB->getId();
    }

    public function generaFacturaTransferencia(Gestores $gestor, $entityManager, Compras $datosCompra){

        $nombreFichero = 'factura_'.time().'_'.$gestor->getId().'.pdf';
        $anio = date('Y');
        $codFactura = $entityManager->getRepository(Facturas::class)->findLastFactura($anio);
        
        $datosFacturacion = $entityManager->getRepository(DatosFacturacion::class)->findOneBy([
            'codGestor' => $gestor
        ]);
        
        if(empty($codFactura[0]['codFactura'])){
            $codFactura = 1;
        }else{
            $codFactura = $codFactura[0]['codFactura'] + 1;
        }
        
        $factura = new Facturas();

        $factura->setCodFactura($codFactura);

        //Datos del Cliente
        $factura->setNombre($datosFacturacion->getNombre());
        $factura->setCif($datosFacturacion->getCif());
        $factura->setDireccion($datosFacturacion->getDireccion());
        $factura->setCodComunidad($datosFacturacion->getCodComunidad());
        $factura->setCodProvincia($datosFacturacion->getCodProvincia());
        $factura->setLocalidad($datosFacturacion->getLocalidad());
        $factura->setCp($datosFacturacion->getCp());
        $factura->setFechaFactura(time());
        $factura->setAnio($anio);
        $factura->setNombreFichero($nombreFichero);
        $factura->setCodGestor($gestor);
        $factura->setCodCompra($datosCompra);

        $entityManager->persist($factura);
        $entityManager->flush();

        $facturaDB = $entityManager->getRepository(Facturas::class)->findOneBy([
            'codGestor'  => $gestor,
            'codFactura'    => $codFactura
        ]);

        $detalleCompras = $entityManager->getRepository(DetalleCompras::class)->findBy([
            'codCompra' => $datosCompra
        ]);
        
        $totalLicencias = 0;
        $totalLicenciasAsistidas = 0;
        //Conceptos
        
        foreach($detalleCompras as $item){

            $facturaDetalle = new FacturaDetalle();

            $facturaDetalle->setCantidad($item->getCantidad());
            $facturaDetalle->setImporte($item->getImporte());
            $facturaDetalle->setConcepto($item->getIdProducto()->getNombre());
            $facturaDetalle->setCodFactura($facturaDB);

            if($item->getIdProducto()->getTipoLicencia() == "Experta"){
                $totalLicencias += $item->getIdProducto()->getNumLicencias();
            }else if ($item->getIdProducto()->getTipoLicencia() == "Asistida"){
                $totalLicenciasAsistidas += $item->getIdProducto()->getNumLicencias();
            }
            
            $entityManager->persist($facturaDetalle);
            $entityManager->flush();
        }

        $totalLicencias = $totalLicencias +  $gestor->getLicenciasDisponibles();
        $gestor->setLicenciasDisponibles($totalLicencias);

        $totalLicenciasAsistidas = $totalLicenciasAsistidas +  $gestor->getLicenciasDisponiblesAsistida();
        $gestor->setLicenciasDisponiblesAsistida($totalLicenciasAsistidas);

        $entityManager->persist($gestor);
        $entityManager->flush();

        return $facturaDB->getId();
    }

    public function generaFacturaPersonalizada(DatosFacturacion $datosFacturacion, $entityManager, Compras $datosCompra){

        $nombreFichero = 'factura_'.time().'_'.$datosFacturacion->getCif().'.pdf';
        $anio = date('Y');
        $codFactura = $entityManager->getRepository(Facturas::class)->findLastFactura($anio);
                
        if(empty($codFactura[0]['codFactura'])){
            $codFactura = 1;
        }else{
            $codFactura = $codFactura[0]['codFactura'] + 1;
        }
        
        $factura = new Facturas();

        $factura->setCodFactura($codFactura);

        //Datos del Cliente
        $factura->setNombre($datosFacturacion->getNombre());
        $factura->setCif($datosFacturacion->getCif());
        $factura->setDireccion($datosFacturacion->getDireccion());
        $factura->setCodComunidad($datosFacturacion->getCodComunidad());
        $factura->setCodProvincia($datosFacturacion->getCodProvincia());
        $factura->setLocalidad($datosFacturacion->getLocalidad());
        $factura->setCp($datosFacturacion->getCp());
        $factura->setFechaFactura(time());
        $factura->setAnio($anio);
        $factura->setNombreFichero($nombreFichero);
        $factura->setCodDatosFacturacion($datosFacturacion);
        $factura->setCodCompra($datosCompra);

        $entityManager->persist($factura);
        $entityManager->flush();

        $facturaDB = $entityManager->getRepository(Facturas::class)->findFacturaByCodDatosFacturacion($datosFacturacion->getId());

        $detalleCompras = $entityManager->getRepository(DetalleCompras::class)->findBy([
            'codCompra' => $datosCompra
        ]);
        //Conceptos
        foreach($detalleCompras as $item){

            $facturaDetalle = new FacturaDetalle();

            $facturaDetalle->setCantidad($item->getCantidad());
            $facturaDetalle->setImporte($item->getImporte());
            $facturaDetalle->setConcepto($item->getIdProducto()->getNombre());
            $facturaDetalle->setCodFactura($facturaDB);
            
            $entityManager->persist($facturaDetalle);
            $entityManager->flush();
        }

        return $facturaDB->getId();
    }

    public static function calculaMes($mes){
        
        switch($mes){
            case 1: 
                $valorMes = "Enero";
                break;
            case 2: 
                $valorMes = "Febrero";
                break;
            case 3: 
                $valorMes = "Marzo";
                break;
            case 4: 
                $valorMes = "Abril";
                break;
            case 5: 
                $valorMes = "Mayo";
                break;
            case 6: 
                $valorMes = "Junio";
                break;
            case 7: 
                $valorMes = "Julio";
                break;
            case 8: 
                $valorMes = "Agosto";
                break;
            case 9: 
                $valorMes = "Septiembre";
                break;
            case 10: 
                $valorMes = "Octubre";
                break;
            case 11: 
                $valorMes = "Noviembre";
                break;
            default: 
                $valorMes = "Diciembre";
                break;
        }

        return $valorMes;
    }

    public static function datosFacturacionMapper($data, $datosFacturacion){

        $datosFacturacion->setNombre($data->getNombre());
        $datosFacturacion->setCif($data->getCif());
        $datosFacturacion->setDireccion($data->getDireccion());
        $datosFacturacion->setCp($data->getCp());
        $datosFacturacion->setLocalidad($data->getLocalidad());
        $datosFacturacion->setCodProvincia($data->getCodProvincia());
        $datosFacturacion->setCodComunidad($data->getCodComunidad());
        
        return $datosFacturacion;
    }

    public static function datosFacturacionPersonalizadaMapper($data, $datosFacturacion){

        $datosFacturacion->setNombre($data['nombre']);
        $datosFacturacion->setCif($data['cif']);
        $datosFacturacion->setDireccion($data['direccion']);
        $datosFacturacion->setCp($data['cp']);
        $datosFacturacion->setLocalidad($data['localidad']);
        $datosFacturacion->setCodProvincia($data['codProvincia']);
        $datosFacturacion->setCodComunidad($data['codComunidad']);
        
        return $datosFacturacion;
    }
}