<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210427104004 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE motivo_reduccion (id INT AUTO_INCREMENT NOT NULL, valor VARCHAR(255) NOT NULL, descripcion VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador ADD cod_motivo_reduccion_id INT DEFAULT NULL, ADD porcentaje_reducida DOUBLE PRECISION DEFAULT NULL, ADD porcentaje_jornada DOUBLE PRECISION DEFAULT NULL, ADD nivel_retributivo SMALLINT DEFAULT NULL, CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador ADD CONSTRAINT FK_42157CDF3240B9B3 FOREIGN KEY (cod_motivo_reduccion_id) REFERENCES motivo_reduccion (id)');
        $this->addSql('CREATE INDEX IDX_42157CDF3240B9B3 ON trabajador (cod_motivo_reduccion_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trabajador DROP FOREIGN KEY FK_42157CDF3240B9B3');
        $this->addSql('DROP TABLE motivo_reduccion');
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_42157CDF3240B9B3 ON trabajador');
        $this->addSql('ALTER TABLE trabajador DROP cod_motivo_reduccion_id, DROP porcentaje_reducida, DROP porcentaje_jornada, DROP nivel_retributivo, CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
