<?php

namespace App\Entity;

use App\Repository\TrabajadorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as AcmeAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TrabajadorRepository::class)
 */
class Trabajador implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=350)
     */
    private $apellidos;

    /**
     * @ORM\Column(type="string", length=9)
     * @AcmeAssert\ValidateIdentityNumber
     */
    private $dni;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaInicioEmpresa;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $sexo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $puesto;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numPagas = 12;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaAlta;

    /**
     * @ORM\OneToMany(targetEntity=DetalleRegistroSalarial::class, mappedBy="codTrabajador", orphanRemoval=true)
     */
    private $detallesRegistroSalarial;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class, inversedBy="trabajadores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codEmpresa;

    /**
     * @ORM\ManyToOne(targetEntity=ConvenioColectivo::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $codConvenio;

    /**
     * @ORM\ManyToOne(targetEntity=GrupoProfesional::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $grupo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $salarioAnual;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fechaFinEmpresa;

    /**
     * @ORM\ManyToOne(targetEntity=MotivoReduccion::class)
     */
    private $codMotivoReduccion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $porcentajeReducida;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $porcentajeJornada;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nivelRetributivo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $jornadaReducida;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $primeraClave = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $activo = true;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codigoContrato; 

    public function __construct()
    {
        $this->detallesRegistroSalarial = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getFechaInicioEmpresa(): ?int
    {
        return $this->fechaInicioEmpresa;
    }

    public function setFechaInicioEmpresa(?int $fechaInicioEmpresa): self
    {
        $this->fechaInicioEmpresa = $fechaInicioEmpresa;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    public function setPuesto(string $puesto): self
    {
        $this->puesto = $puesto;

        return $this;
    }

    public function getNumPagas(): ?int
    {
        return $this->numPagas;
    }

    public function setNumPagas(int $numPagas): self
    {
        $this->numPagas = $numPagas;

        return $this;
    }

    public function getFechaAlta(): ?int
    {
        return $this->fechaAlta;
    }

    public function setFechaAlta(int $fechaAlta): self
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * @return Collection|DetalleRegistroSalarial[]
     */
    public function getDetallesRegistroSalarial(): Collection
    {
        return $this->detallesRegistroSalarial;
    }

    public function addDetallesRegistroSalarial(DetalleRegistroSalarial $detallesRegistroSalarial): self
    {
        if (!$this->detallesRegistroSalarial->contains($detallesRegistroSalarial)) {
            $this->detallesRegistroSalarial[] = $detallesRegistroSalarial;
            $detallesRegistroSalarial->setCodTrabajador($this);
        }

        return $this;
    }

    public function removeDetallesRegistroSalarial(DetalleRegistroSalarial $detallesRegistroSalarial): self
    {
        if ($this->detallesRegistroSalarial->removeElement($detallesRegistroSalarial)) {
            // set the owning side to null (unless already changed)
            if ($detallesRegistroSalarial->getCodTrabajador() === $this) {
                $detallesRegistroSalarial->setCodTrabajador(null);
            }
        }

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    public function getCodConvenio(): ?ConvenioColectivo
    {
        return $this->codConvenio;
    }

    public function setCodConvenio(?ConvenioColectivo $codConvenio): self
    {
        $this->codConvenio = $codConvenio;

        return $this;
    }

    public function getGrupo(): ?GrupoProfesional
    {
        return $this->grupo;
    }

    public function setGrupo(?GrupoProfesional $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getSalarioAnual(): ?float
    {
        return $this->salarioAnual;
    }

    public function setSalarioAnual(float $salarioAnual): self
    {
        $this->salarioAnual = $salarioAnual;

        return $this;
    }

    public function getFechaFinEmpresa(): ?int
    {
        return $this->fechaFinEmpresa;
    }

    public function setFechaFinEmpresa(?int $fechaFinEmpresa): self
    {
        $this->fechaFinEmpresa = $fechaFinEmpresa;

        return $this;
    }

    public function getCodMotivoReduccion(): ?MotivoReduccion
    {
        return $this->codMotivoReduccion;
    }

    public function setCodMotivoReduccion(?MotivoReduccion $codMotivoReduccion): self
    {
        $this->codMotivoReduccion = $codMotivoReduccion;

        return $this;
    }

    public function getPorcentajeReducida(): ?float
    {
        return $this->porcentajeReducida;
    }

    public function setPorcentajeReducida(?float $porcentajeReducida): self
    {
        $this->porcentajeReducida = $porcentajeReducida;

        return $this;
    }

    public function getPorcentajeJornada(): ?float
    {
        return $this->porcentajeJornada;
    }

    public function setPorcentajeJornada(?float $porcentajeJornada): self
    {
        $this->porcentajeJornada = $porcentajeJornada;

        return $this;
    }

    public function getNivelRetributivo(): ?int
    {
        return $this->nivelRetributivo;
    }

    public function setNivelRetributivo(?int $nivelRetributivo): self
    {
        $this->nivelRetributivo = $nivelRetributivo;

        return $this;
    }

    public function getJornadaReducida(): ?bool
    {
        return $this->jornadaReducida;
    }

    public function setJornadaReducida(?bool $jornadaReducida): self
    {
        $this->jornadaReducida = $jornadaReducida;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPrimeraClave(): ?bool
    {
        return $this->primeraClave;
    }

    public function setPrimeraClave(bool $primeraClave): self
    {
        $this->primeraClave = $primeraClave;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): self
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_TRABAJADOR';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->dni;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getCodigoContrato(): ?int
    {
        return $this->codigoContrato;
    }

    public function setCodigoContrato(?int $codigoContrato): self
    {
        $this->codigoContrato = $codigoContrato;

        return $this;
    }
}
