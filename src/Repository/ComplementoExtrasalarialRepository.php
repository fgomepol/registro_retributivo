<?php

namespace App\Repository;

use App\Entity\ComplementoExtrasalarial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ComplementoExtrasalarial|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComplementoExtrasalarial|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComplementoExtrasalarial[]    findAll()
 * @method ComplementoExtrasalarial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplementoExtrasalarialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComplementoExtrasalarial::class);
    }

    // /**
    //  * @return ComplementoExtrasalarial[] Returns an array of ComplementoExtrasalarial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ComplementoExtrasalarial
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
