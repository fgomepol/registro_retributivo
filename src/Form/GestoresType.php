<?php

namespace App\Form;

use App\Entity\Gestores;
use App\Entity\CCAA;
use App\Entity\Provincias;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GestoresType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre']]) 
            ->add('cif', TextType::class, ['attr' => ['placeholder' => 'CIF/NIF/NIE']])
            ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección']])
            ->add('telefono', TextType::class, ['attr' => ['placeholder' => 'Teléfono']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Correo electrónico']])
            ->add('ccaa', EntityType::class, [
                'class' => CCAA::class,
                'choice_label' => 'nombre',
                'placeholder'   => '-- Elija la Comunidad Autónoma --',
            ])
            ->add('provincia', EntityType::class, [
                'class' => Provincias::class,
                'choice_label' => 'nombre',
                'placeholder'   => '-- Elija la Provincia --',
            ])
            ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal']])
            ->add('save', SubmitType::class, ['label' => 'Guardar Asesoría'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gestores::class,
        ]);
    }
}