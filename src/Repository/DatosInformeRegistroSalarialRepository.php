<?php

namespace App\Repository;

use App\Entity\DatosInformeRegistroSalarial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DatosInformeRegistroSalarial|null find($id, $lockMode = null, $lockVersion = null)
 * @method DatosInformeRegistroSalarial|null findOneBy(array $criteria, array $orderBy = null)
 * @method DatosInformeRegistroSalarial[]    findAll()
 * @method DatosInformeRegistroSalarial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DatosInformeRegistroSalarialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DatosInformeRegistroSalarial::class);
    }


    function findByEmpresaAndTipoCalculoYSexoTotales(int $codEmpresa, string $tipoCalculo, string $sexo, int $anio){
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
            ->select('d.sexo', 'd.concepto', 'd.descripcion', 'SUM(d.importe) AS total, case when c.nombre IS NOT NULL then c.nombre when ce.nombre IS NOT NULL then ce.nombre else d.descripcion end as nombre')
            ->from('App\Entity\DatosInformeRegistroSalarial', 'd')
            ->innerJoin('d.idInforme','i')
            ->leftJoin('d.codComplemento','c')
            ->leftJoin('d.codComplementoExtrasalarial','ce')
            ->where('i.codEmpresa = :cod')
            ->andWhere('d.tipoCalculo = :calculo')
            ->andWhere('d.sexo = :sexo')
            ->andWhere('i.anio = :anio')
            ->groupBy('d.sexo, d.concepto, d.descripcion, d.codComplemento, d.codComplementoExtrasalarial')
            ->setParameter('cod', $codEmpresa)
            ->setParameter('calculo', $tipoCalculo)
            ->setParameter('sexo', $sexo)
            ->setParameter('anio', $anio)
            ->orderBy('d.concepto', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    
    function findByEmpresaAndTipoCalculoYSexo(int $codEmpresa, string $tipoCalculo, string $sexo, int $anio){
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
            ->select('d.grupo', 'd.sexo', 'd.concepto','case when c.nombre IS NOT NULL then case when c.nombre = \'Otro\' then d.descripcion else c.nombre end when ce.nombre IS NOT NULL then case when ce.nombre = \'Otro\' then d.descripcion else ce.nombre end else d.descripcion end as descr', 'SUM(d.importe) AS total')
            ->from('App\Entity\DatosInformeRegistroSalarial', 'd')
            ->innerJoin('d.idInforme','i')
            ->leftJoin('d.codComplemento','c')
            ->leftJoin('d.codComplementoExtrasalarial','ce')
            ->where('i.codEmpresa = :cod')
            ->andWhere('d.tipoCalculo = :calculo')
            ->andWhere('d.sexo = :sexo')
            ->andWhere('i.anio = :anio')
            ->groupBy('d.grupo, d.sexo, d.concepto, d.descripcion, d.codComplemento')
            ->setParameter('cod', $codEmpresa)
            ->setParameter('calculo', $tipoCalculo)
            ->setParameter('sexo', $sexo)
            ->setParameter('anio', $anio)
            ->orderBy('d.grupo, d.concepto', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    function findDistintosConceptos(int $codEmpresa, int $anio){
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
            ->select('d.concepto', 'case when c.nombre IS NOT NULL then case when c.nombre = \'Otro\' then d.descripcion else c.nombre end when ce.nombre IS NOT NULL then case when ce.nombre = \'Otro\' then d.descripcion else ce.nombre end else d.descripcion end as descr','c.tipoRetribucion','c.normalizable','c.anualizable')
            ->from('App\Entity\DatosInformeRegistroSalarial', 'd')
            ->innerJoin('d.idInforme','i')
            ->leftJoin('d.codComplemento','c')
            ->leftJoin('d.codComplementoExtrasalarial','ce')
            ->where('i.codEmpresa = :cod')
            ->andWhere('i.anio = :anio')
            ->groupBy('d.concepto, d.descripcion, d.codComplemento, d.codComplementoExtrasalarial')
            ->setParameter('cod', $codEmpresa)
            ->setParameter('anio', $anio)
            ->orderBy('d.concepto, descr', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return DatosInformeRegistroSalarial[] Returns an array of DatosInformeRegistroSalarial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DatosInformeRegistroSalarial
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
