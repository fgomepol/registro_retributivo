<?php

namespace App\Entity;

use App\Repository\CentroTrabajoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CentroTrabajoRepository::class)
 */
class CentroTrabajo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="Empresa", mappedBy="CentroTrabajo", cascade={"ALL"}, indexBy="id")
     */
    private $codEmpresa;

    /**
     * @ORM\Column(type="smallint")
     * @ORM\OneToMany(targetEntity="CCAA", mappedBy="CentroTrabajo", cascade={"ALL"}, indexBy="id")
     */
    private $ccaa;

    /**
     * @ORM\Column(type="smallint")
     * @ORM\OneToMany(targetEntity="Provincias", mappedBy="CentroTrabajo", cascade={"ALL"}, indexBy="id")
     */
    private $provincia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(columnDefinition="SMALLINT UNSIGNED")
     */
    private $cp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodEmpresa(): ?int
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(int $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    public function getCcaa(): ?int
    {
        return $this->ccaa;
    }

    public function setCcaa(int $ccaa): self
    {
        $this->ccaa = $ccaa;

        return $this;
    }

    public function getProvincia(): ?int
    {
        return $this->provincia;
    }

    public function setProvincia(int $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }
}
