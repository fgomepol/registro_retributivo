<?php

namespace App\Repository;

use App\Entity\MotivoReduccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MotivoReduccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method MotivoReduccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method MotivoReduccion[]    findAll()
 * @method MotivoReduccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotivoReduccionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MotivoReduccion::class);
    }

    // /**
    //  * @return MotivoReduccion[] Returns an array of MotivoReduccion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MotivoReduccion
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
