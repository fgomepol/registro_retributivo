$(document).ready(function() {
    $("#listaEmpresas").addClass("active");
    $("#registroMenu").addClass("menu-is-opening menu-open");
    $("#registroRetributivo").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });

    if($('select[name="detalle_registro_salarial[concepto]"]').val() == "complementoExtrasalarial"){
        activaExtrasalarial();
    }else if($('select[name="detalle_registro_salarial[concepto]"]').val() == "complementoSalarial"){
        activaSalarial()
    }

    $('select[name="detalle_registro_salarial[concepto]"]').on('change', function () {

        
        var selectedCountry = $(this).children("option:selected").val();

        $("#detalle_registro_salarial_descripcion").val($(this).children("option:selected").text());
        
        if(selectedCountry == "complementoExtrasalarial"){

            activaExtrasalarial();

        }else if(selectedCountry == "complementoSalarial"){

            activaSalarial()
            
        }else{

            $('#divHoras').hide();
            $("#detalle_registro_salarial_horas").val('');
            
            $('#divConceptoExtraSalarial').hide();
            $('#divConceptoSalarial').hide();

            $('#detalle_registro_salarial_codComplemento').val("");
            $('#detalle_registro_salarial_codComplemento').rules("remove");
            $('#detalle_registro_salarial_descripcion').rules("remove");
            $('#detalle_registro_salarial_horas').rules("remove");
            $('#detalle_registro_salarial_codComplementoExtrasalarial').val("");
            $('#detalle_registro_salarial_codComplementoExtrasalarial').rules("remove");
        }
    });

    $('select[name="detalle_registro_salarial[codComplemento]"]').on('change', function () {

        var selectedCountry = $(this).children("option:selected").text();
        $("#detalle_registro_salarial_descripcion").val($('select[name="detalle_registro_salarial[concepto]"]').children("option:selected").text());

        if(selectedCountry == "Horas Extra" || selectedCountry == "Horas Complementarias"){
            $('#detalle_registro_salarial_horas').rules("add", {
                required        : true,                             
                messages        : {
                    required    : 'Por favor, introduzca el número de horas'
                }
            });

            $('#divHoras').show();

        }else if(selectedCountry == "Otro"){

            $('#divHoras').hide();
            $("#detalle_registro_salarial_horas").val('');

            $("#detalle_registro_salarial_descripcion").val('');
            $('#detalle_registro_salarial_descripcion').rules("add", {
                required        : true,                             
                messages        : {
                    required    : 'Por favor, introduzca la descripción del complemento'
                }
            });
        }else{            

            $('#divHoras').hide();
            $("#detalle_registro_salarial_horas").val('');

            $('#detalle_registro_salarial_descripcion').rules("remove");
            $('#detalle_registro_salarial_horas').rules("remove");
        }
    });

    $('select[name="detalle_registro_salarial[codComplementoExtrasalarial]"]').on('change', function () {
        var selectedCountry = $(this).children("option:selected").val();
        $("#detalle_registro_salarial_descripcion").val($('select[name="detalle_registro_salarial[concepto]"]').children("option:selected").text());

        if(selectedCountry == "7"){
            $("#detalle_registro_salarial_descripcion").val('');
            $('#detalle_registro_salarial_descripcion').rules("add", {
                required        : true,                             
                messages        : {
                    required    : 'Por favor, introduzca la descripción del complemento extra salarial'
                }
            });
        }else{            
            $('#detalle_registro_salarial_descripcion').rules("remove");
        }
    });
});

$('#divModificado').hide();

$("form[name='detalle_registro_salarial']").validate({
    rules: {
        'detalle_registro_salarial[descripcion]': "required",
        'detalle_registro_salarial[direccion]': "required"
    },
    messages: {
        'detalle_registro_salarial[descripcion]': "Por favor, introduzca la descripción",
        'detalle_registro_salarial[importe]': "Por favor, introduzca el importe"
    },
    submitHandler: function(form) {
        if(comprueba_detalle_duplicado()){
            form.submit();
        }
    }
});

function activaExtrasalarial(){
    $('#divHoras').hide();
    $("#detalle_registro_salarial_horas").val('');

    $('#divConceptoSalarial').hide();
    $('#detalle_registro_salarial_codComplemento').val("");
    $('#detalle_registro_salarial_codComplemento').rules("remove");
    $('#detalle_registro_salarial_descripcion').rules("remove");
    $('#detalle_registro_salarial_horas').rules("remove");

    $('#divConceptoExtraSalarial').show();
    $('#detalle_registro_salarial_codComplementoExtrasalarial').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, seleccione el tipo de complemento'
        }
    });
}

function activaSalarial(){
    $('#divHoras').hide();
    $("#detalle_registro_salarial_horas").val('');

    $('#divConceptoExtraSalarial').hide();
    $('#detalle_registro_salarial_codComplementoExtrasalarial').val("");
    $('#detalle_registro_salarial_codComplementoExtrasalarial').rules("remove");
    $('#detalle_registro_salarial_descripcion').rules("remove");
    $('#detalle_registro_salarial_horas').rules("remove");

    $('#divConceptoSalarial').show();
    $('#detalle_registro_salarial_codComplemento').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, seleccione el tipo de complemento'
        }
    });
}

function modificaDetalle(idDetalle, mes, anio, horas, importe, observaciones){

    if(confirm('¿Va a modificar el detalle salarial, está seguro?')){
        var mes = $('#'+mes).val();
        var anio = $('#'+anio).val();
        var horas = $('#'+horas).val();
        var importe = $('#'+importe).val();
        var observaciones = $('#'+observaciones).val();

        var data = { "idDetalle" : idDetalle , "mes" : mes , "anio" : anio , "horas" : horas , "importe" : importe , "observaciones" : observaciones};

        if(importe > 0){
            $.ajax({
                type: "GET",
                url: '/empresa/modDetalle',
                data: {json:data},
                success: function (data) {
                    $('#divModificado').show();
                },
                error: function () {
                    // alert('it broke');
                },
                complete: function () {
                    // alert('it completed');
                }
            });
        }else{
            alert('Debe introducir el importe.');
        }
    }
}

function checkActiveButton(){
    if($('input:checkbox[name=idDetalle]:checked').length > 0){
        $("button[name='deleteDetailsButton']").removeAttr("disabled").button('refresh');
    }else{
        $("button[name='deleteDetailsButton']").attr("disabled", "disabled").button('refresh');
    }
}

function deleteGroupDetails(codEmpresa, codTrabajador){
    if(confirm('¿Va a eliminar los detalles salariales seleccionados, está seguro?')){
        
        var detailsId = "";
        var numIds = 0;
        
        $('input:checkbox[name=idDetalle]:checked').each(function() 
        {   
            if(numIds == 0){
                detailsId = $(this).val();
            }else{
                detailsId = detailsId + "," + $(this).val();
            }

            numIds++;
        });

        if($('input:checkbox[name=idDetalle]:checked').length > 0){
            
            $.post('/empresa/detalleRegistroSalarial/deleteMasive', { idDetalles: detailsId }, 
                function(){
                    location.href = '/empresa/' + codEmpresa + '/' + codTrabajador + '/detalleRegistroSalarial';
            });
        }else{
            alert('Debe seleccionar algun detalle para eliminarlo.');
        }
    }
}

function comprueba_detalle_duplicado(){
    
    var importe     = $('#detalle_registro_salarial_importe').val();
    var concepto    = $('select[name="detalle_registro_salarial[concepto]"]').children("option:selected").text();

    $('#tablaDetalles tr').each(function() {
        var keval = $(this).find("#descripcion").text();
        var keva2 = $(this).find(".importe input").val();

        if(keval.toLowerCase() == concepto.toLowerCase() && keva2 == importe){

            if(!confirm('Va a introducir un detalle salarial con el mismo concepto e importe que uno ya existente, ¿desea continuar?')){
                return false;
            }
        }
    });

    return true;
}