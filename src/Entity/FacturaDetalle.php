<?php

namespace App\Entity;

use App\Repository\FacturaDetalleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturaDetalleRepository::class)
 */
class FacturaDetalle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturaDetalles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codFactura;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $concepto;

    /**
     * @ORM\Column(type="smallint")
     */
    private $cantidad;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodFactura(): ?Facturas
    {
        return $this->codFactura;
    }

    public function setCodFactura(?Facturas $codFactura): self
    {
        $this->codFactura = $codFactura;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }
}
