<?php

namespace App\Controller;

use App\Entity\ActividadesEconomicas;
use App\Entity\Empresa;
use App\Entity\Gestores;
use App\Entity\CCAA;
use App\Entity\ComplementoExtrasalarial;
use App\Entity\ComplementoSalarial;
use App\Entity\DatosInformeRegistroSalarial;
use App\Entity\DetalleRegistroSalarial;
use App\Entity\GrupoProfesional;
use App\Entity\InformeRegistroSalarial;
use App\Entity\Provincias;
use App\Entity\Trabajador;
use App\Form\EmpresaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Utils;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Regex;

class EmpresaController extends AbstractController
{
    /**
     * @Route("/empresa/nuevaEmpresa", name="crear_empresa")
     */
    public function nuevaEmpresa(Request $request, Utils $utiles, MailerInterface $mailer): Response
    {
        $this->get('session')->set('templateURL', 'empresa/alta.html.twig');
        $this->get('session')->set('page', 'altaEmpresa');

        $loggedUser = $this->getUser();

        if($loggedUser instanceof Gestores){
            $array_final = array();
            
            if($loggedUser->getLicenciasDisponibles()>0){
                $array_final["Básica"] = "Básica";
                $creada = true;
            }
            
            if($loggedUser->getLicenciasDisponiblesAsistida()>0){
                $array_final["Asistida"] = "Asistida";
                $creada = "Asistida";
            }
        }

        $empresa = new Empresa();

        $random_passwd = $utiles->generateStrongPassword();
        $empresa->setPassword($random_passwd);

        $form = $this->createForm(EmpresaType::class, $empresa)
            ->add('TipoLicencia', ChoiceType::class, array(
                'choices' => $array_final,
                'placeholder' => '-- Elija el tipo de Licencia --'
            )   
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $empresa = $form->getData(); 

            $empresa->setFechaAlta(time());
            $empresa->setFechaRenovacion(time()+(365*24*3600));
            $empresa->setRoles($empresa->getRoles()); 
            $empresa->setClaveEnviada(false);

            if($loggedUser instanceof Gestores){
                $empresa->setCodGestor($loggedUser);
            }

            if($empresa->getTipoLicencia() == "Asistida"){
                $numLicencias = $loggedUser->getLicenciasDisponiblesAsistida() - 1;
                $loggedUser->setLicenciasDisponiblesAsistida($numLicencias);

                $utiles->sendEmpresaAsistidaEmail($mailer, $empresa);
            }else{
                $numLicencias = $loggedUser->getLicenciasDisponibles() - 1;
                $loggedUser->setLicenciasDisponibles($numLicencias);
            }
                
            $empresa->setPassword(password_hash($empresa->getPassword(), PASSWORD_BCRYPT));

            $entityManager = $this->getDoctrine()->getManager();

            $empresas = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'cif' => $empresa->getCif(),
            ]);

            if(!empty($empresas)){
                return $this->render('dashboard/inicio.html.twig', array(
                    'form' => $form->createView(),
                    'creada' => false
                ));
            }
        
            $entityManager->persist($empresa);
            $entityManager->flush();

            $empresa = new Empresa();
            $form = $this->createForm(EmpresaType::class, $empresa)->add('TipoLicencia', ChoiceType::class, array(
                'choices' => $array_final,
                'placeholder' => '-- Elija el tipo de Licencia --'
            ));
            
            return $this->render('dashboard/inicio.html.twig', array(
                'form' => $form->createView(),
                'creada' => $creada
            ));
        }
        
        return $this->render('dashboard/inicio.html.twig', array(
            'form' => $form->createView(),
            'creada' => ''
        ));
    }

    /**
     * @Route("/empresa", name="ya_logado_empresa")
     */
    public function login(Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $empresa = $this->getUser();

        if($empresa->getPrimeraClave()){

            $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => ['attr' => ['placeholder' => 'Contraseña']],
                'second_options' => ['attr' => ['placeholder' => 'Repetir Contraseña']],
                'invalid_message' => 'Los campos de contraseña deben ser iguales',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?=.{8})(?=.*[A-Z])(?=.*[a-z])(?=.*\d)/',
                        'message' => 'La contraseña debe tener al menos 8 carácteres, una letra mayúscula, una letra minúscula y un número.',
                    ]),
                ],
            ))
            ->add('save', SubmitType::class, array('label' => 'Guardar'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $formData = $form->getData();

                $empresa->setPassword(password_hash($formData['password'], PASSWORD_BCRYPT));
                $empresa->setPrimeraClave(0);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($empresa);
                $entityManager->flush();

                $this->get('session')->set('templateURL', '/dashboard/common/initPage.html.twig');
                $this->get('session')->set('page', 'paginaInicial');

                return $this->render('dashboard/inicio.html.twig', array('changed' => true));
            }

            return $this->render('empresa/primerLogin.html.twig', array(
                'form' => $form->createView(),
            ));
        }else{
            $this->get('session')->set('templateURL', '/dashboard/common/initPage.html.twig');
            $this->get('session')->set('page', 'paginaInicial');

            return $this->render('dashboard/inicio.html.twig');
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/perfil", name="perfil_empresa")
     */
    public function perfil(Request $request, int $codEmpresa): Response
    {

        //TODO validar si el usuario logado es un gestor que la Empresa sea suya
        
        $this->get('session')->set('templateURL', 'empresa/perfil.html.twig');
        $this->get('session')->set('page', 'perfilEmpresa');

        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if(!empty($empresa)){

            $ccaa = $this->getDoctrine()->getRepository(CCAA::class)->find($empresa->getCodComunidad());
            $provincia = $this->getDoctrine()->getRepository(Provincias::class)->find($empresa->getCodProvincia()); 

            $form = $this->createFormBuilder()
                ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre', 'empty_data' => $empresa->getNombre()]]) 
                ->add('cif', TextType::class, ['attr' => ['placeholder' => 'Teléfono', 'empty_data' => $empresa->getCif()]])
                ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección', 'empty_data' => $empresa->getDireccion()]])
                ->add('telefono', TextType::class, ['attr' => ['placeholder' => 'Teléfono', 'empty_data' => $empresa->getTelefono()]])
                ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Correo electrónico', 'empty_data' => $empresa->getEmail()]])
                ->add('codComunidad', EntityType::class, [
                    'class' => CCAA::class,
                    'choice_label' => 'nombre',
                    'data' => $ccaa,
                ])
                ->add('codProvincia', EntityType::class, [
                    'class' => Provincias::class,
                    'choice_label' => 'nombre',
                    'data' => $provincia,
                ])
                ->add('registroHorario', ChoiceType::class, array(
                    'choices' => array(
                        'Si' => '1',
                        'No' => '0'),
                        'data' => $empresa->getRegistroHorario(),
                    )
                )
                ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal', 'empty_data' => $empresa->getCp()]])
                ->add('save', SubmitType::class, ['label' => 'Modificar datos'])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
            
                $formData = $form->getData();
                $empresa = self::modificaEmpresaMapper($formData, $empresa);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();

                return $this->render('dashboard/inicio.html.twig', array(
                    'form' => $form->createView(),
                    'modificado' => true,
                    'empresa' => $empresa
                ));
            }

            return $this->render('dashboard/inicio.html.twig', array(
                'form' => $form->createView(),
                'modificado' => '',
                'empresa' => $empresa
            ));

        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/clave", name="enviar_clave_empresa")
     */
    public function envioClave(int $codEmpresa, MailerInterface $mailer, Utils $utiles): Response
    {

        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {

            $this->get('session')->set('templateURL', 'gestores/listaEmpresas.html.twig');
            $this->get('session')->set('page', 'listaEmpresa');

            $random_passwd = $utiles->generateStrongPassword();
            $empresa->setPassword($random_passwd);

            if(!$utiles->sendPassEmail($mailer, $empresa->getNombre(),$empresa->getEmail(),$empresa->getPassword())){
                return $this->redirectToRoute('lista_empresas', array('errMail' => true));
            }else{
                
                $empresa->setPassword(password_hash($empresa->getPassword(), PASSWORD_BCRYPT));
                $empresa->setClaveEnviada(1);
                $empresa->setActivo(1);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($empresa);
                $entityManager->flush();

                return $this->redirectToRoute('lista_empresas', array('mandado' => true, 'nombre' => $empresa->getNombre()));
            }
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/listaTrabajadores", name="lista_trabajadores")
     */
    public function getListaTrabajadores(int $codEmpresa): Response
    {
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){

            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {
            $listaTrabajadores = $this->getDoctrine()->getRepository(Trabajador::class)->findBy([
                'codEmpresa' => $codEmpresa,
            ]);

            $this->get('session')->set('templateURL', 'empresa/listaTrabajadores.html.twig');
            $this->get('session')->set('page', 'listaTrabajadores');

            return $this->render('dashboard/inicio.html.twig', array(
                'lista' => $listaTrabajadores,
                'empresa' => $empresa,
            ));
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/listaInformes", name="lista_informes_empresa")
     */
    public function getListaInforme(Request $request, int $codEmpresa, Utils $utiles): Response
    {
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles()) || in_array("ROLE_ADMIN", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {

            $lista = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findBy(
                array('codEmpresa' => $codEmpresa),
                array('fechaCreacion' => 'DESC')
            );

            $this->get('session')->set('templateURL', 'empresa/listaInformes.html.twig');
            $this->get('session')->set('page', 'listaInformesEmpresa');

            $array_anios = array();

            $anios = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->findAniosDisponibles($codEmpresa);

            foreach ($anios as $anio) {
                $array_anios[$anio['anio']] = $anio['anio'];
            }

            $vacio = true;
            if(sizeof($array_anios)>0){
                $vacio = false;
            }

            $form = $this->createFormBuilder()
                ->add('anio', ChoiceType::class, [
                    'choices'  => $array_anios,
                    'placeholder'   => '-- Elija el Año --',
                ])
                //->add('vistaPrevia', SubmitType::class, ['label' => 'Vista Previa'])
                ->add('save', SubmitType::class, ['label' => 'Guardar Informe'])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();

                $entityManager = $this->getDoctrine()->getManager();

                $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->deleteRegistroByEmpresaYAnio($empresa,$formData['anio']);

                $nuevoInforme = new InformeRegistroSalarial();

                $nuevoInforme->setAnio($formData['anio']);
                $nuevoInforme->setCodEmpresa($empresa);
                $nuevoInforme->setFechaCreacion(time());

                if ($loggedUser instanceof Gestores) {
                    $nuevoInforme->setCodGestor($loggedUser);
                }

                $entityManager->persist($nuevoInforme);
                $entityManager->flush();

                $informe = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findOneBy([
                    'codEmpresa'    => $empresa,
                    'anio'          => $formData['anio']
                ]);

                self::calculaValoresInformesalarial($entityManager, $codEmpresa, $informe, $utiles, $formData['anio']);

                $lista = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findBy(
                    array('codEmpresa' => $codEmpresa),
                    array('fechaCreacion' => 'DESC')
                );

                $form = $this->createFormBuilder()
                ->add('anio', ChoiceType::class, [
                    'choices'  => $array_anios,
                    'placeholder'   => '-- Elija el Año --',
                ])
                //->add('vistaPrevia', SubmitType::class, ['label' => 'Vista Previa'])
                ->add('save', SubmitType::class, ['label' => 'Guardar Informe'])
                ->getForm();

                $vacio = true;
                if(sizeof($array_anios)>0){
                    $vacio = false;
                }
                
                return $this->render('dashboard/inicio.html.twig', array(
                    'form'      => $form->createView(),
                    'lista'     => $lista,
                    'empresa'   => $empresa,
                    'vacio'     => $vacio,
                    'creada'    => true
                ));
            }

            return $this->render('dashboard/inicio.html.twig', array(
                'form'    => $form->createView(),
                'lista'   => $lista,
                'vacio'   => $vacio,
                'empresa' => $empresa,
                'creada'  => ''
            ));
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/generaInformes", name="genera_informes_empresa")
     */
    public function generaInformeEmpresa(int $codEmpresa): Response
    {
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {
            $lista = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findBy(
                array('codEmpresa' => $codEmpresa),
                array('fechaCreacion' => 'DESC')
            );

            $this->get('session')->set('templateURL', 'empresa/listaInformes.html.twig');
            $this->get('session')->set('page', 'listaInformesEmpresa');

            return $this->render('dashboard/inicio.html.twig', array(
                'lista' => $lista,
                'empresa' => $empresa,
            ));

        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/registroHorario", name="formulario_registro_horario")
     */
    public function formularioRegistroHorario(int $codEmpresa, Request $request): Response
    {
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles()) || in_array("ROLE_ADMIN", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {
            
            $this->get('session')->set('templateURL', 'empresa/formularioHorarios.html.twig');
            $this->get('session')->set('page', 'formularioHorarios');

            $array_trabajadores = array();

            $array_trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->findBy([
                'codEmpresa' =>$codEmpresa
            ],
            [ 'apellidos' => 'DESC']
            );

            $array_trabajadores["Todos"] = 0;

            foreach ($array_trabajador as $trabajador) {
                $array_trabajadores[$trabajador->getApellidos()." ".$trabajador->getNombre()] = $trabajador->getId();
            } 

            $vacio = true;
            if(sizeof($array_trabajadores)>0){
                $vacio = false;
            }

            $form = $this->createFormBuilder()
                ->add('trabajador', ChoiceType::class, [
                    'choices'  => $array_trabajadores,
                    'placeholder'   => '-- Elija el Trabajador --',
                ])
                ->add('fechaInicio', DateType::class, array(
                    'attr' => array(
                        'class' => 'js-datepicker'
                    ),
                    'widget' => 'single_text'
                    )
                )
                ->add('fechaFin', DateType::class, array(
                    'attr' => array(
                        'class' => 'js-datepicker'
                    ),
                    'widget' => 'single_text',
                    'required' => false
                    )
                )
                ->add('save', SubmitType::class, ['label' => 'Descargar Registro Horario'])
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();

                return $this->redirectToRoute('informe_excel_horario_empresa', array(
                    'codEmpresa'      => $empresa->getId(),
                    'codTrabajador'   => $formData['trabajador'],
                    'fechaInicio'     => strtotime($formData['fechaInicio']->format("d-m-Y 00:00:00")),
                    'fechaFin'        => strtotime($formData['fechaFin']->format("d-m-Y 23:59:59"))
                ));
            }

            return $this->render('dashboard/inicio.html.twig', array(
                'form'      => $form->createView(),
                'empresa'   => $empresa,
                'vacio'     => $vacio
            ));

        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    private static function modificaEmpresaMapper($formData, $empresa){

        $empresa->setNombre($formData['nombre']);
        $empresa->setDireccion($formData['direccion']);
        $empresa->setTelefono($formData['telefono']);
        $empresa->setEmail($formData['email']);
        $empresa->setCodComunidad($formData['codComunidad']);
        $empresa->setCodProvincia($formData['codProvincia']);
        $empresa->setCp($formData['cp']);
        $empresa->setRegistroHorario($formData['registroHorario']);
        
        return $empresa;

    }

    private static function calculaValoresInformesalarial($entityManager, $codEmpresa, $idInforme, $utiles, $anio){

        $detalleInformesMedia = $entityManager->getRepository(DetalleRegistroSalarial::class)->findDetallesInformeMedia($codEmpresa, $anio);

        foreach ($detalleInformesMedia as $detalle){

            $detalleInformeMedia = new DatosInformeRegistroSalarial();

            $grupo = $entityManager->getRepository(GrupoProfesional::class)->find($detalle['grupo']);
            
            $codComplemento = (!empty($detalle['codComplemento'])) ? $detalle['codComplemento'] : 0;

            $complemento = $entityManager->getRepository(ComplementoSalarial::class)->find($codComplemento);

            $codComplementoExtrasalarial = (!empty($detalle['codComplementoExtrasalarial'])) ? $detalle['codComplementoExtrasalarial'] : 0;

            $complementoExtrasalarial = $entityManager->getRepository(ComplementoExtrasalarial::class)->find($codComplementoExtrasalarial);

            $numTrabajadores = $entityManager->getRepository(DetalleRegistroSalarial::class)->findNumTrabajadores($codEmpresa, $grupo->getId(), $detalle['sexo'], $detalle['concepto'], $anio, $codComplemento);

            $numTrabajadores=$numTrabajadores[0]['numTrabajadores'];

            $detalleInformeMedia->setIdInforme($idInforme);
            $detalleInformeMedia->setGrupo($grupo->getId());
            $detalleInformeMedia->setSexo($detalle['sexo']);
            $detalleInformeMedia->setConcepto($detalle['concepto']);
            $detalleInformeMedia->setDescripcion($detalle['descripcion']);
            $detalleInformeMedia->setImporte($utiles->redondeado($detalle['total']/$numTrabajadores,2));
            $detalleInformeMedia->setBrecha(0);
            $detalleInformeMedia->setTipoCalculo('Media');
            $detalleInformeMedia->setNumTrabajadores($numTrabajadores);
            $detalleInformeMedia->setCodComplemento($complemento);
            $detalleInformeMedia->setCodComplementoExtrasalarial($complementoExtrasalarial);

            $entityManager->persist($detalleInformeMedia);

            $detalleInformesMediana = $entityManager->getRepository(DetalleRegistroSalarial::class)->findDetallesInformeMediana($codEmpresa, $grupo->getId(), $detalle['sexo'], $detalle['concepto'], $anio, $codComplemento);
            $array_calculo = array();

            foreach ($detalleInformesMediana as $detalle){
                $array_calculo[] = $detalle['importe'];
            }

            $mediana = $utiles->calcula_mediana($array_calculo);
            
            $detalleInformeMediana = new DatosInformeRegistroSalarial();

            $detalleInformeMediana->setIdInforme($idInforme);
            $detalleInformeMediana->setGrupo($grupo->getId());
            $detalleInformeMediana->setSexo($detalle['sexo']);
            $detalleInformeMediana->setConcepto($detalle['concepto']);
            $detalleInformeMediana->setDescripcion($detalle['descripcion']);
            $detalleInformeMediana->setBrecha(0);
            $detalleInformeMediana->setImporte($utiles->redondeado($mediana,2));
            $detalleInformeMediana->setTipoCalculo('Mediana');
            $detalleInformeMediana->setNumTrabajadores($numTrabajadores);
            $detalleInformeMediana->setCodComplemento($complemento);
            $detalleInformeMedia->setCodComplementoExtrasalarial($complementoExtrasalarial);

            $entityManager->persist($detalleInformeMediana);
                
        }

        $entityManager->flush();
    }
}
