<?php

namespace App\Entity;

use App\Repository\ConvenioColectivoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConvenioColectivoRepository::class)
 */
class ConvenioColectivo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $nombreConvenio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $salarioConvenio; 

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class, inversedBy="conveniosColectivos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codEmpresa;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreConvenio(): ?string
    {
        return $this->nombreConvenio;
    }

    public function setNombreConvenio(string $nombreConvenio): self
    {
        $this->nombreConvenio = $nombreConvenio;

        return $this;
    }

    public function getSalarioConvenio(): ?float
    {
        return $this->salarioConvenio;
    }

    public function setSalarioConvenio(float $salarioConvenio): self
    {
        $this->salarioConvenio = $salarioConvenio;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }
}
