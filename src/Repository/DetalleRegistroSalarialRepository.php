<?php

namespace App\Repository;

use App\Entity\DetalleRegistroSalarial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DetalleRegistroSalarial|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleRegistroSalarial|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleRegistroSalarial[]    findAll()
 * @method DetalleRegistroSalarial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleRegistroSalarialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleRegistroSalarial::class);
    }

    public function deleteDetalleById($id)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
                'DELETE FROM App\Entity\DetalleRegistroSalarial d
                WHERE d.id = :id'
            )
            ->setParameter('id', $id)
            ->getOneOrNullResult();
    }

    public function findByExampleField(int $codTrbajador, int $codEmpresa)
    {
        return $this->createQueryBuilder('d')
            ->select('d.puesto','d.concepto','d.descripcion')
            ->andWhere('d.codEmpresa = :codTrbajador')
            ->andWhere('d.codTrabajador = :codEmpresa')
            ->setParameter('codTrbajador', $codTrbajador)
            ->setParameter('codEmpresa', $codEmpresa)
            ->orderBy('d.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByIdTrabajadorIdEmpresa(int $codTrbajador, int $codEmpresa)
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('d.id','d.mes','d.anio','d.puesto','g.grupo','d.descripcion','d.horas','d.importe','d.observaciones','d.repetible','case when c.nombre IS NOT NULL then c.nombre else ce.nombre end as nombreComplemento')
        ->from('App\Entity\DetalleRegistroSalarial', 'd')
        ->innerJoin('d.grupo','g')
        ->leftJoin('d.codComplemento','c')
        ->leftJoin('d.codComplementoExtrasalarial','ce')
        ->where('d.codTrabajador = :codigoTrabajador')
        ->andWhere('d.codEmpresa = :codigoEmpresa')
        ->orderBy('d.mes, d.anio')
        ->setParameter('codigoTrabajador', $codTrbajador)
        ->setParameter('codigoEmpresa', $codEmpresa)
        ->getQuery()
        ->getResult();
    }

    public function findAniosDisponibles(int $codEmpresa)
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('DISTINCT (d.anio) as anio')
        ->from('App\Entity\DetalleRegistroSalarial', 'd')
        ->andWhere('d.codEmpresa = :codigoEmpresa')
        ->orderBy('d.anio')
        ->setParameter('codigoEmpresa', $codEmpresa)
        ->getQuery()
        ->getResult();
    }

    public function findDetallesInformeMedia(int $codEmpresa, int $anio)
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('IDENTITY(d.grupo) as grupo, d.sexo, d.concepto, d.descripcion, SUM(d.importe) AS total, IDENTITY(d.codComplemento) as codComplemento, IDENTITY(d.codComplementoExtrasalarial) as codComplementoExtrasalarial')
        ->from('App\Entity\DetalleRegistroSalarial', 'd')
        ->where('d.codEmpresa = :codigoEmpresa')
        ->andWhere('d.anio = :anio')
        ->groupBy('d.grupo, d.sexo, d.concepto, d.descripcion, d.codComplemento, d.codComplementoExtrasalarial')
        ->orderBy('d.concepto, d.sexo')
        ->setParameter('codigoEmpresa', $codEmpresa)
        ->setParameter('anio', $anio)
        ->getQuery()
        ->getResult();
    }

    public function findDetallesInformeMediana(int $codEmpresa, int $grupo, string $sexo, string $concepto, int $anio, int $complemento)
    {
        $entityManager = $this->getEntityManager();

        if(!empty($complemento) && $complemento > 0){
            return $entityManager->createQueryBuilder()
            ->select('g.nombreGrupo', 'd.sexo', 'd.concepto', 'd.descripcion', 'd.importe', 'IDENTITY(d.codComplemento) as codComplemento, IDENTITY(d.codComplementoExtrasalarial) as codComplementoExtrasalarial')
            ->from('App\Entity\DetalleRegistroSalarial', 'd')
            ->innerJoin('d.grupo','g')
            ->where('d.codEmpresa = :codigoEmpresa')
            ->andWhere('d.grupo = :grupo')
            ->andWhere('d.sexo = :sexo')
            ->andWhere('d.concepto = :concepto')
            ->andWhere('d.anio = :anio')
            ->andWhere('d.codComplemento = :complemento')
            ->setParameter('codigoEmpresa', $codEmpresa)
            ->setParameter('grupo', $grupo)
            ->setParameter('sexo', $sexo)
            ->setParameter('concepto', $concepto)
            ->setParameter('anio', $anio)
            ->setParameter('complemento', $complemento)
            ->getQuery()
            ->getResult();
        }else{
            return $entityManager->createQueryBuilder()
            ->select('g.nombreGrupo', 'd.sexo', 'd.concepto', 'd.descripcion', 'd.importe')
            ->from('App\Entity\DetalleRegistroSalarial', 'd')
            ->innerJoin('d.grupo','g')
            ->where('d.codEmpresa = :codigoEmpresa')
            ->andWhere('d.grupo = :grupo')
            ->andWhere('d.sexo = :sexo')
            ->andWhere('d.concepto = :concepto')
            ->andWhere('d.anio = :anio')
            ->setParameter('codigoEmpresa', $codEmpresa)
            ->setParameter('grupo', $grupo)
            ->setParameter('sexo', $sexo)
            ->setParameter('concepto', $concepto)
            ->setParameter('anio', $anio)
            ->getQuery()
            ->getResult();
        }
    }

    public function findNumTrabajadores(int $codEmpresa, int $grupo, string $sexo, string $concepto, int $anio, int $complemento)
    {
        $entityManager = $this->getEntityManager();
        
        if(!empty($complemento) && $complemento>0){
            return $entityManager->createQueryBuilder()
            ->select('COUNT(DISTINCT(d.codTrabajador)) AS numTrabajadores')
            ->from('App\Entity\DetalleRegistroSalarial', 'd')
            ->where('d.codEmpresa = :codigoEmpresa')
            ->andWhere('d.grupo = :grupo')
            ->andWhere('d.sexo = :sexo')
            ->andWhere('d.concepto = :concepto')
            ->andWhere('d.codComplemento = :complemento')
            ->andWhere('d.anio = :anio')
            ->setParameter('codigoEmpresa', $codEmpresa)
            ->setParameter('grupo', $grupo)
            ->setParameter('sexo', $sexo)
            ->setParameter('concepto', $concepto)
            ->setParameter('complemento', $complemento)
            ->setParameter('anio', $anio)
            ->getQuery()
            ->getResult();
        }else{
            return $entityManager->createQueryBuilder()
            ->select('COUNT(DISTINCT(d.codTrabajador)) AS numTrabajadores')
            ->from('App\Entity\DetalleRegistroSalarial', 'd')
            ->where('d.codEmpresa = :codigoEmpresa')
            ->andWhere('d.grupo = :grupo')
            ->andWhere('d.sexo = :sexo')
            ->andWhere('d.concepto = :concepto')
            ->andWhere('d.anio = :anio')
            ->setParameter('codigoEmpresa', $codEmpresa)
            ->setParameter('grupo', $grupo)
            ->setParameter('sexo', $sexo)
            ->setParameter('concepto', $concepto)
            ->setParameter('anio', $anio)
            ->getQuery()
            ->getResult();
        }
    }

    public function findDetallesMensualesByCodEmpresaAndAnio(int $codEmpresa, int $anio)
    {
        $entityManager = $this->getEntityManager(); 

        return $entityManager->createQueryBuilder()
        ->select('t.id as codTrabajador, d.mes, d.anio, t.sexo, t.puesto, g.id as idGrupo, d.observaciones, g.grupo, d.concepto, c.nombreConvenio, case when cs.nombre IS NOT NULL then case when cs.nombre = \'Otro\' then d.descripcion else cs.nombre end when ce.nombre IS NOT NULL then case when ce.nombre = \'Otros\' then d.descripcion else ce.nombre end else d.descripcion end as descr')
        ->from('App\Entity\DetalleRegistroSalarial', 'd')
        ->innerJoin('d.grupo','g')
        ->innerJoin('d.codTrabajador','t')
        ->innerJoin('t.codConvenio','c')
        ->leftJoin('d.codComplemento','cs')
        ->leftJoin('d.codComplementoExtrasalarial','ce')
        ->leftJoin('t.codMotivoReduccion','m')
        ->where('d.codEmpresa = :codigoEmpresa')
        ->andWhere('d.anio = :anio')
        ->andWhere('d.observaciones IS NOT NULL')
        ->orderBy('g.grupo, t.id')
        ->setParameter('codigoEmpresa', $codEmpresa)
        ->setParameter('anio', $anio)
        ->getQuery()
        ->getResult();
        
    }

    public function findDetallesByCodEmpresaAndAnio(int $codEmpresa, int $anio)
    {
        $entityManager = $this->getEntityManager(); 

        return $entityManager->createQueryBuilder()
        ->select('t.id as codTrabajador, t.salarioAnual AS salarioBase, t.codigoContrato AS contrato, t.porcentajeReducida, t.porcentajeJornada , t.sexo, t.fechaInicioEmpresa,t.puesto, t.fechaFinEmpresa, g.id as idGrupo, g.grupo,d.concepto, SUM(d.importe) AS total, c.nombreConvenio, case when cs.nombre IS NOT NULL then case when cs.nombre = \'Otro\' then d.descripcion else cs.nombre end when ce.nombre IS NOT NULL then case when ce.nombre = \'Otros\' then d.descripcion else ce.nombre end else d.descripcion end as descr, m.valor')
        ->from('App\Entity\DetalleRegistroSalarial', 'd')
        ->innerJoin('d.grupo','g')
        ->innerJoin('d.codTrabajador','t')
        ->innerJoin('t.codConvenio','c')
        ->leftJoin('d.codComplemento','cs')
        ->leftJoin('d.codComplementoExtrasalarial','ce')
        ->leftJoin('t.codMotivoReduccion','m')
        ->where('d.codEmpresa = :codigoEmpresa')
        ->andWhere('d.anio = :anio')
        //->andWhere('d.concepto NOT IN (:valorConcepto)')
        ->groupBy('t.id, g.id, d.concepto, d.descripcion, cs.nombre, ce.nombre')
        ->orderBy('g.grupo, t.id')
        ->setParameter('codigoEmpresa', $codEmpresa)
        ->setParameter('anio', $anio)
        //->setParameter('valorConcepto', "salarioBase")
        ->getQuery()
        ->getResult();
        
    }

    function findDistintosConceptos(int $codEmpresa, int $anio){
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
            ->select('d.concepto', 'case when c.nombre IS NOT NULL then case when c.nombre = \'Otro\' then d.descripcion else c.nombre end when ce.nombre IS NOT NULL then case when ce.nombre = \'Otros\' then d.descripcion else ce.nombre end else d.descripcion end as descr','c.tipoRetribucion','c.normalizable','c.anualizable')
            ->from('App\Entity\DetalleRegistroSalarial', 'd')
            ->leftJoin('d.codComplemento','c')
            ->leftJoin('d.codComplementoExtrasalarial','ce')
            ->where('d.codEmpresa = :cod')
            ->andWhere('d.anio = :anio')
            ->groupBy('d.concepto, d.descripcion, d.codComplemento, d.codComplementoExtrasalarial')
            ->setParameter('cod', $codEmpresa)
            ->setParameter('anio', $anio)
            ->orderBy('d.concepto, descr', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDetalleSalarioBaseByCodTrabajadorAndAnio(int $codTrabajador, int $anio)
    {
        $entityManager = $this->getEntityManager(); 

        return $entityManager->createQueryBuilder()
        ->select('SUM(importe) AS total, COUNT(d.id) AS meses')
        ->from('App\Entity\DetalleRegistroSalarial', 'd')
        ->where('d.codTrabajador = :codigoTrabajador')
        ->andWhere('d.anio = :anio')
        ->andWhere('d.concepto = :concepto')
        ->orderBy('mes, anio', 'ASC')
        ->setParameter('codigoTrabajador', $codTrabajador)
        ->setParameter('anio', $anio)
        ->setParameter('concepto', 'salarioBase')
        ->getQuery()
        ->getResult();
        
    }

    // /**
    //  * @return DetalleRegistroSalarial[] Returns an array of DetalleRegistroSalarial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetalleRegistroSalarial
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
