<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520162216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE registro_horario (id INT AUTO_INCREMENT NOT NULL, cod_trabajador_id INT NOT NULL, cod_empresa_id INT NOT NULL, hora INT NOT NULL, tipo VARCHAR(1) NOT NULL, INDEX IDX_3AC9D70AE6516B8D (cod_trabajador_id), INDEX IDX_3AC9D70A7902D32C (cod_empresa_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE registro_horario ADD CONSTRAINT FK_3AC9D70AE6516B8D FOREIGN KEY (cod_trabajador_id) REFERENCES trabajador (id)');
        $this->addSql('ALTER TABLE registro_horario ADD CONSTRAINT FK_3AC9D70A7902D32C FOREIGN KEY (cod_empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE datos_facturacion CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED, CHANGE licencias_disponibles_asistida licencias_disponibles_asistida SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AE04DC83E');
        $this->addSql('DROP INDEX IDX_7CE748AE04DC83E ON reset_password_request');
        $this->addSql('ALTER TABLE reset_password_request DROP user_trabajador_id');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE registro_horario');
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE datos_facturacion CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE carta_digital carta_digital VARCHAR(200) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles_asistida licencias_disponibles_asistida SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE reset_password_request ADD user_trabajador_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AE04DC83E FOREIGN KEY (user_trabajador_id) REFERENCES trabajador (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_7CE748AE04DC83E ON reset_password_request (user_trabajador_id)');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
