$(document).ready(function() {
    $("#compraProductos").addClass("active");
    $("#compraLicencias").addClass("menu-is-opening menu-open");
    $("#menuCompras").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

function mandaProducto(idProducto, nameSelect){

    var cantidad = $('#'+nameSelect).val();

    if(cantidad > 0){
        $.ajax({
            type: "GET",
            url: '/gestores/' + idProducto + '/' + cantidad + '/addProducto',
            success: function (data) {
                location.href = '/gestores/cart';
            },
            error: function () {
                // alert('it broke');
            },
            complete: function () {
                // alert('it completed');
            }
        });
    }else{
        alert('Debe seleccionar una cantidar mayor a 0.');
    }
}