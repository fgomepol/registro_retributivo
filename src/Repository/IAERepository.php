<?php

namespace App\Repository;

use App\Entity\IAE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IAE|null find($id, $lockMode = null, $lockVersion = null)
 * @method IAE|null findOneBy(array $criteria, array $orderBy = null)
 * @method IAE[]    findAll()
 * @method IAE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IAERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IAE::class);
    }

    public function findByBusqueda(string $tipo, string $valor)
    {
        $resultado = null;

        if($tipo == "grupo"){
            $resultado = $this->createQueryBuilder('c')
            ->where('c.grupo like :valor')
            ->setParameter('valor', $valor."%")
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
        }else if($tipo == "epigrafe"){
            $resultado = $this->createQueryBuilder('c')
            ->where('c.epigrafe like :valor')
            ->setParameter('valor', $valor."%")
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
        }else if($tipo == "denominacion"){
            $resultado = $this->createQueryBuilder('c')
            ->where('c.denominacion like :valor')
            ->setParameter('valor', "%".$valor."%")
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
        }

        return $resultado;
    }

    // /**
    //  * @return IAE[] Returns an array of IAE objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IAE
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
