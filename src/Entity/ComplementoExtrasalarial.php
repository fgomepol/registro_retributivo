<?php

namespace App\Entity;

use App\Repository\ComplementoExtrasalarialRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementoExtrasalarialRepository::class)
 */
class ComplementoExtrasalarial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo_retribucion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipoRetribucion(): ?string
    {
        return $this->tipo_retribucion;
    }

    public function setTipoRetribucion(string $tipo_retribucion): self
    {
        $this->tipo_retribucion = $tipo_retribucion;

        return $this;
    }
}
