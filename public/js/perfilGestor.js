$(document).ready(function() {
    $("#perfil").addClass("active");

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

$("form[name='form']").validate({
    rules: {
        'form[nombre]': "required",
        'form[telefono]': "required",
        'form[direccion]': "required",
        'form[localidad]': "required",
        'form[cp]': {
            cpValid: true
        },
        'form[email]': {
            emailValid: true
        },
        'form[cif]': {
            docValid: true
        }
    },
    messages: {
        'form[nombre]': "Por favor, introduzca su nombre",
        'form[telefono]': "Por favor, introduzca su telefono",
        'form[direccion]': "Por favor, introduzca su direccion",
        'form[cp]': "Por favor, introduzca un código postal válido",
        'form[cif]': "Introduzca un CIF/NIF/NIE válido.",
        'form[email]': "Por favor, introduce una dirección de correo electrónico válida",
        'form[ccaa]': "Por favor, introduzca su comunidad autónoma",
        'form[provincia]': "Por favor, introduzca su provincia",
        'form[localidad]': "Por favor, introduzca su localidad"
    },
    submitHandler: function(form) {
        form.submit();
    }
});