<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\DetalleRegistroSalarial;
use App\Form\DetalleRegistroSalarialType;
use App\Entity\Empresa;
use App\Entity\Gestores;
use App\Entity\Trabajador;
use App\Utils\Utils;

class DetalleRegistroSalarialController extends AbstractController
{
    
    /**
     * @Route("/empresa/{codEmpresa}/{idDetalle}/detalleRegistroSalarial/delete", name="eliminar_detalle_registro")
     */
    public function eliminarDetalleRegistroSalarial(int $codEmpresa, int $idDetalle): Response
    {
        $datosDetalle = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->findOneBy([
            'id' => $idDetalle,
            'codEmpresa' => $codEmpresa
        ]);

        if($datosDetalle){

            $this->get('session')->set('templateURL', 'detalleRegistroSalarial/detalleRegistro.html.twig');
            $this->get('session')->set('page', 'detalleRegistroSalarial');

            $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->deleteDetalleById($idDetalle);

            return $this->redirectToRoute('detalle_registro_salarial', array('codTrabajador' => $datosDetalle->getCodTrabajador()->getId(), 'codEmpresa' => $datosDetalle->getCodEmpresa()->getId(), 'creada' => 'eliminada'));

        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "El detalle indicado para eliminar no existe."
            ));
        }
    }

    /**
     * @Route("/empresa/detalleRegistroSalarial/deleteMasive", name="eliminar_detalle_registro_masivo")
     */
    public function eliminarDetalleRegistroSalarialMasivo(Request $request): Response
    {
        $this->get('session')->set('templateURL', 'detalleRegistroSalarial/detalleRegistro.html.twig');
        $this->get('session')->set('page', 'detalleRegistroSalarial');

        $loggedUser = $this->getUser();

        $idDetalles=$request->get('idDetalles');

        if($idDetalles){

            $idDetalles = explode(",",$idDetalles);

            $datosDetalle = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->find($idDetalles[0]);

            if($loggedUser instanceof Gestores){
                if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                    $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($datosDetalle->getCodEmpresa()->getId());
                }else{
                    $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                        'codGestor' => $loggedUser->getid(),
                        'id' => $datosDetalle->getCodEmpresa()->getId()
                    ]);
                }
            }elseif($loggedUser instanceof Empresa){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($datosDetalle->getCodEmpresa()->getId());
            }

            if($empresa->getid() != $datosDetalle->getCodTrabajador()->getId()){
                foreach($idDetalles as $detalle){
                    $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->deleteDetalleById($detalle);
                }
            }

            return $this->redirectToRoute('detalle_registro_salarial', array('codTrabajador' => $datosDetalle->getCodTrabajador()->getId(), 'codEmpresa' => $datosDetalle->getCodEmpresa()->getId(), 'creada' => 'eliminada'));

        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Debe seleccionar una lista de detalles para eliminarlos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/{codTrabajador}/detalleRegistroSalarial", name="detalle_registro_salarial")
     */
    public function generaDetalleRegistroSalarial(Request $request, int $codEmpresa, int $codTrabajador, Utils $utiles): Response
    { 
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {

            $trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->findOneBy([
                'id' => $codTrabajador,
                'codEmpresa' => $empresa
            ]);

            $this->get('session')->set('templateURL', 'detalleRegistroSalarial/detalleRegistro.html.twig');
            $this->get('session')->set('page', 'detalleRegistroSalarial');

            $creada = '';
            $lista  = '';

            $fechaActual = new \DateTime();

            $mesActual = $fechaActual->format('m');
            $anioActual = $fechaActual->format('Y');

            $detalleRegistroSalarial = new DetalleRegistroSalarial();

            $detalleRegistroSalarial->setMes($mesActual);
            $detalleRegistroSalarial->setAnio($anioActual);
            $detalleRegistroSalarial->setGrupo($trabajador->getGrupo());
            $detalleRegistroSalarial->setPuesto($trabajador->getPuesto());

            $form = $this->createForm(DetalleRegistroSalarialType::class, $detalleRegistroSalarial);

            $form->get('repetible')->setData(true);
            $form->get('descripcion')->setData("Salario base");

            if (!empty($trabajador) && !empty($empresa)) {
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $formData = $form->getData();

                    $entityManager = $this->getDoctrine()->getManager();
                    $repositorioDetalles = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class);

                    if($formData->getRepetible() && ($anioActual>$formData->getAnio() || ($anioActual == $formData->getAnio() && $mesActual > $formData->getMes()))){
                        self::rellenaDetallesSalario($trabajador, $entityManager, $formData, $repositorioDetalles);
                    }else{
                        $detalleRegistroSalarial = self::detalleRegistroSalarialMapper($formData, $detalleRegistroSalarial, $trabajador);

                        $datosDetalleDB = $repositorioDetalles->findOneBy([
                            'puesto' =>  $detalleRegistroSalarial->getPuesto(),
                            'sexo' =>  $detalleRegistroSalarial->getSexo(),
                            'concepto' =>  $detalleRegistroSalarial->getConcepto(),
                            'importe' =>  $detalleRegistroSalarial->getImporte(),
                            'mes' =>  $detalleRegistroSalarial->getMes(),
                            'anio' =>  $detalleRegistroSalarial->getAnio(),
                            'codEmpresa' =>  $detalleRegistroSalarial->getCodTrabajador()->getCodEmpresa(),
                            'codTrabajador' =>  $detalleRegistroSalarial->getCodTrabajador()
                        ]);
        
                        if(empty($datosDetalleDB)){
                            $entityManager->persist($detalleRegistroSalarial);
                            $entityManager->flush();   
                        }
                    }

                    $creada = true;

                    $detalleRegistroSalarial = new DetalleRegistroSalarial();

                    $detalleRegistroSalarial->setMes($mesActual);
                    $detalleRegistroSalarial->setAnio($anioActual);
                    $detalleRegistroSalarial->setGrupo($trabajador->getGrupo());
                    $detalleRegistroSalarial->setPuesto($trabajador->getPuesto());

                    $form = $this->createForm(DetalleRegistroSalarialType::class, $detalleRegistroSalarial);

                    $form->get('concepto')->setData($formData->getConcepto());
                    $form->get('codComplemento')->setData($formData->getCodComplemento());
                    $form->get('codComplementoExtrasalarial')->setData($formData->getCodComplementoExtrasalarial());
                    $form->get('anio')->setData($formData->getAnio());
                    $form->get('mes')->setData($formData->getMes());
                    $form->get('repetible')->setData($formData->getRepetible());
                    $form->get('descripcion')->setData($formData->getDescripcion());

                    if ($request->query->get('creada') == 'eliminada') {
                        $request->query->remove('creada');
            
                        return $this->redirectToRoute('detalle_registro_salarial', array(
                            'codTrabajador' => $trabajador->getId(),
                            'codEmpresa' => $empresa->getId(),
                            'creada' => $creada
                        ));
                    }
                }
            }

            
            $lista = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->findByIdTrabajadorIdEmpresa($codTrabajador, $codEmpresa);

            return $this->render('dashboard/inicio.html.twig', array(
                'trabajador' => $trabajador,
                'empresa' => $empresa,
                'form' => $form->createView(),
                'creada' => $creada,
                'lista' => $lista
            ));
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/{idDetalle}/modificaDetalleRegistroSalarial", name="modifica_detalle_registro_salarial")
     */
    public function modificaDetalleRegistroSalarial(Request $request, int $codEmpresa, int $idDetalle): Response
    { 
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {
            $datosDetalle = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->find($idDetalle);

            if ($datosDetalle) {
                $this->get('session')->set('templateURL', 'detalleRegistroSalarial/editarRegistro.html.twig');
                $this->get('session')->set('page', 'editarRegistroSalarial');

                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($datosDetalle->getCodEmpresa());
                $trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->find($datosDetalle->getCodTrabajador());

                $form = $this->createForm(DetalleRegistroSalarialType::class, $datosDetalle);

                $creada = '';

                if (!empty($trabajador) && !empty($empresa)) {
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $formData = $form->getData();
                        
                        $detalleRegistroSalarial = self::detalleRegistroSalarialMapper($formData, $datosDetalle, $trabajador);

                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($detalleRegistroSalarial);
                        $entityManager->flush();

                        $creada = 'modificada';

                        $this->get('session')->set('templateURL', 'detalleRegistroSalarial/detalleRegistro.html.twig');
                        $this->get('session')->set('page', 'detalleRegistroSalarial');

                        $fechaActual = new \DateTime();

                        $mesActual = $fechaActual->format('m');
                        $anioActual = $fechaActual->format('Y');

                        $detalleRegistroSalarial = new DetalleRegistroSalarial();

                        $detalleRegistroSalarial->setMes($mesActual);
                        $detalleRegistroSalarial->setAnio($anioActual);
                        $detalleRegistroSalarial->setGrupo($trabajador->getGrupo());
                        $detalleRegistroSalarial->setPuesto($trabajador->getPuesto());

                        $form = $this->createForm(DetalleRegistroSalarialType::class, $detalleRegistroSalarial);

                        return $this->redirectToRoute('detalle_registro_salarial', array(
                            'codTrabajador' => $trabajador->getId(),
                            'codEmpresa' => $empresa->getId(),
                            'creada' => $creada
                        ));
                    }

                    return $this->render('dashboard/inicio.html.twig', array(
                        'trabajador' => $trabajador,
                        'empresa' => $empresa,
                        'form' => $form->createView(),
                        'creada' => $creada,
                        'datosDetalle' => $datosDetalle
                    ));
                } else {
                    $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                    $this->get('session')->set('page', 'error404');
        
                    return $this->render('dashboard/inicio.html.twig', array(
                        'error' => "El detalle del registro indicado para modificar no existe."
                    ));
                }
            } else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');

                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "El detalle a modificar indicado no existe."
                ));
            }
        } else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/empresa/modDetalle", name="modifica_detalle_registro_salarial_2")
     */
    public function modificaDetalleRegistroSalarial_2(Request $request): Response
    { 
        $data = $request->get('json');
        $idDetalle = $data['idDetalle'];
        
        $datosDetalle = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->find($idDetalle);

        if ($datosDetalle) {
            $this->get('session')->set('templateURL', 'detalleRegistroSalarial/editarRegistro.html.twig');
            $this->get('session')->set('page', 'editarRegistroSalarial');

            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($datosDetalle->getCodEmpresa());
            $trabajador = $this->getDoctrine()->getRepository(Trabajador::class)->find($datosDetalle->getCodTrabajador());

            $creada = '';

            if (!empty($trabajador) && !empty($empresa)) {

                $mes = $data['mes'];
                $anio = $data['anio'];
                $horas = !empty($data['horas']) ? $data['horas'] : null;
                $importe = $data['importe'];
                $observaciones = !empty($data['observaciones']) ? $data['observaciones'] : null;

                $datosDetalle->setMes($mes);
                $datosDetalle->setAnio($anio);
                $datosDetalle->setHoras($horas);
                $datosDetalle->setImporte($importe);
                $datosDetalle->setObservaciones($observaciones);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($datosDetalle);
                $entityManager->flush();

                $creada = 'modificada';

                $this->get('session')->set('templateURL', 'detalleRegistroSalarial/detalleRegistro.html.twig');
                $this->get('session')->set('page', 'detalleRegistroSalarial');

                return $this->redirectToRoute('detalle_registro_salarial', array(
                    'codTrabajador' => $trabajador->getId(),
                    'codEmpresa' => $empresa->getId(),
                    'creada' => $creada
                ));
            } else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');
    
                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "El detalle del registro indicado para modificar no existe."
                ));
            }
        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "El detalle a modificar indicado no existe."
            ));
        }
    }

    private static function detalleRegistroSalarialMapper($formData, $detalleRegistroSalarial, $trabajador){

        $detalleRegistroSalarial->setPuesto($formData->getPuesto());
        $detalleRegistroSalarial->setSexo($trabajador->getSexo());
        $detalleRegistroSalarial->setConcepto($formData->getConcepto());
        $detalleRegistroSalarial->setDescripcion($formData->getDescripcion());
        $detalleRegistroSalarial->setHoras($formData->getHoras());
        $detalleRegistroSalarial->setImporte($formData->getImporte());
        $detalleRegistroSalarial->setMes($formData->getMes());
        $detalleRegistroSalarial->setAnio($formData->getAnio());
        $detalleRegistroSalarial->setObservaciones($formData->getObservaciones());
        $detalleRegistroSalarial->setCodEmpresa($trabajador->getCodEmpresa());
        $detalleRegistroSalarial->setCodTrabajador($trabajador);        
        $detalleRegistroSalarial->setGrupo($formData->getGrupo());
        $detalleRegistroSalarial->setCodComplemento($formData->getCodComplemento());
        $detalleRegistroSalarial->setCodComplementoExtrasalarial($formData->getCodComplementoExtrasalarial());
        
        return $detalleRegistroSalarial;
    }

    private static function rellenaDetallesSalario($trabajador, $entityManager, $formData, $repositorioDetalle){
        
        $mesInicio = $formData->getMes();
        $anioInicio = $formData->getAnio();

        if($anioInicio < 2020){
            $mesInicio = 1;
            $anioInicio = 2020;    
        }

        if(!empty($trabajador->getFechaFinEmpresa()) && $trabajador->getFechaFinEmpresa() < time()){
            $fechaFinContrato = new \DateTime('@' . $trabajador->getFechaFinEmpresa());

            $mesFin  = $fechaFinContrato->format('m');
            $anioFin = $fechaFinContrato->format('Y');
        }else{
            $fechaActual = new \DateTime();

            $mesFin  = $fechaActual->format('m');
            $anioFin = $fechaActual->format('Y');

            if($fechaActual->format('d') < 20){
                if($mesFin == 1){
                    $mesFin = 12;
                    $anioFin = $anioFin - 1;
                }else{
                    $mesFin = $mesFin - 1;
                }
            }
        }

        for($y = $anioInicio; $y <= $anioFin; $y++){
            
            $mesMaximo = ($y == $anioFin && 12 > $mesFin) ? $mesFin : 12;

            for($m = $mesInicio; $m <= $mesMaximo; $m++){
                
                $detalleSalarial = new DetalleRegistroSalarial();

                $detalleSalarial->setPuesto($trabajador->getPuesto());
                $detalleSalarial->setSexo($trabajador->getSexo());
                $detalleSalarial->setConcepto($formData->getConcepto());
                $detalleSalarial->setDescripcion($formData->getDescripcion());
                $detalleSalarial->setImporte($formData->getImporte());
                $detalleSalarial->setMes($m);
                $detalleSalarial->setAnio($y);
                $detalleSalarial->setCodEmpresa($trabajador->getCodEmpresa());
                $detalleSalarial->setCodTrabajador($trabajador);        
                $detalleSalarial->setGrupo($trabajador->getGrupo());
                $detalleSalarial->setRepetible(true);
                $detalleSalarial->setHoras($formData->getHoras());
                $detalleSalarial->setObservaciones($formData->getObservaciones());
                $detalleSalarial->setCodComplemento($formData->getCodComplemento());
                $detalleSalarial->setCodComplementoExtrasalarial($formData->getCodComplementoExtrasalarial());

                $entityManager->persist($detalleSalarial);
                $entityManager->flush();
            }
        }
    }
}
