function validar_dni_nif_nie(value){
 
    var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
    var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    var str = value.toString().toUpperCase();

    if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

    var nie = str
        .replace(/^[X]/, '0')
        .replace(/^[Y]/, '1')
        .replace(/^[Z]/, '2');

    var letter = str.substr(-1);
    var charIndex = parseInt(nie.substr(0, 8)) % 23;

    if (validChars.charAt(charIndex) === letter) return true;

    return false;
}

function isValidCif(cif) {
	if (!cif || cif.length !== 9) {
		return false;
	}

	var letters = ['J', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
	var digits = cif.substr(1, cif.length - 2);
	var letter = cif.substr(0, 1);
	var control = cif.substr(cif.length - 1);
	var sum = 0;
    var i;
	var digit;

	if (!letter.match(/[A-Z]/)) {
		return false;
	}

	for (i = 0; i < digits.length; ++i) {
		digit = parseInt(digits[i]);

		if (isNaN(digit)) {
			return false;
		}

		if (i % 2 === 0) {
			digit *= 2;
			if (digit > 9) {
				digit = parseInt(digit / 10) + (digit % 10);
			}

			sum += digit;
		} else {
			sum += digit;
		}
	}

	sum %= 10;
	if (sum !== 0) {
		digit = 10 - sum;
	} else {
		digit = sum;
	}

	if (letter.match(/[ABEH]/)) {
		return String(digit) === control;
	}
	if (letter.match(/[NPQRSW]/)) {
		return letters[digit] === control;
	}

	return String(digit) === control || letters[digit] === control;
}

function validar_documento_identidad(value){

    if(!isValidCif(value)){
        return validar_dni_nif_nie(value);
    }

    return true;
}

function validar_cp(value){

    var cpRexp = /^(?:0[1-9]\d{3}|[1-4]\d{4}|5[0-2]\d{3})$/i;
    
    if (!cpRexp.test(value)) return false;

    return true;
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function fn_ValidateIBAN(IBAN) {

    //Se pasa a Mayusculas
    IBAN = IBAN.toUpperCase();
    //Se quita los blancos de principio y final.
    IBAN = IBAN.trim();
    IBAN = IBAN.replace(/\s/g, ""); //Y se quita los espacios en blanco dentro de la cadena

    var letra1,letra2,num1,num2;
    var isbanaux;
    var numeroSustitucion;
    //La longitud debe ser siempre de 24 caracteres
    if (IBAN.length != 24) {
        return false;
    }

    // Se coge las primeras dos letras y se pasan a números
    letra1 = IBAN.substring(0, 1);
    letra2 = IBAN.substring(1, 2);
    num1 = getnumIBAN(letra1);
    num2 = getnumIBAN(letra2);
    //Se sustituye las letras por números.
    isbanaux = String(num1) + String(num2) + IBAN.substring(2);
    // Se mueve los 6 primeros caracteres al final de la cadena.
    isbanaux = isbanaux.substring(6) + isbanaux.substring(0,6);

    //Se calcula el resto, llamando a la función modulo97, definida más abajo
    resto = modulo97(isbanaux);
    if (resto == 1){
        return true;
    }else{
        return false;
    }
}

function modulo97(iban) {
    var parts = Math.ceil(iban.length/7);
    var remainer = "";

    for (var i = 1; i <= parts; i++) {
        remainer = String(parseFloat(remainer+iban.substr((i-1)*7, 7))%97);
    }

    return remainer;
}

function getnumIBAN(letra) {
    ls_letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return ls_letras.search(letra) + 10;
}

function calcularIBAN(ncuenta, cpais) {
    if (cpais.length != 2)
    return "";
    else {
    var aux;var csiguientes;var tmp;var csiguientes;
    ncuenta = ncuenta + (cpais.charCodeAt(0) - 55).toString() + (cpais.charCodeAt(1) - 55).toString() + "00";tmp = parseInt(ncuenta.substring(0, 9), 10) % 97;
    if (tmp < 10)
    aux = "0";
    else
    aux = "";aux=aux + tmp.toString();ncuenta = ncuenta.substring(9);
    while (ncuenta!="") {
    if (parseInt(aux, 10) < 10)
    csiguientes = 8;
    else
    csiguientes = 7;
    if (ncuenta.length<csiguientes) {
    aux=aux + ncuenta;ncuenta="";
    }
    else {
    aux=aux + ncuenta.substring(0, csiguientes);ncuenta=ncuenta.substring(csiguientes);
    }
    tmp = parseInt(aux, 10) % 97;
    if (tmp < 10)
    aux = "0";
    else
    aux = "";
    aux=aux + tmp.toString();
    }
    tmp = 98 - parseInt(aux, 10);
    if (tmp<10)
    return cpais + "0" + tmp.toString()+ncuenta;
    else
    return cpais + tmp.toString()+ncuenta;
    }
}

$.validator.addMethod("dniValid", function(value, element) {
    return validar_dni_nif_nie(value);
}, 'Introduzca un NIF/NIE válido.');

$.validator.addMethod("docValid", function(value, element) {
    return validar_documento_identidad(value);
}, 'Introduzca un CIF/NIF/NIE válido.');

$.validator.addMethod("cpValid", function(value, element) {
    return validar_cp(value);
}, 'Por favor, introduzca un código postal válido.');

$.validator.addMethod("emailValid", function(value, element) {
    return validateEmail(value);
}, 'Por favor, introduzca una dirección de correo electrónico válida.');

$.validator.addMethod("ibanValid", function(value, element) {
    return fn_ValidateIBAN(value);
}, 'La cuenta IBAN introducida no es correcta.');