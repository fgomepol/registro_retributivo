<?php

namespace App\Repository;

use App\Entity\RegistroHorario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegistroHorario|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegistroHorario|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegistroHorario[]    findAll()
 * @method RegistroHorario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegistroHorarioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistroHorario::class);
    }

    public function findByDate(int $codTrabajador, int $codEmpresa, int $fechaInicio, int $fechaFin)
    {

        if($codTrabajador == 0){

            return $this->createQueryBuilder('r')
                ->select('r')
                ->Where('r.codEmpresa = :codEmpresa')
                ->andWhere('r.horaEntrada >=:fechaInicio')
                ->andWhere('r.horaSalida <=:fechaFin')
                ->setParameter('codEmpresa', $codEmpresa)
                ->setParameter('fechaInicio', $fechaInicio)
                ->setParameter('fechaFin', $fechaFin)
                ->orderBy('r.codTrabajador, r.id', 'ASC')
                ->getQuery()
                ->getResult()
            ;
        }else{
            return $this->createQueryBuilder('r')
                ->select('r')
                ->Where('r.codEmpresa = :codEmpresa')
                ->andWhere('r.codTrabajador = :codTrabajador')
                ->andWhere('r.horaEntrada >= :fechaInicio')
                ->andWhere('r.horaSalida <= :fechaFin')
                ->setParameter('codEmpresa', $codEmpresa)
                ->setParameter('codTrabajador', $codTrabajador)
                ->setParameter('fechaInicio', $fechaInicio)
                ->setParameter('fechaFin', $fechaFin)
                ->orderBy('r.codTrabajador, r.id', 'ASC')
                ->getQuery()
                ->getResult()
            ;
        }
    }

    // /**
    //  * @return RegistroHorario[] Returns an array of RegistroHorario objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RegistroHorario
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
