<?php

namespace App\Repository;

use App\Entity\ConvenioColectivo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConvenioColectivo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConvenioColectivo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConvenioColectivo[]    findAll()
 * @method ConvenioColectivo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConvenioColectivoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConvenioColectivo::class);
    }

    public function findByCodEmpresa(int $empresa){
        // if($empresa == $convenio -> getCodEmpresa()){ return $convenio->getNombreConvenio(); }
        return $this->createQueryBuilder('c')
            ->select('c.id','c.nombreConvenio')
            ->andWhere('c.codEmpresa = :val')
            ->setParameter('val', $empresa)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return ConvenioColectivo[] Returns an array of ConvenioColectivo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConvenioColectivo
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
