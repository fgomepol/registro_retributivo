<?php

namespace App\Repository;

use App\Entity\DetalleCompras;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DetalleCompras|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleCompras|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleCompras[]    findAll()
 * @method DetalleCompras[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleComprasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleCompras::class);
    }

    // /**
    //  * @return DetalleCompras[] Returns an array of DetalleCompras objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetalleCompras
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
