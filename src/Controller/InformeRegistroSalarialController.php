<?php

namespace App\Controller;

use App\Entity\DatosInformeRegistroSalarial;
use App\Entity\Gestores;
use App\Entity\InformeRegistroSalarial;
use App\Entity\DetalleRegistroSalarial;
use App\Entity\Empresa;
use App\Entity\Trabajador;
use App\Utils\Utils;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Finder\Finder;

class InformeRegistroSalarialController extends AbstractController
{
     /**
     * @Route("/gestor/listaInformes", name="lista_informes_gestor")
     */
    public function getListaInformes(): Response
    {
        $gestor = $this->getUser();

        if($gestor instanceof Gestores){

            if(in_array('ROLE_ADMIN', $gestor->getRoles())){
                $listaInformes = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findAllInformes();
            }else if(in_array('ROLE_REGISTRO', $gestor->getRoles())){
                $listaInformes = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findAllInformesRegistro();
            }else{
                $listaInformes = $this->getDoctrine()->getRepository(InformeRegistroSalarial::class)->findInformesByGestor($gestor->getId());
            }
        }

        $this->get('session')->set('templateURL', 'gestores/listaInformes.html.twig');
        $this->get('session')->set('page', 'informesGeneradosGestor');

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $listaInformes,
        ));
    }

    /**
     * @Route("/empresa/{codEmpresa}/{anio}/informeExcelCompleto",  name="informe_excel_completo")
     */
    public function informeExcelCompleto(int $codEmpresa, int $anio, Utils $utiles)
    {
        $loggedUser = $this->getUser();
        
        if($loggedUser instanceof Gestores){
            if(in_array("ROLE_REGISTRO", $loggedUser->getRoles()) || in_array("ROLE_ADMIN", $loggedUser->getRoles())){
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
            }else{
                $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                    'codGestor' => $loggedUser->getid(),
                    'id' => $codEmpresa
                ]);
            }
        }elseif($loggedUser instanceof Empresa){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($loggedUser->getId());
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->find($codEmpresa);
        }

        if (!empty($empresa)) {

            //Constantes
            define('TRABAJADORES_SEXO','DATOS!$C9:$C200');
            define('TRABAJADORES_SALARIO_BASE','DATOS!$CH9:$CH200');
            define('TRABAJADORES_SALARIO_BASE2','DATOS!$CH9:$CH200');
            define('TRABAJADORES_GRUPO','DATOS!$W9:$W200');
            define('TOTAL_COPM_SAL','DATOS!$CI9:$CI200');
            define('TOTAL_SAL_EF','DATOS!$CJ9:$CJ200');
            define('TOTAL_COPM_EXT_SAL','DATOS!$CK9:$CK200');
            define('TOTAL_RETRIB','DATOS!$CL9:$CL200');

            //Constantes complementos

            define('COMPL_SAL_00','DATOS!$AE9:$AE200');
            define('COMPL_SAL_01','DATOS!$AF9:$AF200');
            define('COMPL_SAL_02','DATOS!$AG9:$AG200');
            define('COMPL_SAL_03','DATOS!$AH9:$AH200');
            define('COMPL_SAL_04','DATOS!$AI9:$AI200');
            define('COMPL_SAL_05','DATOS!$AJ9:$AJ200');
            define('COMPL_SAL_06','DATOS!$AK9:$AK200');
            define('COMPL_SAL_07','DATOS!$AL9:$AL200');
            define('COMPL_SAL_08','DATOS!$AM9:$AM200');
            define('COMPL_SAL_09','DATOS!$AN9:$AN200');
            define('COMPL_SAL_10','DATOS!$AO9:$AO200');
            define('COMPL_SAL_11','DATOS!$AP9:$AP200');
            define('COMPL_SAL_12','DATOS!$AQ9:$AQ200');
            define('COMPL_SAL_13','DATOS!$AR9:$AR200');
            define('COMPL_SAL_14','DATOS!$AS9:$AS200');
            define('COMPL_SAL_15','DATOS!$AT9:$AT200');
            define('COMPL_SAL_16','DATOS!$AU9:$AU200');
            define('COMPL_SAL_17','DATOS!$AV9:$AV200');
            define('COMPL_SAL_18','DATOS!$AW9:$AW200');
            define('COMPL_SAL_19','DATOS!$AX9:$AX200');
            define('COMPL_SAL_20','DATOS!$AY9:$AY200');
            define('COMPL_SAL_21','DATOS!$AZ9:$AZ200');
            define('COMPL_SAL_22','DATOS!$BA9:$BA200');
            define('COMPL_SAL_23','DATOS!$BB9:$BB200');
            define('COMPL_SAL_24','DATOS!$BC9:$BC200');
            define('COMPL_SAL_25','DATOS!$BD9:$BD200');
            define('COMPL_SAL_26','DATOS!$BE9:$BE200');
            define('COMPL_SAL_27','DATOS!$BF9:$BF200');
            define('COMPL_SAL_28','DATOS!$BG9:$BG200');
            define('COMPL_SAL_29','DATOS!$BH9:$BH200');

            //EXTRA SALARIALES

            define('COMPL_EXT_SAL_00','DATOS!$BI9:$BI200');
            define('COMPL_EXT_SAL_01','DATOS!$BJ9:$BJ200');
            define('COMPL_EXT_SAL_02','DATOS!$BK9:$BK200');
            define('COMPL_EXT_SAL_03','DATOS!$BL9:$BL200');
            define('COMPL_EXT_SAL_04','DATOS!$BM9:$BM200');
            define('COMPL_EXT_SAL_05','DATOS!$BN9:$BN200');
            define('COMPL_EXT_SAL_06','DATOS!$BO9:$BO200');
            define('COMPL_EXT_SAL_07','DATOS!$BP9:$BP200');
            define('COMPL_EXT_SAL_08','DATOS!$BQ9:$BQ200');
            define('COMPL_EXT_SAL_09','DATOS!$BR9:$BR200');

            $compSalArray = array(
                COMPL_SAL_00, COMPL_SAL_01, COMPL_SAL_02, COMPL_SAL_03, COMPL_SAL_04, COMPL_SAL_05, COMPL_SAL_06, COMPL_SAL_07, COMPL_SAL_08, COMPL_SAL_09, 
                COMPL_SAL_10, COMPL_SAL_11, COMPL_SAL_12, COMPL_SAL_13, COMPL_SAL_14, COMPL_SAL_15, COMPL_SAL_16, COMPL_SAL_17, COMPL_SAL_18, COMPL_SAL_19,
                COMPL_SAL_20, COMPL_SAL_21, COMPL_SAL_22, COMPL_SAL_23, COMPL_SAL_24, COMPL_SAL_25, COMPL_SAL_26, COMPL_SAL_27, COMPL_SAL_28, COMPL_SAL_29);
                
            $compExtSalArray = array(COMPL_EXT_SAL_00, COMPL_EXT_SAL_01, COMPL_EXT_SAL_02, COMPL_EXT_SAL_03, COMPL_EXT_SAL_04, COMPL_EXT_SAL_05, COMPL_EXT_SAL_06, COMPL_EXT_SAL_07, COMPL_EXT_SAL_08, COMPL_EXT_SAL_09) ;

            //Datos de la bbdd

            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            $spreadsheet = $reader->load('../public/uploads/Herramienta_Registro_Retributivo_mio_new.xlsx');
            
            $anioReal =substr($anio,-2);
            $fechaInicio = "01-01-".$anioReal;
            $fechaFin = "31-12-".$anioReal;
            $filename = "Informe_".$anio."_".$empresa->getNombre().".xlsx";
            
            // Sheet Inicio
            $sheet = $spreadsheet->setActiveSheetIndex(0);

            $sheet->setCellValue('C6', $empresa->getNombre());
            $sheet->setCellValue('C8', $empresa->getCif());
            $sheet->setCellValue('E10', $fechaInicio);
            $sheet->setCellValue('G10', $fechaFin);

            // Sheet CONC.RETR
            $sheet = $spreadsheet->setActiveSheetIndex(2);

            $filaInicial = 13;
            $filaInicialExtra = 43;

            $ultimoExtrasalarial = '';
            $ultimoSalarial = '';
            $puestoBase = false;

            $celdasSalarial = array('AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH');
            $celdasExtraSalarial = array('BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR');

            $arrayComplementosSalariales = array('F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI'); 
            $arrayComplementosExtraSalariales = array('AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU');

            $array_salariales = array();
            $array_extraSalariales = array();

            $totalConceptosSalariales = 0;
            $totalConceptosExtraSalariales = 0;

            $distintosConceptos = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->findDistintosConceptos($codEmpresa, $anio);

            foreach($distintosConceptos as $concepto){

                if($concepto['concepto'] !== 'complementoExtrasalarial' && $concepto['concepto'] !== 'salarioBase'){

                    if($ultimoSalarial == '' || $ultimoSalarial !== $concepto['descr']){
                        $ultimoSalarial = $concepto['descr'];
                        $array_salariales[$concepto['descr']] = array($celdasSalarial[$totalConceptosSalariales],$arrayComplementosSalariales[$totalConceptosSalariales]);

                        $nombreAgrupacion = $utiles->eliminar_tildes(str_replace(' ','_',strtoupper($concepto['descr'])));
                        $totalConceptosSalariales++;

                        $sheet->setCellValue('C'.$filaInicial, $concepto['descr']);
                        $sheet->setCellValue('D'.$filaInicial, $concepto['descr']);
                        $sheet->setCellValue('E'.$filaInicial, $concepto['tipoRetribucion']);
                        $sheet->setCellValue('F'.$filaInicial, 'Comp. Salarial');
                        $sheet->setCellValue('G'.$filaInicial, $concepto['normalizable']);
                        $sheet->setCellValue('H'.$filaInicial, $concepto['anualizable']);

                        $filaInicial++;
                    }

                }else if($concepto['concepto'] == 'salarioBase' && !$puestoBase){

                    $sheet->setCellValue('C12', $concepto['descr']);
                    $sheet->setCellValue('D12', $concepto['descr']);
                    $sheet->setCellValue('E12', 'Dinero');
                    $sheet->setCellValue('F12', 'Salario Base');
                    $sheet->setCellValue('G12', 'Sí');
                    $sheet->setCellValue('H12', 'Sí');
                    $puestoBase = true;
                    
                }else if($concepto['concepto'] !== 'salarioBase'){
                    
                    if($ultimoExtrasalarial == '' || $ultimoExtrasalarial !== $concepto['descr']){
                        $ultimoExtrasalarial = $concepto['descr'];
                        $array_extraSalariales[$concepto['descr']] = array($celdasExtraSalarial[$totalConceptosExtraSalariales],$arrayComplementosExtraSalariales[$totalConceptosExtraSalariales]);

                        $nombreAgrupacion = $utiles->eliminar_tildes(str_replace(' ','_',strtoupper($concepto['descr'])));
                        $totalConceptosExtraSalariales++;

                        $sheet->setCellValue('C'.$filaInicialExtra, $concepto['descr']);
                        $sheet->setCellValue('D'.$filaInicialExtra, $concepto['descr']);
                        $sheet->setCellValue('E'.$filaInicialExtra, $concepto['tipoRetribucion']);
                        $sheet->setCellValue('F'.$filaInicial, 'Extrasalarial');
                        $sheet->setCellValue('G'.$filaInicial, $concepto['normalizable']);
                        $sheet->setCellValue('H'.$filaInicial, $concepto['anualizable']);

                        $filaInicialExtra++;
                    }
                }                
            }

            // Sheet DATOS

            $sheet = $spreadsheet->setActiveSheetIndex(3);

            //Datos de fecha
            $sheet->setCellValue(
                'D3',
                '=+Inicio!E10'
            );

            $sheet->setCellValue(
                'D4',
                '=+Inicio!G10'
            );

            $sheet->setCellValue(
                'D5',
                '=D4-D3+1'
            );

            //Cuadro dependiente de CONC.RETR

            $sheet->setCellValue(
                'AD3',
                'Salario Base'
            );

            $sheet->setCellValue(
                'AD4',
                ''
            );

            $sheet->setCellValue(
                'AD5',
                ''
            );

            $sheet->setCellValue(
                'AD6',
                'Salario Base'
            );

            $idSalarial = 0;
            $idextraSalarial = 0;

            foreach($array_salariales as $key => $arr) {
                $sheet->setCellValue(
                    $arr[0].'4',
                    ''
                );

                $sheet->setCellValue(
                    $arr[0].'5',
                    ''
                );

                $sheet->setCellValue(
                    $arr[0].'6',
                    $key
                );

                $idSalarial++;
            }

            foreach($array_extraSalariales as $key => $arr) {
                $sheet->setCellValue(
                    $arr[0].'4',
                    ''
                );

                $sheet->setCellValue(
                    $arr[0].'5',
                    ''
                );

                $sheet->setCellValue(
                    $arr[0].'6',
                    $key
                );

                $idextraSalarial++;
            }

            $filaInicial = 9;
            
            $detalleRegistroSalarial = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->findDetallesByCodEmpresaAndAnio($codEmpresa, $anio);

            $i = 1;
            $codTrabajador = 0;
            $sumaComplementosSalariales = 0;
            $sumaComplementosExtraSalariales = 0;

            $idCeldaSalarial = 0;
            $idCeldaExtraSalarial = 0;
            $ultimoSalarioBase = 0;

            $datosAgrupacion = array();
            $datosGrupoConjunto = array();

            foreach($detalleRegistroSalarial as $detalle){

                if($codTrabajador == 0 || $codTrabajador != $detalle['codTrabajador']){

                    if($codTrabajador !== 0 && $codTrabajador != $detalle['codTrabajador']){

                        // celdas comp. salariales solo importe
                        $sheet->setCellValue('CI'.$filaInicial, $sumaComplementosSalariales); // SUMA TOTAL Comp. Salarial
                        $sheet->setCellValue('CJ'.$filaInicial, $ultimoSalarioBase + $sumaComplementosSalariales); // SUMA Base + Comp. Salarial
                        // celdas comp. extra salariales solo importe

                        $sheet->setCellValue('CK'.$filaInicial, $sumaComplementosExtraSalariales); // SUMA TOTAL Comp. Extra Salarial


                        $sheet->setCellValue('CL'.$filaInicial, $ultimoSalarioBase + $sumaComplementosSalariales + $sumaComplementosExtraSalariales); // SUMA Base + Complementos
                        
                        $filaInicial++;
                        $i++;
                        $sumaComplementosSalariales = 0;
                        $sumaComplementosExtraSalariales = 0;
                    }

                    $codTrabajador = $detalle['codTrabajador'];

                    $fechaInicioContrato = new \DateTime('@' . $detalle['fechaInicioEmpresa']); 

                    if(!empty($detalle['fechaFinEmpresa'])){
                        $fechaFinContrato = new \DateTime('@' . $detalle['fechaFinEmpresa']);
                        $sheet->setCellValue('I'.$filaInicial, $fechaFinContrato->format("d/m/Y"));        // fin contratacion
                    }
                    
                    $sheet->setCellValue('B'.$filaInicial, $i);

                    $sexo='Hombre';
                    
                    if($detalle['sexo'] == 'M'){
                        $sexo='Mujer';
                    }

                    $fechaInicioContracion = '';

                    if(strtotime("01-01-".$anio." 00:00:00") < $detalle['fechaInicioEmpresa'] && $detalle['fechaInicioEmpresa'] > strtotime("31-12-".$anio." 23:59:59")){
                        $fechaInicioCalculo = $fechaInicioContrato->format("d/m/Y");
                        $fechaInicioContracion = $fechaInicioContrato->format("d/m/Y");
                    }else{
                        $fechaInicioCalculo = "01/01/".$anio;
                    }

                    if(!empty($detalle['fechaFinEmpresa']) && strtotime("01-01-".$anio." 00:00:00") < $detalle['fechaFinEmpresa'] && $detalle['fechaFinEmpresa'] > strtotime("31-12-".$anio." 23:59:59")){
                        $fechaFinCalculo = $fechaFinContrato->format("d/m/Y");
                    }else{
                        $fechaFinCalculo = "31/12/".$anio;
                    }

                    $sheet->setCellValue('C'.$filaInicial, $sexo);        // sexo
                    $sheet->setCellValue('H'.$filaInicial, $fechaInicioContracion);        // Inicio contratacion
                    $sheet->setCellValue('J'.$filaInicial, $fechaInicioContrato->format("d/m/Y"));        // Antiguedad

                    if(!empty($detalle['porcentajeJornada'])){
                        $porcentajeJornada = $detalle['porcentajeJornada']."%";
                    }else{
                        $porcentajeJornada = "100%";
                    }
                    
                    $sheet->setCellValue('M'.$filaInicial, $porcentajeJornada);             // % jornada
                    
                    if(!empty($detalle['porcentajeJornada'])){
                        $sheet->setCellValue('N'.$filaInicial, $detalle['porcentajeReducida']."%");        // % jornada Reducida
                        $sheet->setCellValue('O'.$filaInicial, $detalle['valor']);          // Motivo Reduccion
                    }

                    $sheet->setCellValue('P'.$filaInicial, $detalle['contrato']);           // Clave de contrato
                    $sheet->setCellValue('S'.$filaInicial, $detalle['puesto']);             // Puesto
                    $sheet->setCellValue('W'.$filaInicial, $detalle['grupo']);              // Grupo
                    $sheet->setCellValue('Y'.$filaInicial, $detalle['nombreConvenio']);     // Convenio
                    $sheet->setCellValue('AB'.$filaInicial, $detalle['idGrupo']);           // Id Grupo
                    $sheet->setCellValue('BS'.$filaInicial, $fechaInicioCalculo);           // F. Inicio Calculo
                    $sheet->setCellValue('BT'.$filaInicial, $fechaFinCalculo);              // F. Fin Calculo
                    
                    $codTrabajador = $detalle['codTrabajador'];

                    if(!in_array(array('puesto' => $detalle['puesto'], 'convenio' => $detalle['nombreConvenio'], 'grupo' => $detalle['grupo']), $datosAgrupacion)){
                        $datosAgrupacion[] = array('puesto' => $detalle['puesto'], 'convenio' => $detalle['nombreConvenio'], 'grupo' => $detalle['grupo']);
                    }

                    if(!isset($datosGrupoConjunto[$detalle['grupo']])){
                        $datosGrupoConjunto[$detalle['grupo']] = 1;
                    }else{
                        $datosGrupoConjunto[$detalle['grupo']] = $datosGrupoConjunto[$detalle['grupo']]+1;
                    }
                    
                }

                if($detalle['concepto'] == "complementoExtrasalarial"){
                    $sumaComplementosExtraSalariales += $detalle['total'];
                    $sheet->setCellValue($array_extraSalariales[$detalle['descr']][0].$filaInicial, $detalle['total']);
                    $idCeldaExtraSalarial++;
                }else if($detalle['concepto'] !== "salarioBase"){
                    $sumaComplementosSalariales += $detalle['total'];
                    
                    $sheet->setCellValue($array_salariales[$detalle['descr']][0].$filaInicial, $detalle['total']);
                    $idCeldaSalarial++;
                }else if($detalle['concepto'] == "salarioBase"){
                    $ultimoSalarioBase = $detalle['total'];

                    $sheet->setCellValue('AD'.$filaInicial, $ultimoSalarioBase);            // Salario Base Anual
                    $sheet->setCellValue('CH'.$filaInicial, $ultimoSalarioBase);            // Salario Base                    
                }
            }

            // celdas comp. salariales solo importe
            $sheet->setCellValue('CI'.$filaInicial, $sumaComplementosSalariales); // SUMA TOTAL Comp. Salarial
            $sheet->setCellValue('CJ'.$filaInicial, $ultimoSalarioBase + $sumaComplementosSalariales); // SUMA Base + Comp. Salarial
            // celdas comp. extra salariales solo importe
            $sheet->setCellValue('CK'.$filaInicial, $sumaComplementosExtraSalariales); // SUMA TOTAL Comp. Extra Salarial
            $sheet->setCellValue('CL'.$filaInicial, $ultimoSalarioBase + $sumaComplementosSalariales + $sumaComplementosExtraSalariales); // SUMA Base + Complementos
            
            //TODO AGRUPACIONES

            $sheet = $spreadsheet->setActiveSheetIndex(4);
            
            ksort($datosAgrupacion);

            $filaInicio = 14;

            foreach($datosAgrupacion as $key => $arr) {

                $sheet->getCell('D'.$filaInicio)->setValue($arr['puesto']);
                $sheet->getCell('E'.$filaInicio)->setValue($arr['convenio']);
                $sheet->getCell('G'.$filaInicio)->setValue($arr['grupo']);
                
                $filaInicio++;
            }

            ksort($datosGrupoConjunto);
            $arrayIndices=array('I','J','K','L','M','N','O','P','Q','R','S');

            $i = 0;
            foreach($datosGrupoConjunto as $key => $arr) {

                $sheet->getCell($arrayIndices[$i]."13")->setValue($key);
                $sheet->getCell($arrayIndices[$i]."14")->setValue($arr);
                
                $i++;
            }


            //Sheet 1.1.a.Efect.Prom

            $sheet = $spreadsheet->setActiveSheetIndex(5);

            //Datos de fecha
            $sheet->setCellValue(
                'F5',
                '=+Inicio!E10'
            );

            $sheet->setCellValue(
                'G5',
                '=+Inicio!D10'
            );

            $sheet->setCellValue(
                'F6',
                '=+Inicio!G10'
            );

            $sheet->setCellValue(
                'F3',
                '=+"Razón Social: "&Inicio!C6&"  -  NIF: "&Inicio!C8'
            );

            //Datos totales

            //Fila Hombres

            $sheet->setCellValue(
                'C10',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B10)'
                
            );

            $sheet->setCellValue(
                'D10',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B10)'
                
            );

            $sheet->getCell("E10")->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.TRABAJADORES_SALARIO_BASE.')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');
            $sheet->getCell('AJ10')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.TOTAL_COPM_SAL.')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');
            $sheet->getCell('AK10')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.TOTAL_SAL_EF.')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');
            $sheet->getCell('AV10')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.TOTAL_COPM_EXT_SAL.')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');
            $sheet->getCell('AW10')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.TOTAL_RETRIB.')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');

            $i = 0;
            foreach($array_salariales as $key => $arr) {

                $sheet->setCellValue(
                    $arr[1].'8',
                    $key
                );

                $sheet->getCell($arr[1].'10')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.$compSalArray[$i].')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');
                $sheet->getCell($arr[1].'11')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.$compSalArray[$i].')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');
                
                $i++;
            }

            $i = 0;
            foreach($array_extraSalariales as $key => $arr) {

                $sheet->setCellValue(
                    $arr[1].'8',
                    $key
                );
                
                $sheet->getCell($arr[1].'10')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B10, '.$compExtSalArray[$i].')/COUNTIF('.TRABAJADORES_SEXO.',$B10),0)');
                $sheet->getCell($arr[1].'11')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.$compExtSalArray[$i].')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');

                $i++;
            }

            for($i=count($array_salariales);$i<count($arrayComplementosSalariales);$i++){
                $sheet->getColumnDimension($arrayComplementosSalariales[$i])->setVisible(false);
            }

            for($i=count($array_extraSalariales);$i<count($arrayComplementosExtraSalariales);$i++){
                $sheet->getColumnDimension($arrayComplementosExtraSalariales[$i])->setVisible(false);
            }

            //Fila Mujeres
            $sheet->setCellValue(
                'C11',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B11)'
                
            );

            $sheet->setCellValue(
                'D11',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B11)'
            );

            $sheet->getCell("E11")->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.TRABAJADORES_SALARIO_BASE.')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');
            $sheet->getCell('AJ11')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.TOTAL_COPM_SAL.')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');
            $sheet->getCell('AK11')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.TOTAL_SAL_EF.')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');
            $sheet->getCell('AV11')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.TOTAL_COPM_EXT_SAL.')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');
            $sheet->getCell('AW11')->setValue('=IFERROR(SUMIF('.TRABAJADORES_SEXO.',$B11, '.TOTAL_RETRIB.')/COUNTIF('.TRABAJADORES_SEXO.',$B11),0)');
          
            //Datos GRUPALES
            $array_grupos = array(1 => array(16 , 17), 2 => array(19 , 20), 3 => array(22 , 23), 4 => array(25 , 26), 5 => array(28 , 29), 6 => array(31 , 32), 7 => array(34 , 35), 8 => array(37 , 38), 9 => array(40 , 41), 10 => array(43 , 44), 11 => array(46 , 47));

            $celdaGrupo = 15;
            
            foreach($array_grupos as $key => $arr){
                
                $sheet->getCell('C'.$arr[0])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('D'.$arr[0])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('E'.$arr[0])->setValue('=IFERROR(SUMIFS('.TRABAJADORES_SALARIO_BASE.', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                
                $sheet->getCell('AJ'.$arr[0])->setValue('=IFERROR(SUMIFS('.TOTAL_COPM_SAL.', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                $sheet->getCell('AK'.$arr[0])->setValue('=IFERROR(SUMIFS('.TOTAL_SAL_EF.', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                $sheet->getCell('AV'.$arr[0])->setValue('=IFERROR(SUMIFS('.TOTAL_COPM_EXT_SAL.', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                $sheet->getCell('AW'.$arr[0])->setValue('=IFERROR(SUMIFS('.TOTAL_RETRIB.', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');

                $sheet->getCell('C'.$arr[1])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('D'.$arr[1])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('E'.$arr[1])->setValue('=IFERROR(SUMIFS('.TRABAJADORES_SALARIO_BASE.', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');

                $sheet->getCell('AJ'.$arr[1])->setValue('=IFERROR(SUMIFS('.TOTAL_COPM_SAL.', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                $sheet->getCell('AK'.$arr[1])->setValue('=IFERROR(SUMIFS('.TOTAL_SAL_EF.', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                $sheet->getCell('AV'.$arr[1])->setValue('=IFERROR(SUMIFS('.TOTAL_COPM_EXT_SAL.', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                $sheet->getCell('AW'.$arr[1])->setValue('=IFERROR(SUMIFS('.TOTAL_RETRIB.', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');

                $i = 0;
                foreach($array_salariales as $key2 => $arr2) {

                    //echo '=IFERROR(SUMIFS('.$compSalArray[$i].', '.TRABAJADORES_SEXO.',$B'.$arr2[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr2[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)';

                    $sheet->getCell($arr2[1].$arr[0])->setValue('=IFERROR(SUMIFS('.$compSalArray[$i].', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                    $sheet->getCell($arr2[1].$arr[1])->setValue('=IFERROR(SUMIFS('.$compSalArray[$i].', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                    
                    $i++;
                }

                $i = 0;
                foreach($array_extraSalariales as $key2 => $arr2) {
                    
                    $sheet->getCell($arr2[1].$arr[0])->setValue('=IFERROR(SUMIFS('.$compExtSalArray[$i].', '.TRABAJADORES_SEXO.',$B'.$arr[0].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');
                    $sheet->getCell($arr2[1].$arr[1])->setValue('=IFERROR(SUMIFS('.$compExtSalArray[$i].', '.TRABAJADORES_SEXO.',$B'.$arr[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)');

                    $i++;
                }

                $celdaGrupo = $celdaGrupo + 3;
            }

            //Sheet 1.2.a.Efect.Mediana

            $sheet = $spreadsheet->setActiveSheetIndex(6);

            //Datos de fecha
            $sheet->setCellValue(
                'F5',
                '=+Inicio!E10'
            );

            $sheet->setCellValue(
                'G5',
                '=+Inicio!D10'
            );

            $sheet->setCellValue(
                'F6',
                '=+Inicio!G10'
            );

            $sheet->setCellValue(
                'F3',
                '=+"Razón Social: "&Inicio!C6&"  -  NIF: "&Inicio!C8'
            );

            //Datos totales

            //Fila Hombres

            $sheet->setCellValue(
                'C10',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B10)'
                
            );

            $sheet->setCellValue(
                'D10',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B10)'
                
            );

            $sheet->getCell("E10")->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, '.TRABAJADORES_SALARIO_BASE.',FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AJ10')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, '.TOTAL_COPM_SAL.',FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AK10')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, '.TOTAL_SAL_EF.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AV10')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, '.TOTAL_COPM_EXT_SAL.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AW10')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, '.TOTAL_RETRIB.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);

            $i = 0;
            foreach($array_salariales as $key => $arr) {

                $sheet->setCellValue(
                    $arr[1].'8',
                    $key
                );

                $sheet->getCell($arr[1].'10')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, IF('.$compSalArray[$i].' > 0, '.$compSalArray[$i].',""), "")),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell($arr[1].'11')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, IF('.$compSalArray[$i].' > 0, '.$compSalArray[$i].',""), "")),0)',DataType::TYPE_FORMULA_ARRAY);
                $i++;
            }

            $i = 0;
            foreach($array_extraSalariales as $key => $arr) {

                $sheet->setCellValue(
                    $arr[1].'8',
                    $key
                );
                
                $sheet->getCell($arr[1].'10')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B10, IF('.$compExtSalArray[$i].' > 0, '.$compExtSalArray[$i].',""), "")),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell($arr[1].'11')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, IF('.$compExtSalArray[$i].' > 0, '.$compExtSalArray[$i].',""), "")),0)',DataType::TYPE_FORMULA_ARRAY);

                $i++;
            }

            for($i=count($array_salariales);$i<count($arrayComplementosSalariales);$i++){
                $sheet->getColumnDimension($arrayComplementosSalariales[$i])->setVisible(false);
            }

            for($i=count($array_extraSalariales);$i<count($arrayComplementosExtraSalariales);$i++){
                $sheet->getColumnDimension($arrayComplementosExtraSalariales[$i])->setVisible(false);
            }

            //Fila Mujeres
            $sheet->setCellValue(
                'C11',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B11)'
                
            );

            $sheet->setCellValue(
                'D11',
                '=COUNTIF('.TRABAJADORES_SEXO.',$B11)'
            );

            $sheet->getCell("E11")->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, '.TRABAJADORES_SALARIO_BASE.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AJ11')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, '.TOTAL_COPM_SAL.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AK11')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, '.TOTAL_SAL_EF.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AV11')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, '.TOTAL_COPM_EXT_SAL.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
            $sheet->getCell('AW11')->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B11, '.TOTAL_RETRIB.', FALSE)),0)',DataType::TYPE_FORMULA_ARRAY);
          
            //Datos GRUPALES
            $array_grupos = array(1 => array(16 , 17), 2 => array(19 , 20), 3 => array(22 , 23), 4 => array(25 , 26), 5 => array(28 , 29), 6 => array(31 , 32), 7 => array(34 , 35), 8 => array(37 , 38), 9 => array(40 , 41), 10 => array(43 , 44), 11 => array(46 , 47));

            $celdaGrupo = 15;
            
            foreach($array_grupos as $key => $arr){

                //echo '=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', '.TRABAJADORES_SALARIO_BASE.'))),0)';
                                
                $sheet->getCell('C'.$arr[0])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('D'.$arr[0])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[0].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('E'.$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TRABAJADORES_SALARIO_BASE.'>0, '.TRABAJADORES_SALARIO_BASE.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                
                $sheet->getCell('AJ'.$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_COPM_SAL.'>0, '.TOTAL_COPM_SAL.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('AK'.$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_SAL_EF.'>0, '.TOTAL_SAL_EF.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('AV'.$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_COPM_EXT_SAL.'>0, '.TOTAL_COPM_EXT_SAL.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('AW'.$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_RETRIB.'>0, '.TOTAL_RETRIB.')))),0)',DataType::TYPE_FORMULA_ARRAY);

                $sheet->getCell('C'.$arr[1])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('D'.$arr[1])->setValueExplicit('=COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.')',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('E'.$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TRABAJADORES_SALARIO_BASE.'>0, '.TRABAJADORES_SALARIO_BASE.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                
                $sheet->getCell('AJ'.$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_COPM_SAL.'>0, '.TOTAL_COPM_SAL.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('AK'.$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_SAL_EF.'>0, '.TOTAL_SAL_EF.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('AV'.$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_COPM_EXT_SAL.'>0, '.TOTAL_COPM_EXT_SAL.')))),0)',DataType::TYPE_FORMULA_ARRAY);
                $sheet->getCell('AW'.$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.TOTAL_RETRIB.'>0, '.TOTAL_RETRIB.')))),0)',DataType::TYPE_FORMULA_ARRAY);

                $i = 0;
                foreach($array_salariales as $key2 => $arr2) {

                    //echo '=IFERROR(SUMIFS('.$compSalArray[$i].', '.TRABAJADORES_SEXO.',$B'.$arr2[1].', '.TRABAJADORES_GRUPO.', $B'.$celdaGrupo.')/COUNTIFS('.TRABAJADORES_SEXO.',$B'.$arr2[1].','.TRABAJADORES_GRUPO.',$B'.$celdaGrupo.'),0)';

                    $sheet->getCell($arr2[1].$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.$compSalArray[$i].', '.$compSalArray[$i].', ""), ""), "")),0)',DataType::TYPE_FORMULA_ARRAY);
                    $sheet->getCell($arr2[1].$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.$compSalArray[$i].', '.$compSalArray[$i].', ""), ""), "")),0)',DataType::TYPE_FORMULA_ARRAY);
                    
                    $i++;
                }

                $i = 0;
                foreach($array_extraSalariales as $key2 => $arr2) {
                    
                    $sheet->getCell($arr2[1].$arr[0])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[0].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.$compExtSalArray[$i].', '.$compExtSalArray[$i].', ""), ""), "")),0)',DataType::TYPE_FORMULA_ARRAY);
                    $sheet->getCell($arr2[1].$arr[1])->setValueExplicit('=IFERROR(MEDIAN(IF('.TRABAJADORES_SEXO.'=$B'.$arr[1].', IF('.TRABAJADORES_GRUPO.'=$B'.$celdaGrupo.', IF('.$compExtSalArray[$i].', '.$compExtSalArray[$i].', ""), ""), "")),0)',DataType::TYPE_FORMULA_ARRAY);

                    $i++;
                }

                $celdaGrupo = $celdaGrupo + 3;
            }

            $detalleRegistroSalarial = $this->getDoctrine()->getRepository(DetalleRegistroSalarial::class)->findDetallesMensualesByCodEmpresaAndAnio($codEmpresa, $anio);

            if(sizeof($detalleRegistroSalarial)>0){
                $sheet = $spreadsheet->setActiveSheetIndex(7);

                $filaInicial = 2;

                foreach ($detalleRegistroSalarial as $detalle) {

                    $sexo='Hombre';
                        
                    if($detalle['sexo'] == 'M'){
                        $sexo='Mujer';
                    }

                    $sheet->setCellValue('A'.$filaInicial, $sexo);                                  // Sexo
                    $sheet->setCellValue('B'.$filaInicial, $detalle['puesto']);                     // Puesto
                    $sheet->setCellValue('C'.$filaInicial, $detalle['grupo']);                      // Grupo
                    $sheet->setCellValue('D'.$filaInicial, $detalle['nombreConvenio']);             // Convenio
                    $sheet->setCellValue('E'.$filaInicial, $detalle['idGrupo']);                    // idGrupo
                    $sheet->setCellValue('F'.$filaInicial, $detalle['mes']."/".$detalle['anio']);   // mes/anio
                    $sheet->setCellValue('G'.$filaInicial, $detalle['observaciones']);                // Observacion

                    $filaInicial++;
                }
            }            

            //Con esto sacamos por pantalla el informe 
            $writer = new Xlsx($spreadsheet);
            $writer->setPreCalculateFormulas(false);

            $response =  new StreamedResponse(
                function () use ($writer) {
                    $writer->save('php://output');
                }
            );

            $response->headers->set('Content-Type', 'application/vnd.ms-excel');
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'"');
            $response->headers->set('Cache-Control','max-age=0');

            return $response;
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');
        
            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos de empresa introducidos no son correctos."
            ));
        }
    }
}
