<?php

namespace App\Controller;

use App\Form\ContactoType;
use App\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mailer\MailerInterface;

class DefaulltController extends AbstractController
{
    /**
     * @Route("/politicacookies", name="politica_cookies")
     */
    public function politicaCookies()
    {
        return $this->render('politicacookies.html.twig', array(
        ));
    }

    /**
     * @Route("/guia", name="guia_uso")
     */
    public function downloadFileAction(){
        $response = new BinaryFileResponse('../public/uploads/GUIA.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'guia_uso.pdf');
        return $response;
    }

    /**
     * @Route("/descargarAdjunto/{filename}", name="adjunto")
     */
    public function downloadAttachmentAction(string $filename){
        $response = new BinaryFileResponse('../public/uploads/screen_incidencias/'.$filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,$filename);
        return $response;
    }

    /**
     * @Route("/excelpdf", name="convert_excel_to_pdf")
     */
    public function excelToPdf()
    {
        $spreadsheet = new Spreadsheet();
        $writer = new Xlsx($spreadsheet);
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("C:\\registro_retributivo\\src\\Controller\\demolectura.xlsx");
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Dompdf');
        $writer->setPreCalculateFormulas(false);
        $writer->setSheetIndex(6);
        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        $spreadsheet->getActiveSheet()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
        // $writer->setSheetIndex(7);
        // $writer->setSheetIndex(10);
        // $writer->setSheetIndex(11);
        // $writer->setSheetIndex(0,1);
        // $writer->writeAllSheets();
        // $writer->save('php://output');
        $writer->save("C:\\registro_retributivo\\src\\Controller\\demoescritura.pdf");

        exit();
    }

    /**
     * @Route("/nuevopdf", name="crear_pdf")
     */
    public function pdf()
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultfont', 'Arial');

        $dompdf = new Dompdf($pdfOptions);

        $html = $this->renderView('mypdf.html.twig', [
            'title' => 'Título del pdf',
        ]);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4','portrait');

        $dompdf->render();

        $dompdf->stream('mi_pdf.pdf', [
            "Attachment" => true
        ]);

        exit(0);
    }
    
    /**
     * @Route("/", name="index_template")
     */

    public function indexAction(Request $request, Utils $utils, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactoType::class);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            // echo "hasta aqui llegamos";

            $utils->sendContactEmail($mailer, $form->getData());

            $form = $this->createForm(ContactoType::class);
        }

        return $this->render('inicio.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/landing", name="landing_contacto")
     */

    public function landingContacto(Request $request, Utils $utils, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactoType::class);

        $form->handleRequest($request);
        
        $send = false;
        
        if ($form->isSubmitted() && $form->isValid()) {

            $utils->sendContactEmail($mailer, $form->getData());

            $form = $this->createForm(ContactoType::class);

            $send = true;
        }

        return $this->render('landingContacto.html.twig', [
            'form' => $form->createView(),
            'send' => $send
        ]);
    }

    //main dashboard

    /**
     * @Route("/dashboard", name="index_dashboard")
     */

    public function indexDashboardAction(Request $request): Response
    {
        return $this->render('dashboard/inicio.v1.html.twig', array(
        ));
    }

    /**
     * @Route("/dashboard2", name="index_dashboard2")
     */

    public function indexDashboardAction2(Request $request): Response
    {
        return $this->render('dashboard/inicio.v2.html.twig', array(
        ));
    }

    /**
     * @Route("/dashboard3", name="index_dashboard3")
     */

    public function indexDashboardAction3(Request $request): Response
    {
        return $this->render('dashboard/inicio.v3.html.twig', array(
        ));
    }

    /**
     * @Route("/iframe", name="iframe")
     */

    public function iframe(Request $request): Response
    {
        return $this->render('dashboard/iframe.html.twig', array(
        ));
    }

    /**
     * @Route("/starter", name="starter")
     */

    public function starter(Request $request): Response
    {
        return $this->render('dashboard/starter.html.twig', array(
        ));
    }

    //pages

    /**
     * @Route("/widgets", name="widgets")
     */

    public function widgets(Request $request): Response
    {
        return $this->render('dashboard/pages/widgets.html.twig', array(
        ));
    }

    /**
     * @Route("/kanban", name="kanban")
     */

    public function kanban(Request $request): Response
    {
        return $this->render('dashboard/pages/kanban.html.twig', array(
        ));
    }

    /**
     * @Route("/gallery", name="gallery")
     */

    public function gallery(Request $request): Response
    {
        return $this->render('dashboard/pages/gallery.html.twig', array(
        ));
    }

    /**
     * @Route("/calendar", name="calendar")
     */

    public function calendar(Request $request): Response
    {
        return $this->render('dashboard/pages/calendar.html.twig', array(
        ));
    }

    //pages/charts

    /**
     * @Route("/chartjs", name="chartjs")
     */

    public function chartjs(Request $request): Response
    {
        return $this->render('dashboard/pages/charts/chartjs.html.twig', array(
        ));
    }

    /**
     * @Route("/flot", name="flot")
     */

    public function flot(Request $request): Response
    {
        return $this->render('dashboard/pages/charts/flot.html.twig', array(
        ));
    }

    /**
     * @Route("/inline", name="inline")
     */

    public function inline(Request $request): Response
    {
        return $this->render('dashboard/pages/charts/inline.html.twig', array(
        ));
    }

    /**
     * @Route("/uplot", name="uplot")
     */

    public function uplot(Request $request): Response
    {
        return $this->render('dashboard/pages/charts/uplot.html.twig', array(
        ));
    }

    //pages/examples

    /**
     * @Route("/page404", name="page404")
     */

    public function page404(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/404.html.twig', array(
        ));
    }

    /**
     * @Route("/page500", name="page500")
     */

    public function page500(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/500.html.twig', array(
        ));
    }

    /**
     * @Route("/blank", name="blank")
     */

    public function blank(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/blank.html.twig', array(
        ));
    }

    /**
     * @Route("/contact-us", name="contact-us")
     */

    public function contactUs(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/contact-us.html.twig', array(
        ));
    }

    /**
     * @Route("/contacts", name="contacts")
     */

    public function contacts(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/contacts.html.twig', array(
        ));
    }

    /**
     * @Route("/e-commerce", name="e-commerce")
     */

    public function eCommerce(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/e-commerce.html.twig', array(
        ));
    }

    /**
     * @Route("/faq", name="faq")
     */

    public function faq(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/faq.html.twig', array(
        ));
    }

    /**
     * @Route("/forgot-password-v2", name="forgot-password-v2")
     */

    public function forgotPasswordV2(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/forgot-password-v2.html.twig', array(
        ));
    }

    /**
     * @Route("/forgot-password", name="forgot-password")
     */

    public function forgotPassword(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/forgot-password.html.twig', array(
        ));
    }

    /**
     * @Route("/invoice-print", name="invoice-print")
     */

    public function invoicePrint(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/invoice-print.html.twig', array(
        ));
    }

    /**
     * @Route("/invoice", name="invoice")
     */

    public function invoice(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/invoice.html.twig', array(
        ));
    }

    /**
     * @Route("/language-menu", name="language-menu")
     */

    public function languageMenu(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/language-menu.html.twig', array(
        ));
    }

    /**
     * @Route("/legacy-user-menu", name="legacy-user-menu")
     */

    public function legacyUserMenu(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/legacy-user-menu.html.twig', array(
        ));
    }

    /**
     * @Route("/lockscreen", name="lockscreen")
     */

    public function lockscreen(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/lockscreen.html.twig', array(
        ));
    }

    /**
     * @Route("/login-v2", name="login-v2")
     */

    public function loginV2(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/login-v2.html.twig', array(
        ));
    }

    /**
     * @Route("/loginDashboard", name="loginDashboard")
     */

    public function loginDashboard(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/login.html.twig', array(
        ));
    }

    /**
     * @Route("/pace", name="pace")
     */

    public function pace(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/pace.html.twig', array(
        ));
    }

    /**
     * @Route("/profile", name="profile")
     */

    public function profile(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/profile.html.twig', array(
        ));
    }

    /**
     * @Route("/project-add", name="project-add")
     */

    public function projectAdd(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/project-add.html.twig', array(
        ));
    }

    /**
     * @Route("/project-detail", name="project-detail")
     */

    public function projectDetail(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/project-detail.html.twig', array(
        ));
    }

    /**
     * @Route("/project-edit", name="project-edit")
     */

    public function projectEdit(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/project-edit.html.twig', array(
        ));
    }

    /**
     * @Route("/projects", name="projects")
     */

    public function projects(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/projects.html.twig', array(
        ));
    }

    /**
     * @Route("/recover-password-v2", name="recover-password-v2")
     */

    public function recoverPasswordV2(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/recover-password-v2.html.twig', array(
        ));
    }

    /**
     * @Route("/recover-password", name="recover-password")
     */

    public function recoverPassword(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/recover-password.html.twig', array(
        ));
    }

    /**
     * @Route("/register-v2", name="register-v2")
     */

    public function registerV2(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/register-v2.html.twig', array(
        ));
    }

    /**
     * @Route("/register", name="register")
     */

    public function register(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/register.html.twig', array(
        ));
    }

    //pages/forms

    /**
     * @Route("/advanced", name="advanced")
     */

    public function advanced(Request $request): Response
    {
        return $this->render('dashboard/pages/forms/advanced.html.twig', array(
        ));
    }

    /**
     * @Route("/editors", name="editors")
     */

    public function editors(Request $request): Response
    {
        return $this->render('dashboard/pages/forms/editors.html.twig', array(
        ));
    }

    /**
     * @Route("/general", name="general")
     */

    public function general(Request $request): Response
    {
        return $this->render('dashboard/pages/forms/general.html.twig', array(
        ));
    }

    /**
     * @Route("/validation", name="validation")
     */

    public function validation(Request $request): Response
    {
        return $this->render('dashboard/pages/forms/validation.html.twig', array(
        ));
    }

    //pages/layout

    /**
     * @Route("/boxed", name="boxed")
     */

    public function boxed(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/boxed.html.twig', array(
        ));
    }

    /**
     * @Route("/collapsed-sidebar", name="collapsed-sidebar")
     */

    public function collapsedSidebar(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/collapsed-sidebar.html.twig', array(
        ));
    }

    /**
     * @Route("/fixed-footer", name="fixed-footer")
     */

    public function fixedFooter(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/fixed-footer.html.twig', array(
        ));
    }

    /**
     * @Route("/fixed-sidebar-custom", name="fixed-sidebar-custom")
     */

    public function fixedSidebarCustom(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/fixed-sidebar-custom.html.twig', array(
        ));
    }

    /**
     * @Route("/fixed-sidebar", name="fixed-sidebar")
     */

    public function fixedSidebar(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/fixed-sidebar.html.twig', array(
        ));
    }

    /**
     * @Route("/fixed-topnav", name="fixed-topnav")
     */

    public function fixedTopnav(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/fixed-topnav.html.twig', array(
        ));
    }
    
    /**
     * @Route("/top-nav-sidebar", name="top-nav-sidebar")
     */

    public function topNavSidebar(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/top-nav-sidebar.html.twig', array(
        ));
    }

    /**
     * @Route("/top-nav", name="top-nav")
     */

    public function topNav(Request $request): Response
    {
        return $this->render('dashboard/pages/layout/top-nav.html.twig', array(
        ));
    }

    //pages/search

    /**
     * @Route("/enhanced-results", name="enhanced-results")
     */

    public function enhancedResults(Request $request): Response
    {
        return $this->render('dashboard/pages/search/enhanced-results.html.twig', array(
        ));
    }

    /**
     * @Route("/enhanced", name="enhanced")
     */

    public function enhanced(Request $request): Response
    {
        return $this->render('dashboard/pages/search/enhanced.html.twig', array(
        ));
    }

    /**
     * @Route("/simple-results", name="simple-results")
     */

    public function simpleResults(Request $request): Response
    {
        return $this->render('dashboard/pages/search/simple-results.html.twig', array(
        ));
    }

    /**
     * @Route("/simple", name="simple")
     */

    public function simple(Request $request): Response
    {
        return $this->render('dashboard/pages/search/simple.html.twig', array(
        ));
    }

    //pages/tables

    /**
     * @Route("/data", name="data")
     */

    public function data(Request $request): Response
    {
        return $this->render('dashboard/pages/tables/data.html.twig', array(
        ));
    }
    
    /**
     * @Route("/jsgrid", name="jsgrid")
     */

    public function jsgrid(Request $request): Response
    {
        return $this->render('dashboard/pages/tables/jsgrid.html.twig', array(
        ));
    }

    /**
     * @Route("/tableSimple", name="tableSimple")
     */

    public function tableSimple(Request $request): Response
    {
        return $this->render('dashboard/pages/tables/simple.html.twig', array(
        ));
    }

    //pages/UI

    /**
     * @Route("/buttons", name="buttons")
     */

    public function buttons(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/buttons.html.twig', array(
        ));
    }

    /**
     * @Route("/generalUI", name="generalUI")
     */

    public function generalUI(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/general.html.twig', array(
        ));
    }

    /**
     * @Route("/icons", name="icons")
     */

    public function icons(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/icons.html.twig', array(
        ));
    }

    /**
     * @Route("/modals", name="modals")
     */

    public function modals(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/modals.html.twig', array(
        ));
    }

    /**
     * @Route("/navbar", name="navbar")
     */

    public function navbar(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/navbar.html.twig', array(
        ));
    }

    /**
     * @Route("/ribbons", name="ribbons")
     */

    public function ribbons(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/ribbons.html.twig', array(
        ));
    }

    /**
     * @Route("/sliders", name="sliders")
     */

    public function sliders(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/sliders.html.twig', array(
        ));
    }

    /**
     * @Route("/timeline", name="timeline")
     */

    public function timeline(Request $request): Response
    {
        return $this->render('dashboard/pages/UI/timeline.html.twig', array(
        ));
    }

    /**
     * @Route("/kanban2", name="kanban2")
     */

    public function kanban2(Request $request): Response
    {
        return $this->render('dashboard/pages/examples/kanban.html.twig', array(
        ));
    }
}