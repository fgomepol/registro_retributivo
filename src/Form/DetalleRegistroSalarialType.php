<?php

namespace App\Form;

use App\Entity\ComplementoExtrasalarial;
use App\Entity\ComplementoSalarial;
use App\Entity\DetalleRegistroSalarial;
use App\Entity\GrupoProfesional;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class DetalleRegistroSalarialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('concepto', ChoiceType::class, ['choices'  => [
                'Salario Base' => 'salarioBase', 
                'Complemento Salarial' => 'complementoSalarial', 
                'Complemento Extrasalarial' => 'complementoExtrasalarial', 
                'Paga Extra' => 'pagaExtra', ]
                ,])
            ->add('codComplemento', EntityType::class, [
                    'class' => ComplementoSalarial::class,
                    'choice_label' => 'nombre',
                    'placeholder'   => '-- Elija el tipo de complemento --',
                    'required' => false
                ])
            ->add('grupo', EntityType::class, [
                    'class' => GrupoProfesional::class,
                    'choice_label' => 'grupo'
                ])
            ->add('puesto', TextType::class, ['attr' => ['placeholder' => 'Puesto']])
            ->add('codComplementoExtrasalarial', EntityType::class, [
                    'class' => ComplementoExtrasalarial::class,
                    'choice_label' => 'nombre',
                    'placeholder'   => '-- Elija el tipo de complemento --',
                    'required' => false
                ])
            ->add('descripcion', TextType::class, ['attr' => ['placeholder' => 'Descripción']])
            ->add('mes', ChoiceType::class, [
                'choices'  => [
                    '01' => 1, 
                    '02' => 2, 
                    '03' => 3, 
                    '04' => 4, 
                    '05' => 5, 
                    '06' => 6,
                    '07' => 7,
                    '08' => 8,
                    '09' => 9,
                    '10' => 10,
                    '11' => 11,
                    '12' => 12,
                ],
            ])
            ->add('anio', IntegerType::class, ['attr' => ['placeholder' => 'Año']])
            ->add('horas', IntegerType::class, ['attr' => ['placeholder' => 'Horas'],'required' => false])
            ->add('importe', MoneyType::class, ['attr' => ['placeholder' => 'Importe Total']])
            ->add('observaciones', TextareaType::class, ['attr' => ['placeholder' => 'Observaciones'],
            'required' => false])
            ->add('repetible', CheckboxType::class, ['required' => false])
            ->add('save', SubmitType::class, ['label' => 'Guardar datos'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DetalleRegistroSalarial::class,
        ]);
    }
}