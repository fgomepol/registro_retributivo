$(document).ready(function() {
    $("#compraProductos").addClass("active");
    $("#compraLicencias").addClass("menu-is-opening menu-open");
    $("#menuCompras").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

$("form[name='datos_facturacion']").validate({
    rules: {
        'datos_facturacion[nombre]': "required",
        'datos_facturacion[direccion]': "required",
        'datos_facturacion[localidad]': "required",
        'datos_facturacion[cp]': {
            cpValid: true
        },
        'datos_facturacion[cif]': {
            docValid: true
        }
    },
    messages: {
        'datos_facturacion[nombre]': "Por favor, introduzca su razón social",
        'datos_facturacion[direccion]': "Por favor, introduzca su direccion",
        'datos_facturacion[cp]': "Por favor, introduzca un código postal válido",
        'datos_facturacion[cif]': "Introduzca un CIF/NIF/NIE válido.",
        'datos_facturacion[codComunidad]': "Por favor, introduzca su comunidad autónoma",
        'datos_facturacion[codProvincia]': "Por favor, introduzca su provincia",
        'datos_facturacion[localidad]': "Por favor, introduzca su localidad"
    },
    submitHandler: function(form) {
        form.submit();
    }
});