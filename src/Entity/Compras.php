<?php

namespace App\Entity;

use App\Repository\ComprasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComprasRepository::class)
 */
class Compras
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaCompra;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modoPago;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pagado = false;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaPago;

    /**
     * @ORM\ManyToOne(targetEntity=Gestores::class, inversedBy="compras")
     */
    private $codGestor;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class, inversedBy="compras")
     */
    private $codEmpresa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paypalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paypalPayer;

    /**
     * @ORM\OneToMany(targetEntity=DetalleCompras::class, mappedBy="codCompra", orphanRemoval=true)
     */
    private $detalleCompras;

    public function __construct()
    {
        $this->detalleCompras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getFechaCompra(): ?int
    {
        return $this->fechaCompra;
    }

    public function setFechaCompra(int $fechaCompra): self
    {
        $this->fechaCompra = $fechaCompra;

        return $this;
    }

    public function getModoPago(): ?string
    {
        return $this->modoPago;
    }

    public function setModoPago(string $modoPago): self
    {
        $this->modoPago = $modoPago;

        return $this;
    }

    public function getPagado(): ?bool
    {
        return $this->pagado;
    }

    public function setPagado(bool $pagado): self
    {
        $this->pagado = $pagado;

        return $this;
    }

    public function getFechaPago(): ?int
    {
        return $this->fechaPago;
    }

    public function setFechaPago(int $fechaPago): self
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    public function getCodGestor(): ?Gestores
    {
        return $this->codGestor;
    }

    public function setCodGestor(?Gestores $codGestor): self
    {
        $this->codGestor = $codGestor;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    /**
     * Calcula el total de la compra.
     *
     * @return float
     */
    public function getTotal(): float
    {
        $total = 0;

        foreach ($this->detalleCompras as $item) {
            $total += $item->getImporte();
        }

        return $total;
    }

    public function getPaypalCode(): ?string
    {
        return $this->paypalCode;
    }

    public function setPaypalCode(?string $paypalCode): self
    {
        $this->paypalCode = $paypalCode;

        return $this;
    }

    public function getPaypalPayer(): ?string
    {
        return $this->paypalPayer;
    }

    public function setPaypalPayer(string $paypalPayer): self
    {
        $this->paypalPayer = $paypalPayer;

        return $this;
    }

    /**
     * @return Collection|DetalleCompras[]
     */
    public function getDetalleCompras(): Collection
    {
        return $this->detalleCompras;
    }

    public function addDetalleCompra(DetalleCompras $detalleCompra): self
    {
        if (!$this->detalleCompras->contains($detalleCompra)) {
            $this->detalleCompras[] = $detalleCompra;
            $detalleCompra->setCodCompra($this);
        }

        return $this;
    }

    public function removeDetalleCompra(DetalleCompras $detalleCompra): self
    {
        if ($this->detalleCompras->removeElement($detalleCompra)) {
            // set the owning side to null (unless already changed)
            if ($detalleCompra->getCodCompra() === $this) {
                $detalleCompra->setCodCompra(null);
            }
        }

        return $this;
    }
}
