$(document).ready(function() {
    $("#compraProductos").addClass("active");
    $("#compraLicencias").addClass("menu-is-opening menu-open");
    $("#menuCompras").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

var importe = $('#importeTotal').text();

$("#procesarCompra").hide();
$("#transferPayment").hide();

$("#botonTransferencia").click(function() {

  $("#procesarCompra").show();
  $("#payment").hide();

  $.ajax({
      type: "GET",
      url: '/gestores/transferencia/' + importe,
      success: function (data) {
        $("#transferPayment").show();
        $("#procesarCompra").hide();
      },
      error: function (data) {
        $("#payment").show();
        $("#procesarCompra").hide();
      },
      complete: function () {
          // alert('it completed');
      }
  });
});

paypal.Buttons({
    style: {
      shape: 'pill',
      color: 'blue',
      layout: 'vertical',
      label: 'pay',
      
    },
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: importe
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      
      $("#procesarCompra").show();
      $("#payment").hide();
      
      return actions.order.capture().then(function(details) {
          $.ajax({
            type: "GET",
            url: '/gestores/paypal/' + details.id + '/' + details.payer.payer_id + '/' + details.purchase_units[0].amount.value,
            success: function (data) {
              location.href = '/gestores/paypal/confirm/' + details.id + '/' + details.payer.payer_id + '/' + details.status;
            },
            error: function (data) {
              $("#payment").show();
              $("#procesarCompra").hide();
            },
            complete: function () {
                // alert('it completed');
            }
        });
      });
    }
}).render('#paypal-button-container'); // Display payment options on your web page


