<?php

namespace App\Repository;

use App\Entity\Gestores;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Gestores|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gestores|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gestores[]    findAll()
 * @method Gestores[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Gestores[]    findAllbyName(string $nombre)
 * @method Gestores|null loadUserByCredentials(string $user, string $pass)
 */
class GestoresRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gestores::class);
    }

    /**
    * @return Gestores[]
    */
    public function findAllbyName(string $nombre): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT a
            FROM App\Entity\Gestores a
            WHERE a.nombre LIKE :name 
            ORDER BY a.nombre ASC'
        )->setParameter('name', "%".$nombre."%");

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findAllGestoresActivos(): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT a
            FROM App\Entity\Gestores a
            WHERE a.roles LIKE :role 
            ORDER BY a.nombre ASC'
        )->setParameter('role', '["ROLE_GESTOR"]');

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findDestinatarioSoporte(): Gestores
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
            'SELECT u
            FROM App\Entity\Gestores u
            WHERE u.roles LIKE :rol'
        )->setParameter('rol', "%ROLE_SOPORTE%")
        ->getOneOrNullResult();
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Gestores) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function loadUserByUsername(string $username)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
                'SELECT u
                FROM App\Entity\Gestores u
                WHERE u.cif = :username'
            )
            ->setParameter('username', $username)
            ->getOneOrNullResult();
    }

    public function loadUserByCredentials(string $user, string $pass)
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
                'SELECT u
                FROM App\Entity\Gestores u
                WHERE u.cif = :user AND u.password = :pass'
            )
            ->setParameters(array('user'=> $user, 'pass' => $pass))
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Gestores[] Returns an array of Gestores objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gestores
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
