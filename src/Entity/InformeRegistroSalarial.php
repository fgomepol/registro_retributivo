<?php

namespace App\Entity;

use App\Repository\InformeRegistroSalarialRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InformeRegistroSalarialRepository::class)
 */
class InformeRegistroSalarial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @ORM\OneToMany(targetEntity="CentroTrabajo", mappedBy="InformeRegistroSalarial", cascade={"ALL"}, indexBy="id")
     */
    private $codCentro;

    /**
     * @ORM\Column(type="smallint")
     */
    private $anio;

    /**
     * @ORM\Column(columnDefinition="INTEGER UNSIGNED")
     */
    private $fechaCreacion;

    /**
     * @ORM\ManyToOne(targetEntity=Gestores::class, inversedBy="informeRegistroSalariales")
     */
    private $codGestor;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class, inversedBy="informeRegistroSalarial")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codEmpresa;

    /**
     * @ORM\OneToMany(targetEntity=DatosInformeRegistroSalarial::class, mappedBy="idInforme", orphanRemoval=true)
     */
    private $datosInformeRegistroSalarial;

    public function __construct()
    {
        $this->datosInformeRegistroSalarial = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCentro(): ?int
    {
        return $this->codCentro;
    }

    public function setCodCentro(?int $codCentro): self
    {
        $this->codCentro = $codCentro;

        return $this;
    }

    public function getAnio(): ?int
    {
        return $this->anio;
    }

    public function setAnio(int $anio): self
    {
        $this->anio = $anio;

        return $this;
    }

    public function getFechaCreacion(): ?int
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(int $fechaCreacion): self
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    public function getCodGestor(): ?Gestores
    {
        return $this->codGestor;
    }

    public function setCodGestor(?Gestores $codGestor): self
    {
        $this->codGestor = $codGestor;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    /**
     * @return Collection|DatosInformeRegistroSalarial[]
     */
    public function getDatosInformeRegistroSalarial(): Collection
    {
        return $this->datosInformeRegistroSalarial;
    }

    public function addDatosInformeRegistroSalarial(DatosInformeRegistroSalarial $datosInformeRegistroSalarial): self
    {
        if (!$this->datosInformeRegistroSalarial->contains($datosInformeRegistroSalarial)) {
            $this->datosInformeRegistroSalarial[] = $datosInformeRegistroSalarial;
            $datosInformeRegistroSalarial->setIdInforme($this);
        }

        return $this;
    }

    public function removeDatosInformeRegistroSalarial(DatosInformeRegistroSalarial $datosInformeRegistroSalarial): self
    {
        if ($this->datosInformeRegistroSalarial->removeElement($datosInformeRegistroSalarial)) {
            // set the owning side to null (unless already changed)
            if ($datosInformeRegistroSalarial->getIdInforme() === $this) {
                $datosInformeRegistroSalarial->setIdInforme(null);
            }
        }

        return $this;
    }
}
