<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => ['attr' => ['placeholder' => 'Contraseña']],
                'second_options' => ['attr' => ['placeholder' => 'Repetir Contraseña']],
                'invalid_message' => 'Los campos de contraseña deben ser iguales',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?=.{8})(?=.*[A-Z])(?=.*[a-z])(?=.*\d)/',
                        'message' => 'La contraseña debe tener al menos 8 carácteres, una letra mayúscula, una letra minúscula y un número.',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
