<?php

namespace App\Form;

use App\Entity\Empresa;
use App\Entity\CCAA;
use App\Entity\Provincias;
use App\Entity\ActividadesEconomicas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EmpresaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre de la empresa']]) 
            ->add('cif', TextType::class, ['attr' => ['placeholder' => 'CIF/NIF/NIE de la empresa']])
            ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección de la empresa']])
            ->add('telefono', TextType::class, ['attr' => ['placeholder' => 'Teléfono de la empresa']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Correo electrónico de la empresa']])
            ->add('codComunidad', EntityType::class, [
                'class' => CCAA::class,
                'choice_label' => 'nombre',
                'placeholder'   => '-- Elija la Comunidad Autónoma --',
            ])
            ->add('codProvincia', EntityType::class, [
                'class' => Provincias::class,
                'choice_label' => 'nombre',
                'placeholder'   => '-- Elija la Provincia --',
            ])
            ->add('registroHorario', ChoiceType::class, array(
                'choices' => array(
                    'Si' => '1',
                    'No' => '0')
                )
            )
            ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal']])
            ->add('save', SubmitType::class, ['label' => 'Guardar Empresa'])
        ;
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Empresa::class,
        ]);
    }
}