<?php

namespace App\Controller;

use App\Entity\Empresa;
use App\Entity\Gestores;
use App\Entity\Trabajador;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        
        if ($this->getUser()) {

            if(in_array('ROLE_GESTOR', $this->getUser()->getRoles())){
                return $this->redirectToRoute('ya_logado');
            }else if(in_array('ROLE_TRABAJADOR', $this->getUser()->getRoles())){
                return $this->redirectToRoute('ya_logado_trabajador');
            }else if($this->getUser() instanceof Empresa){
                return $this->redirectToRoute('ya_logado_empresa');
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout() 
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
