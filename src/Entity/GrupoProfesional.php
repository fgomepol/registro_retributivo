<?php

namespace App\Entity;

use App\Repository\GrupoProfesionalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GrupoProfesionalRepository::class)
 */
class GrupoProfesional
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $grupo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombreGrupo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrupo(): ?string
    {
        return $this->grupo;
    }

    public function setGrupo(string $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getNombreGrupo(): ?string
    {
        return $this->nombreGrupo;
    }

    public function setNombreGrupo(string $nombreGrupo): self
    {
        $this->nombreGrupo = $nombreGrupo;

        return $this;
    }
}
