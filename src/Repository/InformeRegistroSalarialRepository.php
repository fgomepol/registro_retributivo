<?php

namespace App\Repository;

use App\Entity\Empresa;
use App\Entity\InformeRegistroSalarial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InformeRegistroSalarial|null find($id, $lockMode = null, $lockVersion = null)
 * @method InformeRegistroSalarial|null findOneBy(array $criteria, array $orderBy = null)
 * @method InformeRegistroSalarial[]    findAll()
 * @method InformeRegistroSalarial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformeRegistroSalarialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InformeRegistroSalarial::class);
    }

    public function findAllInformes(): array
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('i.id','i.anio','i.fechaCreacion','e.nombre','c.nombre','IDENTITY(i.codEmpresa) as codEmpresa')
        ->from('App\Entity\InformeRegistroSalarial', 'i')
        ->innerJoin('i.codEmpresa','e')
        ->innerJoin('e.codGestor','c')
        ->getQuery()
        ->getResult();
    }

    public function findInformesByGestor(int $codGestor): array
    {

        //$rsm = new ResultSetMapping();
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('i.id','i.anio','i.fechaCreacion','e.nombre','IDENTITY(i.codEmpresa) as codEmpresa')
        ->from('App\Entity\InformeRegistroSalarial', 'i')
        ->innerJoin('i.codEmpresa','e')
        ->where('i.codGestor = :codigo')
        ->setParameter('codigo', $codGestor)
        ->orderBy('i.fechaCreacion','DESC')
        ->getQuery()
        ->getResult();
    }

    public function findInformesByEmpresa(int $codEmpresa): array
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('i')
        ->from('App\Entity\InformeRegistroSalarial', 'i')
        ->where('i.codEmpresa = :codigo')
        ->setParameter('codigo', $codEmpresa)
        ->getQuery()->getResult();
    }

    public function findAllInformesRegistro(): array
    {
        $entityManager = $this->getEntityManager();
        
        return $entityManager->createQueryBuilder()
        ->select('i.anio', 'i.fechaCreacion', 'IDENTITY(i.codEmpresa) as codEmpresa','e.nombre')
        ->from('App\Entity\InformeRegistroSalarial', 'i')
        ->innerJoin('i.codEmpresa','e')
        ->where('e.tipoLicencia = :tipoLicencia')
        ->setParameter('tipoLicencia', 'Asistida')
        ->getQuery()->getResult();
    }

    public function deleteRegistroByEmpresaYAnio($codEmpresa, $anio)
    {
        $entityManager = $this->getEntityManager();

        $informes_old = $entityManager->getRepository(InformeRegistroSalarial::class)->findBy([
            'codEmpresa'    => $codEmpresa,
            'anio'          => $anio
        ]);

        foreach ($informes_old as $informe){
            $entityManager->createQuery(
                'DELETE FROM App\Entity\DatosInformeRegistroSalarial d
                WHERE d.idInforme = :id'
            )
            ->setParameter('id', $informe->getId())
            ->getOneOrNullResult();
    
            $entityManager->createQuery(
                    'DELETE FROM App\Entity\InformeRegistroSalarial i
                    WHERE i.id = :id'
                )
                ->setParameter('id', $informe->getId())
                ->getOneOrNullResult();
        }
    }

    // /**
    //  * @return InformeRegistroSalarial[] Returns an array of InformeRegistroSalarial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InformeRegistroSalarial
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
