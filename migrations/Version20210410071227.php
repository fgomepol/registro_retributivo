<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210410071227 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE convenio_colectivo ADD cod_gestor_id INT DEFAULT NULL, ADD cod_empresa_id INT NOT NULL');
        $this->addSql('ALTER TABLE convenio_colectivo ADD CONSTRAINT FK_815CCA90E0F7686E FOREIGN KEY (cod_gestor_id) REFERENCES gestores (id)');
        $this->addSql('ALTER TABLE convenio_colectivo ADD CONSTRAINT FK_815CCA907902D32C FOREIGN KEY (cod_empresa_id) REFERENCES empresa (id)');
        $this->addSql('CREATE INDEX IDX_815CCA90E0F7686E ON convenio_colectivo (cod_gestor_id)');
        $this->addSql('CREATE INDEX IDX_815CCA907902D32C ON convenio_colectivo (cod_empresa_id)');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores ADD cod_comunidad_id INT NOT NULL, ADD cod_provincia_id INT NOT NULL, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE gestores ADD CONSTRAINT FK_2D0C8E692FE170C FOREIGN KEY (cod_comunidad_id) REFERENCES ccaa (id)');
        $this->addSql('ALTER TABLE gestores ADD CONSTRAINT FK_2D0C8E664ABF1E8 FOREIGN KEY (cod_provincia_id) REFERENCES provincias (id)');
        $this->addSql('CREATE INDEX IDX_2D0C8E692FE170C ON gestores (cod_comunidad_id)');
        $this->addSql('CREATE INDEX IDX_2D0C8E664ABF1E8 ON gestores (cod_provincia_id)');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE provincias CHANGE cod_comunidad cod_comunidad_id INT NOT NULL');
        $this->addSql('ALTER TABLE provincias ADD CONSTRAINT FK_9F63142792FE170C FOREIGN KEY (cod_comunidad_id) REFERENCES ccaa (id)');
        $this->addSql('CREATE INDEX IDX_9F63142792FE170C ON provincias (cod_comunidad_id)');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE convenio_colectivo DROP FOREIGN KEY FK_815CCA90E0F7686E');
        $this->addSql('ALTER TABLE convenio_colectivo DROP FOREIGN KEY FK_815CCA907902D32C');
        $this->addSql('DROP INDEX IDX_815CCA90E0F7686E ON convenio_colectivo');
        $this->addSql('DROP INDEX IDX_815CCA907902D32C ON convenio_colectivo');
        $this->addSql('ALTER TABLE convenio_colectivo ADD cod_empresa INT DEFAULT NULL, DROP cod_empresa_id, CHANGE cod_gestor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores DROP FOREIGN KEY FK_2D0C8E692FE170C');
        $this->addSql('ALTER TABLE gestores DROP FOREIGN KEY FK_2D0C8E664ABF1E8');
        $this->addSql('DROP INDEX IDX_2D0C8E692FE170C ON gestores');
        $this->addSql('DROP INDEX IDX_2D0C8E664ABF1E8 ON gestores');
        $this->addSql('ALTER TABLE gestores ADD ccaa SMALLINT UNSIGNED DEFAULT NULL, ADD provincia SMALLINT UNSIGNED DEFAULT NULL, DROP cod_comunidad_id, DROP cod_provincia_id, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE provincias DROP FOREIGN KEY FK_9F63142792FE170C');
        $this->addSql('DROP INDEX IDX_9F63142792FE170C ON provincias');
        $this->addSql('ALTER TABLE provincias CHANGE cod_comunidad_id cod_comunidad INT NOT NULL');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
