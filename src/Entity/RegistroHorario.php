<?php

namespace App\Entity;

use App\Repository\RegistroHorarioRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RegistroHorarioRepository::class)
 */
class RegistroHorario
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Trabajador::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codTrabajador;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $codEmpresa;

    /**
     * @ORM\Column(type="integer")
     */
    private $horaEntrada;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $horaSalida;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tiempoTotal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodTrabajador(): ?Trabajador
    {
        return $this->codTrabajador;
    }

    public function setCodTrabajador(?Trabajador $codTrabajador): self
    {
        $this->codTrabajador = $codTrabajador;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    public function getHoraEntrada(): ?int
    {
        return $this->horaEntrada;
    }

    public function setHoraEntrada(int $horaEntrada): self
    {
        $this->horaEntrada = $horaEntrada;

        return $this;
    }

    public function getHoraSalida(): ?int
    {
        return $this->horaSalida;
    }

    public function setHoraSalida(?int $horaSalida): self
    {
        $this->horaSalida = $horaSalida;

        return $this;
    }

    public function getTiempoTotal(): ?int
    {
        return $this->tiempoTotal;
    }

    public function setTiempoTotal(?int $tiempoTotal): self
    {
        $this->tiempoTotal = $tiempoTotal;

        return $this;
    }
}
