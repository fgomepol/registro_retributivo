$(document).ready(function() {
    $("#nuevaFactura").addClass("active");
    $("#compraLicencias").addClass("menu-is-opening menu-open");
    $("#menuCompras").show(); 

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });

    $('#form_nombre').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
    $('#form_cif').rules("add", {
        required        : true,
        docValid        : true,
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
    $('#form_direccion').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
    $('#form_cp').rules("add", {
        required        : true,
        cpValid         : true,                        
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
    $('#form_codComunidad').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
    $('#form_codProvincia').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
    $('#form_localidad').rules("add", {
        required        : true,                             
        messages        : {
            required    : 'Por favor, introduzca la razón social'
        }
    });
});

$('select[name="form[producto]"]').on('change', function () {
        
    var precioProducto = $(this).children("option:selected").text();
    var cantidad = $('select[name="form[cantidad]"]').children("option:selected").val();

    precioProducto = precioProducto.split('-');
    precioProducto = precioProducto[1].split(' ');
    precioProducto = precioProducto[1].replace(',','.');
    

    var precioTotal = new Intl.NumberFormat("de-DE").format(cantidad * precioProducto);

    $("#importeTotal").text(precioTotal + " €");
});

$('select[name="form[cantidad]"]').on('change', function () {
        
    var precioProducto = $('select[name="form[producto]"]').children("option:selected").text();
    var cantidad = $(this).children("option:selected").text();

    precioProducto = precioProducto.split('-');
    precioProducto = precioProducto[1].split(' ');
    precioProducto = precioProducto[1].replace(',','.');
    

    var precioTotal = new Intl.NumberFormat("de-DE").format(cantidad * precioProducto);

    $("#importeTotal").text(precioTotal + " €");
});

$('select[name="form[codGestor]"]').on('change', function () {
        
    var selectedGestor = $(this).children("option:selected").val();

    if(selectedGestor == ""){

        $('#nuevosDatosFacturacion').show();

        $('#form_nombre').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });
        $('#form_cif').rules("add", {
            required        : true,
            docValid        : true,
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });
        $('#form_direccion').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });
        $('#form_cp').rules("add", {
            required        : true,
            cpValid         : true,                        
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });
        $('#form_codComunidad').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });
        $('#form_codProvincia').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });
        $('#form_localidad').rules("add", {
            required        : true,                             
            messages        : {
                required    : 'Por favor, introduzca la razón social'
            }
        });

    }else if(selectedGestor != ""){

        $('#nuevosDatosFacturacion').hide();
        
        $('#form_nombre').val("");
        $('#form_cif').val("");
        $('#form_direccion').val("");
        $('#form_cp').val("");
        $('#form_localidad').val("");

        $('#form_nombre').rules("remove");
        $('#form_cif').rules("remove");
        $('#form_direccion').rules("remove");
        $('#form_cp').rules("remove");
        $('#form_localidad').rules("remove");
        $('#form_codComunidad').rules("remove");
        $('#form_codProvincia').rules("remove");
    }
});

$("form[name='form']").validate({
    rules: {
        'form[producto]': "required"
    },
    messages: {
        'form[producto]': "Por favor, seleccione el producto"
    },
    submitHandler: function(form) {
        form.submit();
    }
});