<?php

namespace App\Controller;

use App\Entity\Gestores;
use App\Entity\Empresa;
use App\Entity\CCAA;
use App\Entity\CNAE;
use App\Entity\CorreoSoporte;
use App\Entity\IAE;
use App\Entity\Provincias;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Utils\Utils;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class GestoresController extends AbstractController
{

    /**
     * @Route("/empresa/{codEmpresa}/cartaDigital", name="carta_digital")
     */
    public function cartaDigital(int $codEmpresa): Response
    {

        $loggedUser = $this->getUser();

        if(in_array('ROLE_ADMIN', $loggedUser->getRoles())){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'id' => $codEmpresa
            ]);
        }else if(in_array('ROLE_REGISTRO', $loggedUser->getRoles())){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'id' => $codEmpresa
            ]);
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'codGestor' => $loggedUser->getid(),
                'id' => $codEmpresa
            ]);
        }

        $this->get('session')->set('templateURL', 'gestores/cartadigital.html.twig');

        if(!empty($empresa->getCartaDigital())){

            return $this->render('dashboard/inicio.html.twig', array(
                'qr' => true,
                'empresa' => $empresa->getNombre(),
                'codEmpresa' => $codEmpresa,
                'qr_image' => $empresa->getCartaDigital()
            ));
        }else{
            return $this->render('dashboard/inicio.html.twig', array(
                'qr' => false,
                'empresa' => $empresa->getNombre(),
                'codEmpresa' => $codEmpresa
            ));
        }
    }

    /**
     * @Route("/empresa/{codEmpresa}/subidaFichero", name="subida_fichero")
     */
    public function subidaFichero(int $codEmpresa): Response
    {

        $loggedUser = $this->getUser();

        if(in_array('ROLE_ADMIN', $loggedUser->getRoles())){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'id' => $codEmpresa
            ]);
        }else if(in_array('ROLE_REGISTRO', $loggedUser->getRoles())){
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'id' => $codEmpresa
            ]);
        }else{
            $empresa = $this->getDoctrine()->getRepository(Empresa::class)->findOneBy([
                'codGestor' => $loggedUser->getid(),
                'id' => $codEmpresa
            ]);
        }

        $this->get('session')->set('templateURL', 'gestores/cartadigital.html.twig');

        if (isset($_POST['submit']) && $_POST['submit'] == 'Enviar') {
            // $uploadDirectory = $_SERVER['DOCUMENT_ROOT']."qr_images\\";
            $uploadDirectory = "https://informeretributivo.com/qr_images/";

            $errors = []; // Store errors here

            $fileExtensionsAllowed = ['jpeg','jpg','png']; // These will be the only file extensions allowed 

            $name = $codEmpresa;
            $fileName = $_FILES['imagen_carta']['name'];
            $fileTmpName  = $_FILES['imagen_carta']['tmp_name'];
            $fileSize = $_FILES['imagen_carta']['size'];
            $fileExtension = explode('.',$fileName);
            $fileExtension = end($fileExtension);
            $fullName = $name.'.'.$fileExtension;

            if (isset($_POST['submit'])) {

                if (!in_array($fileExtension,$fileExtensionsAllowed)) {
                    $errors[] = "Extensión de archivo no permitida. Por favor, use una imagen en formato JPEG o PNG.";
                }

                if ($fileSize > 4000000){
                    $errors[] = "El archivo supera el tamaño máximo permitido (4MB)";
                }

                if (empty($errors)) {
                    if(move_uploaded_file($fileTmpName, $_SERVER['DOCUMENT_ROOT'].'/qr_images/'.$fullName)){ echo "SUBIDA<br>"; }

                    $empresa->setCartaDigital($uploadDirectory.$fullName);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($empresa);
                    $entityManager->flush();

                    return $this->render('dashboard/inicio.html.twig', array(
                        'qr' => true,
                        'empresa' => $empresa->getNombre(),
                        'codEmpresa' => $codEmpresa,
                        'qr_image' => $empresa->getCartaDigital()
                    ));

                } else {

                    return $this->render('dashboard/inicio.html.twig', array(
                        'qr' => false,
                        'empresa' => $empresa->getNombre(),
                        'codEmpresa' => $codEmpresa,
                        'errores' => $errors
                    ));
                }

            }
        }
    }

    /**
     * @Route("/nuevoGestor", name="crear_gestor")
     */
    public function nuevoGestor(Request $request, MailerInterface $mailer, Utils $utiles): Response
    {
        $creada = '';
        $form = $this->createFormBuilder()
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Razón Social']]) 
            ->add('cif', TextType::class, ['attr' => ['placeholder' => 'CIF/NIF/NIE']])
            ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección']])
            ->add('telefono', TextType::class, ['attr' => ['placeholder' => 'Teléfono']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Correo electrónico']])
            ->add('ccaa', EntityType::class, [
                'class' => CCAA::class,
                'choice_label' => 'nombre',
                'placeholder'   => '-- Elija la Comunidad Autónoma --',
            ])
            ->add('provincia', EntityType::class, [
                'class' => Provincias::class,
                'choice_label' => 'nombre',
                'placeholder'   => '-- Elija la Provincia --',
            ])
            ->add('localidad', TextType::class, ['attr' => ['placeholder' => 'Localidad']])
            ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal']])
            ->add('save', SubmitType::class, ['label' => 'Regístrate'])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $formData = $form->getData();

            $gestores = $this->getDoctrine()->getRepository(Gestores::class)->findOneBy([
                'cif' => $formData['cif'],
            ]);

            if(!empty($gestores)){
                return $this->render('gestores/alta.html.twig', array(
                    'form' => $form->createView(),
                    'creada' => false,
                    'errMail' => ''
                ));
            }

            $gestores = $this->getDoctrine()->getRepository(Gestores::class)->findOneBy([
                'email' => $formData['email'],
            ]);

            if(!empty($gestores)){

                return $this->render('gestores/alta.html.twig', array(
                    'form' => $form->createView(),
                    'creada' => false,
                    'errMail' => true
                ));
            }

            //Al convertirlos ya los asignamos a la entidad en si

            $gestor = self::altaGestorMapper($formData, $utiles);

            $utiles->sendGestoresEmail($mailer, $gestor);

            if(!$utiles->sendPassEmail($mailer, $gestor->getNombre(),$gestor->getEmail(),$gestor->getPassword())){
                return $this->redirectToRoute('crear_gestor', array('errMail' => true, 'creada' => ''));
            }else{
                
                $gestor->setPassword(password_hash($gestor->getPassword(), PASSWORD_BCRYPT));

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($gestor);
                $entityManager->flush();

                return $this->redirectToRoute('crear_gestor', array('creada' => true, 'errMail' => ''));
            }
        }

        return $this->render('gestores/alta.html.twig', array(
            'form' => $form->createView(),
            'creada' => '',
            'errMail' => ''
        ));
    }

    /**
     * @Route("/gestor", name="ya_logado")
     */
    public function login(Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $gestor = $this->getUser();

        if($gestor->getPrimeraClave()){

            $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => ['attr' => ['placeholder' => 'Contraseña']],
                'second_options' => ['attr' => ['placeholder' => 'Repetir Contraseña']],
                'invalid_message' => 'Los campos de contraseña deben ser iguales',
                'constraints' => [
                    new Regex([
                        'pattern' => '/^(?=.{8})(?=.*[A-Z])(?=.*[a-z])(?=.*\d)/',
                        'message' => 'La contraseña debe tener al menos 8 carácteres, una letra mayúscula, una letra minúscula y un número.',
                    ]),
                ],
            ))
            ->add('save', SubmitType::class, array('label' => 'Guardar'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $formData = $form->getData();

                $gestor->setPassword(password_hash($formData['password'], PASSWORD_BCRYPT));
                $gestor->setPrimeraClave(0);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($gestor);
                $entityManager->flush();

                $this->get('session')->set('templateURL', '/dashboard/common/initPage.html.twig');
                $this->get('session')->set('page', 'paginaInicial');

                return $this->render('dashboard/inicio.html.twig', array('changed' => true));
            }

            return $this->render('gestores/primerLogin.html.twig', array(
                'form' => $form->createView(),
            ));
        }else{
            $this->get('session')->set('templateURL', '/dashboard/common/initPage.html.twig');
            $this->get('session')->set('page', 'paginaInicial');

            if($gestor instanceof Gestores){

                $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findNuevosByGestor($gestor->getId());

                if($correos[0]['numNuevos']>0){
                    $this->get('session')->set('emailNuevos', $correos[0]['numNuevos']);
                }else{
                    $this->get('session')->remove('emailNuevos');
                }
            }

            return $this->render('dashboard/inicio.html.twig');
        }
    }

    /**
     * @Route("/gestor/listaEmpresas", name="lista_empresas")
     */
    public function getListaEmpresas(): Response
    {
        $gestor = $this->getUser();

        if($gestor instanceof Gestores){

            if(in_array('ROLE_ADMIN', $gestor->getRoles())){
                $listaEmpresas = $this->getDoctrine()->getRepository(Empresa::class)->findAll();
            }else if(in_array('ROLE_REGISTRO', $gestor->getRoles())){
                $listaEmpresas = $this->getDoctrine()->getRepository(Empresa::class)->findAllRegistro(time());
            }else{
                $listaEmpresas = $this->getDoctrine()->getRepository(Empresa::class)->findBy([
                    'codGestor' => $gestor->getId(),
                ]);
            }
        }

        $this->get('session')->set('templateURL', 'gestores/listaEmpresas.html.twig');
        $this->get('session')->set('page', 'listaEmpresa');

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $listaEmpresas,
        ));
    }

    /**
     * @Route("/gestor/perfil", name="perfil_gestor")
     */
    public function perfil(Request $request, Utils $utiles): Response
    {

        $this->get('session')->set('templateURL', 'gestores/perfil.html.twig');
        $this->get('session')->set('page', 'perfilGestor');

        $gestor = $this->getUser();

        $ccaa = $this->getDoctrine()->getRepository(CCAA::class)->find($gestor->getCodComunidad());
        $provincia = $this->getDoctrine()->getRepository(Provincias::class)->find($gestor->getCodProvincia());

        $form = $this->createFormBuilder()
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre', 'empty_data' => $gestor->getNombre()]]) 
            ->add('cif', TextType::class, ['attr' => ['placeholder' => 'Teléfono', 'empty_data' => $gestor->getCif()]])
            ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección', 'empty_data' => $gestor->getDireccion()]])
            ->add('telefono', TextType::class, ['attr' => ['placeholder' => 'Teléfono', 'empty_data' => $gestor->getTelefono()]])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Correo electrónico', 'empty_data' => $gestor->getEmail()]])
            ->add('licencias', IntegerType::class, ['attr' => ['placeholder' => 'Licencias disponibles', 'empty_data' => $gestor->getLicenciasDisponibles()]])
            ->add('licenciasAsistidas', IntegerType::class, ['attr' => ['placeholder' => 'Licencias Asistidas disponibles', 'empty_data' => $gestor->getLicenciasDisponiblesAsistida()]])
            ->add('codComunidad', EntityType::class, [
                'class' => CCAA::class,
                'choice_label' => 'nombre',
                'data' => $ccaa,
            ])
            ->add('codProvincia', EntityType::class, [
                'class' => Provincias::class,
                'choice_label' => 'nombre',
                'data' => $provincia,
            ])
            ->add('localidad', TextType::class, ['attr' => ['placeholder' => 'Localidad', 'empty_data' => $gestor->getLocalidad()]])
            ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal', 'empty_data' => $gestor->getCp()]])
            ->add('save', SubmitType::class, ['label' => 'Modificar datos'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        
            $formData = $form->getData();

            if($formData['cif'] !== $gestor->getCif()){
                
                $gestores = $this->getDoctrine()->getRepository(Gestores::class)->findOneBy([
                    'cif' => $formData['cif'],
                ]);
    
                if(!empty($gestores)){
                    return $this->render('dashboard/inicio.html.twig', array(
                        'form' => $form->createView(),
                        'modificado' => false
                    ));
                }
            }

            //Convertimos los datos del form a los de la entidad
            if(!in_array('ROLE_DEMO',$gestor->getRoles())){
                $gestor = self::modificaGestorMapper($formData, $gestor);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();
            }

            return $this->render('dashboard/inicio.html.twig', array(
                'form' => $form->createView(),
                'modificado' => true
            ));
        }

        return $this->render('dashboard/inicio.html.twig', array(
            'form' => $form->createView(),
            'modificado' => ''
        ));
    }

    /**
     * @Route("/gestores/buscaCNAE/{tipoBusqueda}/{criterio}", name="buscaCNAE")
     */
    public function bucarCNAE(string $tipoBusqueda, string $criterio): Response
    {
        $listaCNAE = $this->getDoctrine()->getRepository(CNAE::class)->findByBusqueda($tipoBusqueda, $criterio);

        return $this->render('/dashboard/common/widgets/cnaeList.html.twig', array(
            'lista' => $listaCNAE
        ));
    }

    /**
     * @Route("/gestores/buscaIAE/{tipoBusqueda}/{criterio}", name="buscaIAE")
     */
    public function bucarIAE(string $tipoBusqueda, string $criterio): Response
    {
        $listaCNAE = $this->getDoctrine()->getRepository(IAE::class)->findByBusqueda($tipoBusqueda, $criterio);

        return $this->render('/dashboard/common/widgets/iaeList.html.twig', array(
            'lista' => $listaCNAE
        ));
    }

    private static function altaGestorMapper($formData, $utiles){

        $gestor = new Gestores();

        $gestor->setNombre($formData['nombre']);
        $gestor->setCif($formData['cif']);
        $gestor->setDireccion($formData['direccion']);
        $gestor->setTelefono($formData['telefono']);
        $gestor->setEmail($formData['email']);
        $gestor->setCodComunidad($formData['ccaa']);
        $gestor->setCodProvincia($formData['provincia']);
        $gestor->setLocalidad($formData['localidad']);
        $gestor->setCp($formData['cp']);
        $gestor->setFechaAlta(time());
        $gestor->setLicenciasDisponibles(0);
        $gestor->setActivo(1);
        $gestor->setPrimeraClave(1);
        $gestor->setRoles($gestor->getRoles());

        $random_passwd = $utiles->generateStrongPassword();
        $gestor->setPassword($random_passwd);

        return $gestor;

    }

    private static function modificaGestorMapper($formData, $gestor){

        $gestor->setNombre($formData['nombre']);
        $gestor->setCif($formData['cif']);
        $gestor->setDireccion($formData['direccion']);
        $gestor->setTelefono($formData['telefono']);
        $gestor->setEmail($formData['email']);
        $gestor->setCodComunidad($formData['codComunidad']);
        $gestor->setCodProvincia($formData['codProvincia']);
        $gestor->setLocalidad($formData['localidad']);
        $gestor->setCp($formData['cp']);
        
        return $gestor;

    }
    
}


