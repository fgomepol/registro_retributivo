<?php

namespace App\Form;

use App\Entity\Trabajador;
use App\Entity\GrupoProfesional;
use App\Entity\ConvenioColectivo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TrabajadorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['help' => 'Nombre del trabajador',])
            ->add('apellidos', TextType::class)
            ->add('dni', TextType::class)
            ->add('TipoConvenio', TextType::class)
            ->add('codConvenio', EntityType::class, array(
                    'class' => ConvenioColectivo::class,
                    'choice_label' => function($convenio){
                            return $convenio->getNombreConvenio();
                        },
                    'placeholder' => 'Debe elegir un convenio colectivo'
                )
            )
            ->add('sexo', ChoiceType::class, array(
                'choices' => array(
                    'Hombre' => 'H',
                    'Mujer' => 'M')
                )
            )
            ->add('grupo', EntityType::class, array(
                'class' => GrupoProfesional::class,
                'choice_label' => function($grupo){
                        return $grupo->getGrupo().' - '.$grupo->getNombreGrupo();
                    },
                    'placeholder' => 'Debe elegir un grupo profesional'
                )
            )
            ->add('puesto', TextType::class)
            ->add('numpagas', ChoiceType::class, array(
                'choices' => array(
                    '12' => '12',
                    '14' => '14')
                )
            )
            ->add('fechaInicioEmpresa', DateType::class, array(
                'attr' => array(
                    'class' => 'js-datepicker'
                ),
                'widget' => 'single_text'
                )
            )
            ->add('fechaFinEmpresa', DateType::class, array(
                'attr' => array(
                    'class' => 'js-datepicker'
                ),
                'widget' => 'single_text',
                'required' => false
                )
            )
            ->add('save', SubmitType::class, ['label' => 'Guardar trabajador'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trabajador::class,
        ]);
    }
}