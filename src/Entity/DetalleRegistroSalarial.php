<?php

namespace App\Entity;

use App\Repository\DetalleRegistroSalarialRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DetalleRegistroSalarialRepository::class)
 */
class DetalleRegistroSalarial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @ORM\OneToMany(targetEntity="CentroTrabajo", mappedBy="DetalleRegistroSalarial", cascade={"ALL"}, indexBy="id")
     */
    private $codCentro;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $puesto;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $sexo;

    /**
     * @ORM\Column(type="string")
     */
    private $concepto;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $horas;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(type="smallint")
     */
    private $mes;

    /**
     * @ORM\Column(type="smallint")
     */
    private $anio;

    /**
     * @ORM\ManyToOne(targetEntity=Empresa::class, inversedBy="detallesRegistroSalarial")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codEmpresa;

    /**
     * @ORM\ManyToOne(targetEntity=Trabajador::class, inversedBy="detallesRegistroSalarial")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codTrabajador;

    /**
     * @ORM\ManyToOne(targetEntity=GrupoProfesional::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $grupo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $motivoCambio;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity=ComplementoSalarial::class)
     */
    private $codComplemento;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $repetible = false;

    /**
     * @ORM\ManyToOne(targetEntity=ComplementoExtrasalarial::class)
     */
    private $codComplementoExtrasalarial;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCentro(): ?int
    {
        return $this->codCentro;
    }

    public function setCodCentro(?int $codCentro): self
    {
        $this->codCentro = $codCentro;

        return $this;
    }

    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    public function setPuesto(string $puesto): self
    {
        $this->puesto = $puesto;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getHoras(): ?int
    {
        return $this->horas;
    }

    public function setHoras(?int $horas): self
    {
        $this->horas = $horas;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getMes(): ?int
    {
        return $this->mes;
    }

    public function setMes(int $mes): self
    {
        $this->mes = $mes;

        return $this;
    }

    public function getAnio(): ?int
    {
        return $this->anio;
    }

    public function setAnio(int $anio): self
    {
        $this->anio = $anio;

        return $this;
    }

    public function getCodEmpresa(): ?Empresa
    {
        return $this->codEmpresa;
    }

    public function setCodEmpresa(?Empresa $codEmpresa): self
    {
        $this->codEmpresa = $codEmpresa;

        return $this;
    }

    public function getCodTrabajador(): ?Trabajador
    {
        return $this->codTrabajador;
    }

    public function setCodTrabajador(?Trabajador $codTrabajador): self
    {
        $this->codTrabajador = $codTrabajador;

        return $this;
    }

    public function getGrupo(): ?GrupoProfesional
    {
        return $this->grupo;
    }

    public function setGrupo(?GrupoProfesional $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getMotivoCambio(): ?string
    {
        return $this->motivoCambio;
    }

    public function setMotivoCambio(?string $motivoCambio): self
    {
        $this->motivoCambio = $motivoCambio;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getCodComplemento(): ?ComplementoSalarial
    {
        return $this->codComplemento;
    }

    public function setCodComplemento(?ComplementoSalarial $codComplemento): self
    {
        $this->codComplemento = $codComplemento;

        return $this;
    }

    public function getRepetible(): ?bool
    {
        return $this->repetible;
    }

    public function setRepetible(?bool $repetible): self
    {
        $this->repetible = $repetible;

        return $this;
    }

    public function getCodComplementoExtrasalarial(): ?ComplementoExtrasalarial
    {
        return $this->codComplementoExtrasalarial;
    }

    public function setCodComplementoExtrasalarial(?ComplementoExtrasalarial $codComplementoExtrasalarial): self
    {
        $this->codComplementoExtrasalarial = $codComplementoExtrasalarial;

        return $this;
    }
}
