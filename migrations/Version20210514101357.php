<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210514101357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE correo_soporte DROP FOREIGN KEY FK_C1E2AD9481AF5F9C');
        $this->addSql('DROP INDEX IDX_C1E2AD9481AF5F9C ON correo_soporte');
        $this->addSql('ALTER TABLE correo_soporte DROP cod_gestor_destinatario_id');
        $this->addSql('ALTER TABLE datos_facturacion DROP FOREIGN KEY FK_F03143BD25F15E99');
        $this->addSql('DROP INDEX IDX_F03143BD64ABF1E8 ON datos_facturacion');
        $this->addSql('ALTER TABLE datos_facturacion CHANGE cp cp SMALLINT UNSIGNED, CHANGE cod_provicia_id cod_provincia_id INT NOT NULL');
        $this->addSql('ALTER TABLE datos_facturacion ADD CONSTRAINT FK_F03143BD64ABF1E8 FOREIGN KEY (cod_provincia_id) REFERENCES provincias (id)');
        $this->addSql('CREATE INDEX IDX_F03143BD64ABF1E8 ON datos_facturacion (cod_provincia_id)');
        $this->addSql('ALTER TABLE empresa DROP carta_digital, CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED, CHANGE licencias_disponibles_asistida licencias_disponibles_asistida SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador ADD roles JSON NOT NULL, CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE correo_soporte ADD cod_gestor_destinatario_id INT NOT NULL');
        $this->addSql('ALTER TABLE correo_soporte ADD CONSTRAINT FK_C1E2AD9481AF5F9C FOREIGN KEY (cod_gestor_destinatario_id) REFERENCES gestores (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C1E2AD9481AF5F9C ON correo_soporte (cod_gestor_destinatario_id)');
        $this->addSql('ALTER TABLE datos_facturacion DROP FOREIGN KEY FK_F03143BD64ABF1E8');
        $this->addSql('DROP INDEX IDX_F03143BD64ABF1E8 ON datos_facturacion');
        $this->addSql('ALTER TABLE datos_facturacion CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE cod_provincia_id cod_provicia_id INT NOT NULL');
        $this->addSql('ALTER TABLE datos_facturacion ADD CONSTRAINT FK_F03143BD25F15E99 FOREIGN KEY (cod_provicia_id) REFERENCES provincias (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_F03143BD64ABF1E8 ON datos_facturacion (cod_provicia_id)');
        $this->addSql('ALTER TABLE empresa ADD carta_digital VARCHAR(200) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles_asistida licencias_disponibles_asistida SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE trabajador DROP roles, CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
