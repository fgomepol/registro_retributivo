<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210325133546 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE empresa ADD activo TINYINT(1) DEFAULT NULL, ADD uuid VARCHAR(255) DEFAULT NULL, ADD verification_code VARCHAR(6) DEFAULT NULL, ADD roles JSON NOT NULL, CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE empresa DROP activo, DROP uuid, DROP verification_code, DROP roles, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
