<?php

namespace App\Repository;

use App\Entity\GrupoProfesional;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GrupoProfesional|null find($id, $lockMode = null, $lockVersion = null)
 * @method GrupoProfesional|null findOneBy(array $criteria, array $orderBy = null)
 * @method GrupoProfesional[]    findAll()
 * @method GrupoProfesional[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrupoProfesionalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GrupoProfesional::class);
    }

    // /**
    //  * @return GrupoProfesional[] Returns an array of GrupoProfesional objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GrupoProfesional
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
