<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210409122943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INTEGER UNSIGNED, CHANGE fecha_pago fecha_pago INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE fecha_renovacion fecha_renovacion INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED, CHANGE fecha_factura fecha_factura INTEGER UNSIGNED');
        $this->addSql('ALTER TABLE gestores CHANGE fecha_alta fecha_alta INTEGER UNSIGNED, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED, CHANGE ccaa ccaa SMALLINT UNSIGNED, CHANGE provincia provincia SMALLINT UNSIGNED, CHANGE cp cp SMALLINT UNSIGNED');
        $this->addSql('ALTER TABLE informe_registro_salarial ADD cod_gestor_id INT DEFAULT NULL, CHANGE fecha_creacion fecha_creacion INTEGER UNSIGNED, CHANGE cod_empresa cod_empresa_id INT NOT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial ADD CONSTRAINT FK_AE6465F5E0F7686E FOREIGN KEY (cod_gestor_id) REFERENCES gestores (id)');
        $this->addSql('ALTER TABLE informe_registro_salarial ADD CONSTRAINT FK_AE6465F57902D32C FOREIGN KEY (cod_empresa_id) REFERENCES empresa (id)');
        $this->addSql('CREATE INDEX IDX_AE6465F5E0F7686E ON informe_registro_salarial (cod_gestor_id)');
        $this->addSql('CREATE INDEX IDX_AE6465F57902D32C ON informe_registro_salarial (cod_empresa_id)');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INTEGER UNSIGNED, CHANGE fecha_alta fecha_alta INTEGER UNSIGNED');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE centro_trabajo CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE compras CHANGE fecha_compra fecha_compra INT UNSIGNED DEFAULT NULL, CHANGE fecha_pago fecha_pago INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE empresa CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE fecha_renovacion fecha_renovacion INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE facturas CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_factura fecha_factura INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE gestores CHANGE ccaa ccaa SMALLINT UNSIGNED DEFAULT NULL, CHANGE provincia provincia SMALLINT UNSIGNED DEFAULT NULL, CHANGE cp cp SMALLINT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL, CHANGE licencias_disponibles licencias_disponibles SMALLINT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE informe_registro_salarial DROP FOREIGN KEY FK_AE6465F5E0F7686E');
        $this->addSql('ALTER TABLE informe_registro_salarial DROP FOREIGN KEY FK_AE6465F57902D32C');
        $this->addSql('DROP INDEX IDX_AE6465F5E0F7686E ON informe_registro_salarial');
        $this->addSql('DROP INDEX IDX_AE6465F57902D32C ON informe_registro_salarial');
        $this->addSql('ALTER TABLE informe_registro_salarial DROP cod_gestor_id, CHANGE fecha_creacion fecha_creacion INT UNSIGNED DEFAULT NULL, CHANGE cod_empresa_id cod_empresa INT NOT NULL');
        $this->addSql('ALTER TABLE trabajador CHANGE fecha_inicio_empresa fecha_inicio_empresa INT UNSIGNED DEFAULT NULL, CHANGE fecha_alta fecha_alta INT UNSIGNED DEFAULT NULL');
    }
}
