<?php

namespace App\Controller;

use App\Entity\Compras;
use App\Entity\DatosFacturacion;
use App\Entity\Gestores;
use App\Entity\OrderItem;
use App\Entity\Producto;
use App\Form\AddToCartType;
use App\Form\DatosFacturacionType;
use App\Manager\CartManager;
use App\Repository\ProductoRepository;
use App\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductoController extends AbstractController
{
    /**
     * @Route("/gestores/productos", name="productos")
     */
    public function index(): Response
    {
        $this->get('session')->set('templateURL', 'producto/lista.html.twig');
        $this->get('session')->set('page', 'listaProductos');

        $loggedUser = $this->getUser();

        if ($loggedUser instanceof Gestores) {

            return $this->render('dashboard/inicio.html.twig', array(
                'lista' => $this->getDoctrine()->getRepository(Producto::class)->findAll(),
            ));
        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/checkout", name="checkout")
     */
    public function checkout(CartManager $cartManager, Request $request, Utils $utils): Response
    {
        $this->get('session')->set('templateURL', 'producto/checkout.html.twig');
        $this->get('session')->set('page', 'checkout');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            $cart = $cartManager->getCurrentCart();

            if(sizeof($cart->getItems()) > 0){

                $datosFacturacion = $this->getDoctrine()->getRepository(DatosFacturacion::class)->findOneBy([
                    'codGestor' => $loggedUser
                ]);

                if(empty($datosFacturacion)){
                    $datosFacturacion = new DatosFacturacion();

                    $datosFacturacion = $utils->datosFacturacionMapper($loggedUser, $datosFacturacion);

                    $form = $this->createForm(DatosFacturacionType::class, $datosFacturacion);

                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {

                        $formData = $form->getData();
                        $datosFacturacion = $utils->datosFacturacionMapper($formData, $datosFacturacion);

                        $datosFacturacion->setCodGestor($loggedUser);

                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($datosFacturacion);
                        $entityManager->flush();

                        $form = $this->createForm(DatosFacturacionType::class);

                        return $this->render('dashboard/inicio.html.twig', [
                            'cart' => $cart,
                            'datosFacturacion' => $datosFacturacion
                        ]);
                    }

                    return $this->render('dashboard/inicio.html.twig', [
                        'cart' => $cart,
                        'form' => $form->createView(),
                        'datosFacturacion' => $datosFacturacion
                    ]); 
                }

                return $this->render('dashboard/inicio.html.twig', [
                    'cart' => $cart,
                    'datosFacturacion' => $datosFacturacion
                ]);
            }else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');
    
                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "carro"
                ));
            }
            return $this->render('dashboard/inicio.html.twig', array(
                'lista' => $this->getDoctrine()->getRepository(Producto::class)->findAll(),
            ));
        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/{idProducto}/{cantidad}/addProducto", name="addProducto")
     */
    public function add(int $idProducto, int $cantidad, CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'producto/lista.html.twig');
        $this->get('session')->set('page', 'listaProductos');

        $producto = $this->getDoctrine()->getRepository(Producto::class)->find($idProducto);

        if(!empty($producto)){

            $cart = $cartManager->getCurrentCart();
            
            $item = new OrderItem();
            $item->setProducto($producto);
            $item->setCantidad($cantidad);

            $cart
                ->addItem($item)
                ->setActualizadoEl(new \DateTime());

            $cartManager->save($cart);

            $totalArticulos = sizeof($cart->getItems());

            $this->get('session')->set('numArticulos', $totalArticulos);

            return $this->render('dashboard/inicio.html.twig', array(
                'lista' => $this->getDoctrine()->getRepository(Producto::class)->findAll()
            ));

        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos del producto introducido no son correctos."
            ));
        }
        

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $this->getDoctrine()->getRepository(Producto::class)->findAll(),
        ));
    }

    /**
     * @Route("/gestores/{idProducto}/{cantidad}/removeProducto", name="removeProducto")
     */
    public function remove(int $idProducto, int $cantidad, ProductoRepository $productoRepository): Response
    {
        $this->get('session')->set('templateURL', 'producto/lista.html.twig');
        $this->get('session')->set('page', 'listaProductos');

        $carro = $this->get('session')->get('carro');

        $producto = $this->getDoctrine()->getRepository(Producto::class)->find($idProducto);

        $carro->getC($producto);
        $carro->setCantidad($cantidad);

        $this->get('session')->set('carro', $carro);

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $productoRepository->findAll(),
        ));
    }

    /**
     * @Route("/gestores/productos/{id}", name="detalleProducto")
     */
    public function desc(Producto $producto, Request $request, CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'producto/lista.html.twig');
        $this->get('session')->set('page', 'listaProductos');

        $form = $this->createForm(AddToCartType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $item = $form->getData();
            $item->setProducto($producto);

            $cart = $cartManager->getCurrentCart();
            $cart
                ->addItem($item)
                ->setActualizadoEl(new \DateTime());

            $cartManager->save($cart);

            return $this->redirectToRoute('cart');
        }

        return $this->render('producto/detalle.html.twig', [
            'producto' => $producto,
            'form' => $form->createView()
        ]);
    }
}