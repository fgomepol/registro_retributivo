$(document).ready(function() {
    $("#listaEmpresas").addClass("active");
    $("#registroMenu").addClass("menu-is-opening menu-open");
    $("#registroRetributivo").show();

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});

$("form[name='form']").validate({
    rules: {
        'form[anio]': "required"
    },
    messages: {
        'form[anio]': "Por favor, seleccione el año para generar el informe"
    },
    submitHandler: function(form) {
        form.submit();
    }
});