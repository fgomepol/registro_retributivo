<?php

namespace App\Controller;

use App\Entity\CorreoSoporte;
use App\Entity\Empresa;
use App\Entity\Gestores;
use App\Form\CorreoSoporteType;
use App\Utils\Utils;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class MailboxController extends AbstractController
{
    /**
     * @Route("/gestores/mailbox", name="mailbox")
     */

    public function mailbox(): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
        $this->get('session')->set('page', 'mailbox');

        $loggedUser = $this->getUser();

        if($loggedUser instanceof Gestores){
            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByGestor($loggedUser->getId(), 'mailbox');
        }else if($loggedUser instanceof Empresa){
            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByEmpresa($loggedUser->getId());
        }

        $correosNuevos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findNuevosByGestor($loggedUser->getId());

        if($correosNuevos[0]['numNuevos']>0){
            $this->get('session')->set('emailNuevos', $correosNuevos[0]['numNuevos']);
        }else{
            $this->get('session')->remove('emailNuevos');
        }

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $correos,
            'cabecera' => 'Bandeja de entrada'
        ));
    }

    /**
     * @Route("/gestores/compose", name="compose")
     */
    public function compose(Request $request, SluggerInterface $slugger): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/compose.html.twig');
        $this->get('session')->set('page', 'composeMail');

        $loggedUser = $this->getUser();

        if (in_array("ROLE_SOPORTE", $loggedUser->getRoles())) {

            $lista_gestores = $this->getDoctrine()->getRepository(Gestores::class)->findAllGestoresActivos();

            $array_final = array();
            foreach($lista_gestores as $val){ 
                $array_final[$val->getNombre()] = $val->getId();
            }

            $form = $this->createForm(CorreoSoporteType::class)
            ->add('gestorSeleccionado', ChoiceType::class, array(
                'choices' => $array_final,
                'placeholder' => '-- Elija el Gestor Destinatario --'
            ));
        }else{
            $form = $this->createForm(CorreoSoporteType::class);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $correoSoporte = new CorreoSoporte();

            $correoSoporte = self::correoMapper($correoSoporte, $form, $slugger);

            if($loggedUser instanceof Gestores){
                $correoSoporte->setCodGestor($loggedUser);
                if (!in_array("ROLE_SOPORTE", $loggedUser->getRoles())) {
                    $destinatario = $this->getDoctrine()->getRepository(Gestores::class)->findDestinatarioSoporte();
                    $correoSoporte->setCodGestorDestinatario($destinatario->getId());
                }else{
                    $destinatario = $this->getDoctrine()->getRepository(Gestores::class)->find($form->getData()['gestorSeleccionado']);
                    $correoSoporte->setCodGestorDestinatario($destinatario->getId());
                }
                
            }else if($loggedUser instanceof Empresa){
                $correoSoporte->setCodEmpresa($loggedUser);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($correoSoporte);
            $entityManager->flush();

            $form = $this->createForm(CorreoSoporteType::class);

            $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
            $this->get('session')->set('page', 'mailbox');
            
            return $this->redirectToRoute('mailbox');
        }

        if (in_array("ROLE_SOPORTE", $loggedUser->getRoles())) {
            $listaGestores = $this->getDoctrine()->getRepository(Gestores::class)->findAllGestoresActivos();

            return $this->render('dashboard/inicio.html.twig', array(
                'form' => $form->createView(),
                'listaGestores' => $listaGestores
            ));
        }
        
        return $this->render('dashboard/inicio.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/gestores/reply/{idCorreo}", name="reply")
     */
    public function reply(Request $request, int $idCorreo, SluggerInterface $slugger): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/compose.html.twig');
        $this->get('session')->set('page', 'composeMail');

        $correoRelated = $this->getDoctrine()->getRepository(CorreoSoporte::class)->find($idCorreo);

        if(!empty($correoRelated)){
            $loggedUser = $this->getUser();

            if (in_array("ROLE_SOPORTE", $loggedUser->getRoles())) {

                $array_final[$correoRelated->getCodGestor()->getNombre()] = $correoRelated->getCodGestor()->getId();
                
                $form = $this->createForm(CorreoSoporteType::class)
                ->add('gestorSeleccionado', ChoiceType::class, array(
                    'choices' => $array_final
                ));
            }else{
                $form = $this->createForm(CorreoSoporteType::class);
            }

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $correoSoporte = new CorreoSoporte();

                $correoSoporte = self::correoMapper($correoSoporte, $form, $slugger);

                if($loggedUser instanceof Gestores){
                    $correoSoporte->setCodGestor($loggedUser);
                    if (!in_array("ROLE_SOPORTE", $loggedUser->getRoles())) {
                        $destinatario = $this->getDoctrine()->getRepository(Gestores::class)->findDestinatarioSoporte();
                        $correoSoporte->setCodGestorDestinatario($destinatario->getId());
                    }else{
                        echo "Gestor Seleccionado: ".$form->getData()['gestorSeleccionado'];
                        $destinatario = $this->getDoctrine()->getRepository(Gestores::class)->find($form->getData()['gestorSeleccionado']);
                        $correoSoporte->setCodGestorDestinatario($destinatario->getId());
                    }
                    
                }else if($loggedUser instanceof Empresa){
                    $correoSoporte->setCodEmpresa($loggedUser);
                }

                $correoSoporte->setCodCorreoRelacionado($correoRelated);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($correoSoporte);
                $entityManager->flush();

                $form = $this->createForm(CorreoSoporteType::class);

                $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
                $this->get('session')->set('page', 'mailbox');
                
                return $this->redirectToRoute('mailbox');
            }

            if (in_array("ROLE_SOPORTE", $loggedUser->getRoles())) {
                $listaGestores = $this->getDoctrine()->getRepository(Gestores::class)->findAllGestoresActivos();

                return $this->render('dashboard/inicio.html.twig', array(
                    'form' => $form->createView(),
                    'listaGestores' => $listaGestores,
                    'correoRelacionado' => $correoRelated
                ));
            }
            
            return $this->render('dashboard/inicio.html.twig', array(
                'form' => $form->createView(),
                'correoRelacionado' => $correoRelated
            ));
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos del correo introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/gestores/read-mail/{idCorreo}/{bandeja}", name="read-mail")
     */
    public function readMail(int $idCorreo, string $bandeja): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/read-mail.html.twig');
        $this->get('session')->set('page', 'readMail');

        $correo = $this->getDoctrine()->getRepository(CorreoSoporte::class)->find($idCorreo);

        $correoSoporte = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByCod($idCorreo);

        if(!empty($correoSoporte)){

            $correo->setEstado('Leido');

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($correo);
            $entityManager->flush();

            $gestor = $this->getUser();

            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findNuevosByGestor($gestor->getId());

            if($correos[0]['numNuevos']>0){
                $this->get('session')->set('emailNuevos', $correos[0]['numNuevos']);
            }else{
                $this->get('session')->remove('emailNuevos');
            }

            return $this->render('dashboard/inicio.html.twig', array(
                'datosCorreo' => $correoSoporte,
                'bandeja'     => $bandeja
            ));
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos del correo introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/gestores/delete/{idCorreo}/{bandeja}", name="delete-mail")
     */

    public function deleteEmail(int $idCorreo, string $bandeja): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
        $this->get('session')->set('page', 'mailbox');

        $gestor = $this->getUser();

        if($bandeja == "entrada"){
            $correo = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findOneBy([
                'id' => $idCorreo,
                'codGestorDestinatario' => $gestor->getId()
            ]);
        }else if($bandeja == "enviados"){
            $correo = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findOneBy([
                'id' => $idCorreo,
                'codGestor' => $gestor->getId()
            ]);
        }

        if(!empty($correo)){

            $correo->setEstado('Borrado');

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($correo);
            $entityManager->flush();

            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findNuevosByGestor($gestor->getId());

            if($correos[0]['numNuevos']>0){
                $this->get('session')->set('emailNuevos', $correos[0]['numNuevos']);
            }else{
                $this->get('session')->remove('emailNuevos');
            }

            return $this->redirectToRoute('mailbox');
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos del correo introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/gestores/delete-definitivo/{idCorreo}", name="delete-mail-definitivo")
     */

    public function deleteEmailDefinitivo(int $idCorreo): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
        $this->get('session')->set('page', 'mailbox');

        $gestor = $this->getUser();

        if(in_array('ROLE_SOPORTE', $gestor->getRoles())){
            $correo = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findOneBy([
                'id' => $idCorreo,
                'codGestorDestinatario' => $gestor->getId()
            ]);
        }else{
            $correo = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findOneBy([
                'id' => $idCorreo,
                'codGestor' => $gestor
            ]);
        }

        if(!empty($correo)){

            unlink('../public/uploads/screen_incidencias/'.$correo->getAdjuntos()[0]);

            $this->getDoctrine()->getRepository(CorreoSoporte::class)->deleteEmailById($idCorreo);
            
            return $this->redirectToRoute('mailbox-deleted');
        }else{
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "Los datos del correo introducidos no son correctos."
            ));
        }
    }

    /**
     * @Route("/gestores/mailbox-send", name="mailbox-send")
     */
    public function mailboxSend(): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
        $this->get('session')->set('page', 'mailbox');

        $loggedUser = $this->getUser();

        if($loggedUser instanceof Gestores){
            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByGestor($loggedUser->getId(), 'mailbox-send');
        }else if($loggedUser instanceof Empresa){
            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByEmpresa($loggedUser->getId());
        }

        $correosNuevos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findNuevosByGestor($loggedUser->getId());

        if($correosNuevos[0]['numNuevos']>0){
            $this->get('session')->set('emailNuevos', $correosNuevos[0]['numNuevos']);
        }else{
            $this->get('session')->remove('emailNuevos');
        }

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $correos,
            'cabecera' => 'Correos Enviados'
        ));
    }

    /**
     * @Route("/gestores/mailbox-deleted", name="mailbox-deleted")
     */
    public function mailboxDeleted(): Response
    {
        $this->get('session')->set('templateURL', 'dashboard/pages/mailbox/mailbox.html.twig');
        $this->get('session')->set('page', 'mailbox');

        $loggedUser = $this->getUser();

        if($loggedUser instanceof Gestores){
            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByGestor($loggedUser->getId(), 'mailbox-deleted');
        }else if($loggedUser instanceof Empresa){
            $correos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findCorreoByEmpresa($loggedUser->getId());
        }

        $correosNuevos = $this->getDoctrine()->getRepository(CorreoSoporte::class)->findNuevosByGestor($loggedUser->getId());

        if($correosNuevos[0]['numNuevos']>0){
            $this->get('session')->set('emailNuevos', $correosNuevos[0]['numNuevos']);
        }else{
            $this->get('session')->remove('emailNuevos');
        }

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $correos,
            'cabecera' => 'Correos Eliminados'
        ));
    }

    private static function correoMapper(CorreoSoporte $correoSoporte, $form, SluggerInterface $slugger){

        $correoSoporte->setCuerpo($form->getData()['cuerpo']);
        $correoSoporte->setTitulo($form->getData()['titulo']);

        $imagenAdjunta = $form->get('adjuntos')->getData();

        if ($imagenAdjunta) {
            $originalFilename = pathinfo($imagenAdjunta->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$imagenAdjunta->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $imagenAdjunta->move(
                    '../public/uploads/screen_incidencias/',
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
            }

            // updates the 'brochureFilename' property to store the PDF file name
            // instead of its contents
            $correoSoporte->setAdjuntos(array($newFilename));
        }

        $correoSoporte->setFechaCreacion(new \DateTime());
        $correoSoporte->setLeido(false);
        $correoSoporte->setTipoCorreo("Inicial");
        $correoSoporte->setEstado("No Leido");

        return $correoSoporte;
    }
}
