<?php

namespace App\Entity;

use App\Repository\DatosInformeRegistroSalarialRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DatosInformeRegistroSalarialRepository::class)
 */
class DatosInformeRegistroSalarial
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     * @ORM\OneToMany(targetEntity="GrupoProfesional", mappedBy="DatosInformeRegistroSalarial", cascade={"ALL"}, indexBy="id")
     */
    private $grupo;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $sexo;

    /**
     * @ORM\Column(type="string")
     */
    private $concepto;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(type="float")
     */
    private $brecha;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity=InformeRegistroSalarial::class, inversedBy="datosInformeRegistroSalarial")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idInforme;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipoCalculo;

    /**
     * @ORM\Column(type="integer")
     */
    private $numTrabajadores;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity=ComplementoSalarial::class)
     */
    private $codComplemento;

    /**
     * @ORM\ManyToOne(targetEntity=ComplementoExtrasalarial::class)
     */
    private $codComplementoExtrasalarial;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrupo(): ?int
    {
        return $this->grupo;
    }

    public function setGrupo(int $grupo): self
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    public function setSexo(string $sexo): self
    {
        $this->sexo = $sexo;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getBrecha(): ?float
    {
        return $this->brecha;
    }

    public function setBrecha(float $brecha): self
    {
        $this->brecha = $brecha;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getIdInforme(): ?InformeRegistroSalarial
    {
        return $this->idInforme;
    }

    public function setIdInforme(?InformeRegistroSalarial $idInforme): self
    {
        $this->idInforme = $idInforme;

        return $this;
    }

    public function getTipoCalculo(): ?string
    {
        return $this->tipoCalculo;
    }

    public function setTipoCalculo(string $tipoCalculo): self
    {
        $this->tipoCalculo = $tipoCalculo;

        return $this;
    }

    public function getNumTrabajadores(): ?int
    {
        return $this->numTrabajadores;
    }

    public function setNumTrabajadores(int $numTrabajadores): self
    {
        $this->numTrabajadores = $numTrabajadores;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCodComplemento(): ?ComplementoSalarial
    {
        return $this->codComplemento;
    }

    public function setCodComplemento(?ComplementoSalarial $codComplemento): self
    {
        $this->codComplemento = $codComplemento;

        return $this;
    }

    public function getCodComplementoExtrasalarial(): ?ComplementoExtrasalarial
    {
        return $this->codComplementoExtrasalarial;
    }

    public function setCodComplementoExtrasalarial(?ComplementoExtrasalarial $codComplementoExtrasalarial): self
    {
        $this->codComplementoExtrasalarial = $codComplementoExtrasalarial;

        return $this;
    }
}
