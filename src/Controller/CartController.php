<?php

namespace App\Controller;

use App\Entity\Compras;
use App\Entity\DatosFacturacion;
use App\Entity\DetalleCompras;
use App\Entity\Empresa;
use App\Entity\FacturaDetalle;
use App\Entity\Facturas;
use App\Entity\Gestores;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Producto;
use App\Form\CartType;
use App\Manager\CartManager;
use App\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/gestores/cart", name="cart")
     */
    public function index(CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'carro/carro.html.twig');
        $this->get('session')->set('page', 'carro');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            $cart = $cartManager->getCurrentCart();

            return $this->render('dashboard/inicio.html.twig', [
                'cart' => $cart
            ]);

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/cart/{idArticulo}/delete", name="borrar_del_carro")
     */
    public function delete(int $idArticulo, CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'carro/carro.html.twig');
        $this->get('session')->set('page', 'carro');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            $cart = $cartManager->getCurrentCart();
            $producto = $this->getDoctrine()->getManager()->getRepository(Producto::class)->find($idArticulo);
            $order = $this->getDoctrine()->getManager()->getRepository(Order::class)->find($cart->getId());

            if(!empty($producto) && !empty($order)){
                
                $orderItem = $this->getDoctrine()->getManager()->getRepository(OrderItem::class)->findOneBy([
                    'producto' => $producto,
                    'orderRef' => $order
                ]);

                $cart->removeItem($orderItem);
                
                $cart->setActualizadoEl(new \DateTime());
                $cartManager->save($cart);

                $totalArticulos = sizeof($cart->getItems());

                $this->get('session')->set('numArticulos', $totalArticulos);

                return $this->render('dashboard/inicio.html.twig', [
                    'cart' => $cart
                ]);
            }else{
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');

                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "El articulo del carro de compra no es correcto."
                ));
            }

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/cart/limpar", name="limpia_carro")
     */
    public function modifica(CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'carro/carro.html.twig');
        $this->get('session')->set('page', 'carro');

        $cart = $cartManager->getCurrentCart();
        $cart->removeItems();    
        $cart->setActualizadoEl(new \DateTime());
        $cartManager->save($cart);

        $this->get('session')->remove('numArticulos');

        return $this->render('dashboard/inicio.html.twig', [
            'cart' => $cart
        ]);
    }

    /**
     * @Route("/gestores/paypal/{id}/{payerId}/{importe}", name="paypal_data")
     */
    public function paypal(string $id, string $payerId, float $importe, CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'carro/carro.html.twig');
        $this->get('session')->set('page', 'carro');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            $datosCompra = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findOneBy([
                'paypalCode' => $id,
                'paypalPayer' => $payerId
            ]);
    
            if(empty($datosCompra)){
                $cart = $cartManager->getCurrentCart();

                $importeFinal = $cart->getTotal() - ($cart->getTotal()/1.21) * 0.15;

                if(number_format($importe,2,',','.') == number_format($importeFinal,2,',','.')){

                    $datosCompra = new Compras();               
                
                    $datosCompra->setPaypalCode($id);
                    $datosCompra->setPaypalPayer($payerId);
                    $datosCompra->setModoPago('PayPal');
                    $datosCompra->setPagado(false);
        
                    $datosCompra = self::comprasMapper($datosCompra, $cart, $loggedUser);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($datosCompra);
                    $entityManager->flush();

                    $datosCompraBD = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findOneBy([
                        'paypalCode' => $id,
                        'paypalPayer' => $payerId
                    ]);

                    self::detallesMapper($datosCompraBD, $cart, $entityManager);

                    return new Response('OK', 200);

                } else {
                    return new Response('El importe abonado '.number_format($importe,2,',','.').' no coincide con el importe de la compra realizada '.number_format($importeFinal,2,',','.').'.', 412);
                } 

            } else {
                return new Response('Los datos de la compra introducidos ya se han gestionado.', 412);
            }

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/paypal/confirm/{id}/{payerId}/{status}", name="paypal_confirm")
     */
    public function paypalConfirm(string $id, string $payerId, string $status, CartManager $cartManager, Request $request, MailerInterface $mailer, Utils $utils): Response
    {
        $this->get('session')->set('templateURL', 'carro/carro.html.twig');
        $this->get('session')->set('page', 'carro');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            $datosCompra = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findOneBy([
                'paypalCode' => $id,
                'paypalPayer' => $payerId
            ]);
    
            if(!empty($datosCompra)){

                $cart = $cartManager->getCurrentCart();
                
                if($status == "COMPLETED"){

                    $datosCompra->setPagado(true);
                    $datosCompra->setFechaPago(time());

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($datosCompra);
                    $entityManager->flush();

                    $idFactura = $utils->generaFacturaPaypal($loggedUser, $cart, $entityManager, $utils, $datosCompra);

                    $utils->sendInvoice($mailer, $idFactura, $loggedUser->getEmail());
                    
                    $totalLicencias = 0;
                    $totalLicenciasAsistidas = 0;
                    foreach ($cart->getItems() as $item){

                        if($item->getProducto()->getTipoLicencia() == "Experta"){
                            $totalLicencias += $item->getProducto()->getNumLicencias();
                        }else if ($item->getProducto()->getTipoLicencia() == "Asistida"){
                            $totalLicenciasAsistidas += $item->getProducto()->getNumLicencias();
                        }
                    }

                    $totalLicencias = $totalLicencias +  $loggedUser->getLicenciasDisponibles();
                    $loggedUser->setLicenciasDisponibles($totalLicencias);

                    $totalLicenciasAsistidas = $totalLicenciasAsistidas +  $loggedUser->getLicenciasDisponiblesAsistida();
                    $loggedUser->setLicenciasDisponiblesAsistida($totalLicenciasAsistidas);
                    
                    $cart->removeItems();
                    $cart->setActualizadoEl(new \DateTime());
                    
                    $cartManager->save($cart);
                    $this->get('session')->remove('numArticulos');

                    $datosCompra = $this->getDoctrine()->getManager()->getRepository(Order::class)->find($cart->getId());

                    $this->get('session')->set('templateURL', 'producto/lista.html.twig');
                    $this->get('session')->set('page', 'listaProductos');
                    
                    $request->query->remove('id');
                    $request->query->remove('payerId');
                    $request->query->remove('status');

                    return $this->redirectToRoute('productos', array('compra' => true));
                    
                } else {
                    $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                    $this->get('session')->set('page', 'error404');
        
                    return $this->render('dashboard/inicio.html.twig', array(
                        'error' => "El importe pagado no coincide con el de la compra realizada."
                    ));
                } 

            } else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');
    
                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "Los datos de la compra introducidos no son correctos."
                ));
            } 

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/transferencia/{importe}", name="transferencia_data")
     */
    public function transferencia(float $importe, CartManager $cartManager): Response
    {
        $this->get('session')->set('templateURL', 'carro/carro.html.twig');
        $this->get('session')->set('page', 'carro');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            $cart = $cartManager->getCurrentCart();

            $importeFinal = $cart->getTotal() - ($cart->getTotal()/1.21) * 0.15;

            if(number_format($importe,2,',','.') == number_format($importeFinal,2,',','.')){

                $datosCompra = new Compras();
            
                $datosCompra->setModoPago('Transferencia');
                $datosCompra->setPagado(false);
    
                $datosCompra = self::comprasMapper($datosCompra, $cart, $loggedUser);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($datosCompra);
                $entityManager->flush();

                $datosCompraBD = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findUltimaCompra($loggedUser->getId());

                self::detallesMapper($datosCompraBD, $cart, $entityManager);

                $cart->removeItems();
                $cart->setActualizadoEl(new \DateTime());
                
                $cartManager->save($cart);
                $this->get('session')->remove('numArticulos');

                return new Response('OK', 200);

            } else {
                return new Response('El importe abonado '.number_format($importe,2,',','.').' no coincide con el importe de la compra realizada '.number_format($importeFinal,2,',','.').'.', 412);
            } 

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/listaFacturas", name="lista_facturas")
     */
    public function listaFacturas(): Response
    {
        $this->get('session')->set('templateURL', 'gestores/listaFacturas.html.twig');
        $this->get('session')->set('page', 'listaFacturas');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {
            
            return $this->render('dashboard/inicio.html.twig', array(
                'lista' => $this->getDoctrine()->getManager()->getRepository(Facturas::class)->findBy([
                    'codGestor' => $loggedUser
                ])
            ));            
        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/gestores/factura/{codFactura}", name="genera_factura")
     */
    public function genera_factura(int $codFactura, Utils $utils): Response
    {
        $this->get('session')->set('templateURL', 'gestores/listaFacturas.html.twig');
        $this->get('session')->set('page', 'listaFacturas');

        $loggedUser = $this->getUser();
        
        if ($loggedUser instanceof Gestores) {

            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
                $factura = $this->getDoctrine()->getManager()->getRepository(Facturas::class)->find($codFactura);
            }else if(in_array('ROLE_GESTOR', $this->getUser()->getRoles())){
                $factura = $this->getDoctrine()->getManager()->getRepository(Facturas::class)->findOneBy([
                    'id' => $codFactura,
                    'codGestor' => $loggedUser
                ]);
            }

            if(!empty($factura)){
                return $utils->generaFacturaPDF($factura);
            } else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');
    
                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "La factura indicada no existe."
                ));
            }
            
        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    private static function comprasMapper($datosCompra, $cart, $gestor){

        $datosCompra->setCodGestor($gestor);
        $datosCompra->setFechaCompra($cart->getActualizadoEl()->getTimestamp());
        $datosCompra->setImporte($cart->getTotal());

        return $datosCompra;
    }

    private static function detallesMapper(Compras $datosCompraBD, $cart, $entityManager){

        foreach($cart->getItems() as $item){

            $detalleCompra = new DetalleCompras();

            $detalleCompra->setCantidad($item->getCantidad());
            $detalleCompra->setCodCompra($datosCompraBD);
            $detalleCompra->setIdProducto($item->getProducto());
            $detalleCompra->setImporte($item->getTotal());

            $entityManager->persist($detalleCompra);
            $entityManager->flush();
        }
    }
    
}
