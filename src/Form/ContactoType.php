<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre']])
            ->add('email', EmailType::class, ['attr' => ['placeholder' => 'Correo electrónico de la empresa']])
            ->add('mensaje', TextareaType::class, ['attr' => ['placeholder' => 'Mensaje']])
            ->add('telefono', TextType::class, ['attr' => ['placeholder' => 'Teléfono']])
            ->add('save', SubmitType::class, ['label' => 'Mandar Mensaje'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('attr', [
            'novalidate' => false
        ]);
    }
}
