<?php

namespace App\Controller;

use App\Entity\CCAA;
use App\Entity\Compras;
use App\Entity\DatosFacturacion;
use App\Entity\DetalleCompras;
use App\Entity\Facturas;
use App\Entity\Gestores;
use App\Entity\Producto;
use App\Entity\Provincias;
use App\Form\DatosFacturacionType;
use App\Manager\CartManager;
use App\Utils\Utils;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/transferencias", name="lista_transferencias")
     */
    public function transferencias(): Response
    {
        $this->get('session')->set('templateURL', 'admin/listaTransferencias.html.twig');
        $this->get('session')->set('page', 'listaTransferencias');

        $loggedUser = $this->getUser();
        
        if (in_array('ROLE_ADMIN', $loggedUser->getRoles())) {

            $lista = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findComprasNoPagadas();

            return $this->render('dashboard/inicio.html.twig', [
                'lista' => $lista,
                'pagada' => ''
            ]);

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/admin/transferencia/confirm/{idCompra}/{idGestor}", name="transferencia_confirm")
     */
    public function transferenciaConfirm(int $idCompra, int $idGestor, MailerInterface $mailer, Request $request, Utils $utils): Response
    {

        $this->get('session')->set('templateURL', 'admin/listaTransferencias.html.twig');
        $this->get('session')->set('page', 'listaTransferencias');

        $loggedUser = $this->getUser();
        
        if (in_array('ROLE_ADMIN', $loggedUser->getRoles())) {

            $gestor = $this->getDoctrine()->getManager()->getRepository(Gestores::class)->find($idGestor);

            if(!empty($gestor)){

                $datosCompra = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findOneBy([
                    'id' => $idCompra,
                    'codGestor' => $gestor
                ]);
        
                if(!empty($datosCompra)){

                    $datosCompra->setPagado(true);
                    $datosCompra->setFechaPago(time());

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($datosCompra);
                    $entityManager->flush();

                    $idFactura = $utils->generaFacturaTransferencia($gestor, $entityManager, $datosCompra);

                    $utils->sendInvoice($mailer, $idFactura, $gestor->getEmail());
                    
                    $lista = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findComprasNoPagadas();

                    $request->query->remove('idCompra');
                    $request->query->remove('idGestor');

                    return $this->redirectToRoute('lista_transferencias', array('pagada' => true));

                } else {
                    $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                    $this->get('session')->set('page', 'error404');
        
                    return $this->render('dashboard/inicio.html.twig', array(
                        'error' => "Los datos de la compra introducidos no son correctos."
                    ));
                } 
            } else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');

                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "Los datos del gestor introducidos no son correctos."
                ));
            } 

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/admin/transferencia/delete/{idCompra}/{idGestor}", name="transferencia_delete")
     */
    public function transferenciaDelete(int $idCompra, int $idGestor, CartManager $cartManager, MailerInterface $mailer, Utils $utils): Response
    {
        $this->get('session')->set('templateURL', 'admin/listaTransferencias.html.twig');
        $this->get('session')->set('page', 'listaTransferencias');
        
        $loggedUser = $this->getUser();
        
        if (in_array('ROLE_ADMIN', $loggedUser->getRoles())) {

            $gestor = $this->getDoctrine()->getManager()->getRepository(Gestores::class)->find($idGestor);

            if(!empty($gestor)){

                $this->getDoctrine()->getManager()->getRepository(Compras::class)->deleteCompraById($idCompra);

                $lista = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findBy([
                    'pagado' => 0
                ]);

                return $this->render('dashboard/inicio.html.twig', [
                    'lista' => $lista,
                    'pagada' => true
                ]);

            } else {
                $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
                $this->get('session')->set('page', 'error404');

                return $this->render('dashboard/inicio.html.twig', array(
                    'error' => "Los datos del gestor introducidos no son correctos."
                ));
            } 

        } else {
            $this->get('session')->set('templateURL', 'dashboard/pages/examples/404.html.twig');
            $this->get('session')->set('page', 'error404');

            return $this->render('dashboard/inicio.html.twig', array(
                'error' => "No tiene permiso para acceder a esta página."
            ));
        }
    }

    /**
     * @Route("/admin/factura", name="crear_factura_admin")
     */
    public function crearFactura(Utils $utils, Request $request): Response
    {
        $this->get('session')->set('templateURL', 'admin/factura.html.twig');
        $this->get('session')->set('page', 'nuevaFactura');

        $loggedUser = $this->getUser();

        $datosFacturacion = new DatosFacturacion();
        $generada = false;
        $idFactura = 0;

        $form = $this->createFormBuilder()
            ->add('nombre', TextType::class, ['attr' => ['placeholder' => 'Nombre de la empresa']])
            ->add('cif', TextType::class, ['attr' => ['placeholder' => 'CIF/NIF/NIE de la empresa']])
            ->add('direccion', TextType::class, ['attr' => ['placeholder' => 'Dirección de la empresa']])
            ->add('cp', TextType::class, ['attr' => ['placeholder' => 'Código postal']])
            ->add('localidad', TextType::class, ['attr' => ['placeholder' => 'Localidad']])
            ->add('codProvincia', EntityType::class, [
                'class' => Provincias::class,
                'choice_label' => 'nombre',
            ])
            ->add('codComunidad', EntityType::class, [
                'class' => CCAA::class,
                'choice_label' => 'nombre',
            ])
            ->add('save', SubmitType::class, ['label' => 'Guardar Datos'])
            ->add('codGestor', EntityType::class, [
                'class' => DatosFacturacion::class,
                'choice_label' => function ($datosFacturacion) {
                    return $datosFacturacion->getNombre() . ' - ' . $datosFacturacion->getCif();
                },
                'placeholder'   => '-- Elija el Gestor --',
                'required' => false
            ])
            ->add('producto', EntityType::class, [
                'class' => Producto::class,
                'choice_label' => function ($producto) {
                    return $producto->getNombre() . ' - ' . number_format($producto->getPVD(),2,',','.');
                },
                'placeholder'   => '-- Elija el Producto --'
            ])
            ->add('cantidad', ChoiceType::class, array(
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                    '7' => '7',
                    '8' => '8',
                    '9' => '9',
                    '10' => '10')
                )
            )
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $formData = $form->getData();

            $producto = $this->getDoctrine()->getRepository(Producto::class)->find($formData['producto']);

            $datosCompra = new Compras();
            $detalleCompras = new DetalleCompras();

            $entityManager = $this->getDoctrine()->getManager();
            $fechaCompra = time();
            
            $datosCompra->setFechaCompra($fechaCompra);
            $datosCompra->setCodGestor($loggedUser);
            $datosCompra->setModoPago("Transferencia");
            $datosCompra->setImporte($producto->getPVD() * $formData['cantidad']);
            $datosCompra->setPagado(1);

            $entityManager->persist($datosCompra);
            $entityManager->flush();

            $datosCompraBD = $this->getDoctrine()->getManager()->getRepository(Compras::class)->findUltimaCompraPersonalizada($loggedUser->getId());

            $detalleCompras->setCodCompra($datosCompraBD);
            $detalleCompras->setCantidad($formData['cantidad']);
            $detalleCompras->setIdProducto($producto);
            $detalleCompras->setImporte($producto->getPVD() * $formData['cantidad']);

            $entityManager->persist($detalleCompras);
            $entityManager->flush();
            
            if(empty($formData['codGestor'])){
                $datosFacturacion = $utils->datosFacturacionPersonalizadaMapper($formData, $datosFacturacion);
                $entityManager->persist($datosFacturacion);
                $entityManager->flush();

                $datosFacturacionDB = $this->getDoctrine()->getRepository(DatosFacturacion::class)->findOneBy([
                    'cif' =>  $datosFacturacion->getCif(),
                    'nombre' =>  $datosFacturacion->getNombre()
                ]);

            }else{
                $datosFacturacionDB = $this->getDoctrine()->getRepository(DatosFacturacion::class)->find($formData['codGestor']);
            }

            $idFactura = $utils->generaFacturaPersonalizada($datosFacturacionDB, $entityManager, $datosCompraBD);

            return $this->redirectToRoute('crear_factura_admin', array('generada' => true, 'codFactura' => $idFactura));
        }

        return $this->render('dashboard/inicio.html.twig', [
            'form' => $form->createView(),
            'lista' => $this->getDoctrine()->getRepository(Producto::class)->findAll()
        ]);       
    }

    /**
     * @Route("/admin/listaFacturas", name="lista_facturas_admin")
     */
    public function listaFacturas(): Response
    {
        $this->get('session')->set('templateURL', 'admin/listaFacturas.html.twig');
        $this->get('session')->set('page', 'listaFacturas');

        return $this->render('dashboard/inicio.html.twig', array(
            'lista' => $this->getDoctrine()->getManager()->getRepository(Facturas::class)->findAll()
        ));            
    }
}
