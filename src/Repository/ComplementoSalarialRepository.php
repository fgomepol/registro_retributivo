<?php

namespace App\Repository;

use App\Entity\ComplementoSalarial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ComplementoSalarial|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComplementoSalarial|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComplementoSalarial[]    findAll()
 * @method ComplementoSalarial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplementoSalarialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComplementoSalarial::class);
    }

    // /**
    //  * @return ComplementoSalarial[] Returns an array of ComplementoSalarial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ComplementoSalarial
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
